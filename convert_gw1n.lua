
print("converting .nes to gw1n .mi and userflash binary")

local nes_file = assert(io.open("build/test_rom.nes", "rb"))	
local mi_file = assert(io.open("build/test_rom.mi", "wb"))	
local uf_file = assert(io.open("build/test_rom.ufb", "wb"))	

--file example:
--#File_format=Hex
--#Address_depth=8192
--#Data_width=8
--AA
--55
--C3
--...  8192 lines total
	mi_file:write("#File_format=Hex\n")
	mi_file:write("#Address_depth=8192\n")
	mi_file:write("#Data_width=8\n")


	local count = 0
	local data

	while (count < (32*1024+16)) do --32KByte file + header
		local data = nes_file:read(1)

		--user flash data:
		--$8000-AFFF -> bank0&1
		if ((count>=16) and (count<(8*1024+16))) then
			uf_file:write(data)
		end

		--user flash code:
		--$D000-DFFF -> bank2
		if ((count>=(20*1024+16)) and (count<24*1024+16)) then
			uf_file:write(data)
		end

	--	if (count<3) then 
	--		--print "NES"
	--		print(data) 
	--		print(string.format('%02X', string.byte(data)))
	--	end 

		--if (count>=(16) and (count<8*1024+16)) then --skip over header, create from first 8KB of ROM
		if (count>=(24*1024+16)) then --skip over first 24KByte + header
			--convert the binary byte to ascii
			mi_file:write(string.format('%02X', string.byte(data)))
			mi_file:write('\n')
		end
		count = count+1
	end


assert(nes_file:close())
assert(mi_file:close())
assert(uf_file:close())

print("done")


APTR0_ROW = LINE1
APTR1_ROW = APTR0_ROW+1
APTR2_ROW = APTR0_ROW+2
APTR3_ROW = APTR0_ROW+3
APTR_ADDR_HI = LEFT_MARGIN+1;5
APTR_ADDR_LO = LEFT_MARGIN+3;7
APTR_WR_VAL = LEFT_MARGIN+7;11
APTR_RD_VAL = LEFT_MARGIN+12;16

peekpoke_rw_str:
.asciiz "$xxxx Wxxh Rxxh"

peekpoke_inst_str:
.asciiz "Sel:Wr St:Rd"

peekpoke_test_init:

	;draw lines above
	jsr ppu_update

	mva_ptr ptr0, peekpoke_rw_str
	ldx #LEFT_MARGIN
	ldy #APTR0_ROW
	jsr ppu_write_string

	ldx #LEFT_MARGIN
	ldy #APTR1_ROW
	jsr ppu_write_string

	;draw lines above
	jsr ppu_update

	ldx #LEFT_MARGIN
	ldy #APTR2_ROW
	jsr ppu_write_string

	ldx #LEFT_MARGIN
	ldy #APTR3_ROW
	jsr ppu_write_string

	;draw lines above
	jsr ppu_update

	mva_ptr ptr0, peekpoke_inst_str
	ldx #LEFT_MARGIN+11
	ldy #NAME_ROW
	jsr ppu_write_string

	;move cursor to start address line
	lda #LINE1
	sta cursor_y
	lda #LEFT_MARGIN+1
	sta cursor_x

	;switch function pointer to run test
	mva_ptr test_fptr, peekpoke_run

rts;peekpoke_test_init:


peekpoke_run:

	;if cursor is on pointer rows, check for peek/poke/updates
	lda cursor_y
	cmp #APTR0_ROW
	bpl :+
		rts
	:
	cmp #APTR3_ROW+1
	bmi :+
		rts
	:


	;SELECT PEEK
	lda gamepad_new
	and #PAD_START
	beq :+
		jsr peek_row
	:
	;SELECT POKE
	lda gamepad_new
	and #PAD_SELECT
	beq :+
		jsr poke_row
	:

	;check for cursor input
	lda cursor_arg
	bne :+
		rts
	:

	pha ;cursor arg value (amount to change the value under the cursor by)

	;clear so don't come back
	lda #0
	sta cursor_arg

	;input was given, check row
	lda cursor_y

	cmp #APTR0_ROW
	bne :+
		ldx #0 ;table index
		baeq @check_col
	:
	cmp #APTR1_ROW
	bne :+
		ldx #2 ;table index
		bane @check_col
	:
	cmp #APTR2_ROW
	bne :+
		ldx #4 ;table index
		bane @check_col
	:
	cmp #APTR3_ROW
	bne :+
		ldx #6 ;table index
		bane @check_col
	:

	;didn't match row
	pla
	rts

	@check_col:
	lda cursor_x
	cmp #APTR_ADDR_HI
	bne :+
		pla;lda cursor_arg
		mul a, #16
		add aptr_table_hi, x
		sta aptr_table_hi, x
		jmp @done
	:
	cmp #APTR_ADDR_HI+1
	bne :+
		pla;lda cursor_arg
		add aptr_table_hi, x
		sta aptr_table_hi, x
		jmp @done
	:
	cmp #APTR_ADDR_LO
	bne :+
		pla;lda cursor_arg
		mul a, #16
		add aptr_table_lo, x
		sta aptr_table_lo, x
		jmp @done
	:
	cmp #APTR_ADDR_LO+1
	bne :+
		pla;lda cursor_arg
		add aptr_table_lo, x
		sta aptr_table_lo, x
		jmp @done
	:
	cmp #APTR_WR_VAL
	bne :+
		pla;lda cursor_arg
		mul a, #16
		add aptr_table_wr, x
		sta aptr_table_wr, x
		jmp @done
	:
	cmp #APTR_WR_VAL+1
	bne :+
		pla;lda cursor_arg
		add aptr_table_wr, x
		sta aptr_table_wr, x
		jmp @done
	:

	;no col match
	pla
	rts

	@done:
	jsr display_aptr
rts;peekpoke_run:



;peek the line of cursor
peek_row:

	lda cursor_y
	sub #APTR0_ROW
	mul a, #2
	tax
	jsr ptr_peek_display	

rts ;run_test

;poke the line of cursor
poke_row:
	lda cursor_y
	sub #APTR0_ROW
	mul a, #2
	tax
	lda aptr_table_wr, x
	sta (aptr_table, x)
rts ;poke_row


;X = aptr number * 2
;cursor_y set to row would like to access
ptr_peek_display:
;@tempX = reg4
;@tempY = reg5
;
;	stx @tempX
;	sty @tempY

	lda (aptr_table, x)
	ldx #APTR_RD_VAL
	ldy cursor_y
	jsr display_hex_byte	;A-byte X-xpos Y-ypos uses reg1&0

	
rts ;ptr0_peek_display

;A poke value
;X col start display
;Y row to display
;ptr0_poke_peek_display:
;@tempX = reg7
;@tempY = reg6
;
;	stx @tempX
;	sty @tempY
;
;	;display the address
;	lda ptr0_hi
;	jsr display_hex_byte
;
;	ldy @tempY
;	inc @tempX
;	inc @tempX
;	ldx @tempX
;
;	lda ptr0_lo
;	jsr display_hex_byte
;
;	ldy #0
;	lda (ptr0), y
;
;	ldy @tempY
;	inc @tempX
;	inc @tempX
;	inc @tempX
;	ldx @tempX
;
;	jsr display_hex_byte
;
;	;display the byte to write
;	
;rts ;ptr0_peek_display


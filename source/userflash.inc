
UFLASH_REG_BASE = $4030
UFLASH_MODE	= UFLASH_REG_BASE+0
;MODE[3:0] Description
;0000 Read operation and page latch write-in
	UFLASH_MODE_READ 	= %00000000
;0001 Set the pre-programmed and reset automatically when the write operation starts.
	UFLASH_MODE_PEP		= %00000001
;0100 Clear page latch
	UFLASH_MODE_CLRPGL	= %00000100
;1000 Erase page or row
	UFLASH_MODE_ERASE	= %00001000
;1100 Write operation of page or row
	UFLASH_MODE_WRITE	= %00001100
		
UFLASH_SEQ	= UFLASH_REG_BASE+1
	UFLASH_SEQ1 = 1
	UFLASH_SEQ2 = 2
	UFLASH_SEQ3 = 3
	UFLASH_SEQ0 = 0
UFLASH_XPAGE	= UFLASH_REG_BASE+2

UFLASH_CLKCTL 	= UFLASH_REG_BASE+3
	UFLASH_CLKCTL_MAN0 = %00000010
	UFLASH_CLKCTL_MAN1 = %00000011
	UFLASH_CLKCTL_OSC  = %00000000
	UFLASH_CLKCTL_PE1  = %00000110
	UFLASH_CLKCTL_PE0  = %00000010
	UFLASH_CLKCTL_PW1  = %10000000
	UFLASH_CLKCTL_PW0  = %00000000

;UFLASH_ADDR 	= UFLASH_REG_BASE+4
;UFLASH_DATA 	= UFLASH_REG_BASE+5
;UFLASH_???? 	= UFLASH_REG_BASE+6
;UFLASH_???? 	= UFLASH_REG_BASE+7

;0: mode[3:0]    <= cpu_D[3:0];
;1: seq[1:0]     <= cpu_D[1:0];
;2: Xpage[5:0]   <= cpu_D[5:0];
;3:	clkctl  <= cpu_D[1]
;	wr_clk  <= cpu_D[0];
;   `ifdef MANUAL_PW
;                                pw      <= cpu_D[7];
;                        `endif
;                                pe      <= cpu_D[2];
;                                clkctl  <= cpu_D[1];
;                                wr_clk  <= cpu_D[0];
;                        end
;                        //if move this data register when should to change
;                        //define below for pw clocking TOO!
;                        4:
;                        begin
;                        `ifdef MANUAL_PW
;                                wr_data[7:0]    <= cpu_D[7:0];
;                        `endif
;                        end
;                        5:
;                        begin
;                        `ifdef ADDR_REGS
;                                pa[5:0]         <= cpu_D[7:2];
;                                wbsel[1:0]      <= cpu_D[1:0] ^ 2'b11;
;                        `endif
;                        end


UFLASH_PGL_BASE = $4100

;RAM ADDRESS to store the 256B page buffer to write to flash
UFLASH_PAGE_BUFF = $0700

;//      Ts0     SEQ0 cycle                              6       -       -       �s      T=1+
;UFLASH_TS0  = 7
;;//      Ts1     SEQ1 cycle                              15      -       -       �s      T=3+
;UFLASH_TS1  = 16
;;//      Ts2p    Setup ACLK to PE rising edge            5       -       10      �s      T=1
;UFLASH_TS2P = 7 
;;//      Ts3     SEQ3 cycle                              5       -       10      �s      T=1
;UFLASH_TS3  = 7
;;//      Tps3    Setup from PE falling edge to ACLK      60      -       -       �s      T=9+
;;//parameter TPS3 = 10; //61.3us
;UFLASH_TPS3 = 60
;//      Tpe     MODE=1000 erasure time                  5.7     6       6.3     ms      T=857
;UFLASH_TPEE = 6
UFLASH_TPEE = $86 ;bit7 set -> msec count
;UFLASH_TPEE_SPECIAL = $80 ;write the page latches while waiting for erase Tpe.  removed this feature once learned more about Pre-Program step
;//      Tpe     MODE=1100 Write operation time          1.9     2       2.1     ms      T=286
;UFLASH_TPEW = 2
UFLASH_TPEW = $82 ;bit7 set -> msec count
;//      Tpe     MODE=11xx pre-program time              190     200     210     us      T=29
;UFLASH_TPEP = 200
UFLASH_TPEP = 42 ;bit7 clear reg0 count 42d -> meas 202.8us 

.macro	mva_uflash_clk_lo
	lda #UFLASH_CLKCTL_MAN0
	sta UFLASH_CLKCTL
.endmacro

.macro	mva_uflash_clk_pe_set
	lda #UFLASH_CLKCTL_PE1
	sta UFLASH_CLKCTL
.endmacro

.macro	mva_uflash_clk_pe_clr
	lda #UFLASH_CLKCTL_PE0
	sta UFLASH_CLKCTL
.endmacro

.macro	mva_uflash_pw_clk
	lda #UFLASH_CLKCTL_PW1
	sta UFLASH_CLKCTL
	lda #UFLASH_CLKCTL_PW0
	sta UFLASH_CLKCTL
.endmacro


.macro	mva_uflash_clk_hi
	lda #UFLASH_CLKCTL_MAN1
	sta UFLASH_CLKCTL
.endmacro

.macro	mva_uflash_clk_osc
	lda #UFLASH_CLKCTL_OSC
	sta UFLASH_CLKCTL
.endmacro

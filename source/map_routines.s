
;input globals: scroll_dir, cur_room
;output update global: nat_ptr, 
			;NO LONGER DOES THIS for HORZ: and PPU address on init 
			;for horz, dec nat_ptr so can always inc by 56
			;for vert dec/inc by 32
;			also set at_ptr for vert scrolls
			;TODO don't use this function to set initial PPU
			;address for VERT either!
;required: scroll_dir = SCROLL_UP/DOWN/LEFT/RIGHT

;TODO next stream function always points to begining of data, it's only the special up scroll case that differs and it can do it's own math.
;Also PPU start address shouldn't be determined in this function

;cases when call this func:
;1: we lost/corrupted pointer to current room's data
;	just reset it in this case
;	Could just use a cur_room_ptr variable and copy when needed
;	OR call this function and provide current room and it uses LUT
;2: A room change was triggered, need next room's data to draw
;	We provide current room, and direction of next room
;	This function needs to use a LUT to determine address of next
;	room's data

; Let's use two different pointers separate from nat_ptr.
; nat_ptr: is the working pointer, gets inc/dec as being used
; cur_nat_ptr: is pointer to current room's data
; next_nat_ptr: is pointer to next room's data

;based on current room & direction set next_nat_ptr
;TODO move to MAP_BANK
set_next_nat_ptr:
@room_size = reg0
@room_size_msb = reg1
@num_rooms = reg2

	lda scroll_dir ;probably already contained

	and #HORZ_BIT 
	beq @check_vert

	;clear sizes for L/R moves
	lda #0
	sta @room_size
	sta @room_size_msb

	lda scroll_dir
	and #UL_BIT
	beq @right


		;update room_size_ptr to the current room
		lda room_size_ptr+0
		ldy #ROOM_LEFT_IDX
		sub (map_ptr), y

		sta room_size_ptr+0
		bcs :+
			dec room_size_ptr+1	
		:

		;how many rooms to skip/add?
		lda (map_ptr), y
		tay
		dey ;don't subtract current room's size

		;add up number of bytes to move map_ptr
		@next_left_room:
			lda (room_size_ptr), y
			add @room_size
			sta @room_size
			bcc :+
				inc @room_size_msb
			:
			dey
			bpl @next_left_room


		;subtract room_size to map_ptr
		lda map_ptr+0
		sub @room_size
		sta map_ptr+0

		lda map_ptr+1
		sbc @room_size_msb
		sta map_ptr+1

		jmp @load_next

	
	@right:
		;how many rooms to skip/add?
		ldy #ROOM_RIGHT_IDX
		lda (map_ptr), y
		;sta @num_rooms
		tax ;temp store

		tay ;(room_size_ptr), y-> desination room's size
		dey ;points to last room that needs to be skipped over
		;add up number of bytes to move map_ptr
		@next_right_room:
			lda (room_size_ptr), y
			add @room_size
			sta @room_size
			bcc :+
				inc @room_size_msb
			:
			dey
			bpl @next_right_room ;include y=0 (previous room)

		;update room_size_ptr to the current room
		txa
		add room_size_ptr+0
		sta room_size_ptr+0
		bcc :+
			inc room_size_ptr+1	
		:

		;add room_size to map_ptr
		lda map_ptr+0
		add @room_size
		sta map_ptr+0

		lda map_ptr+1
		adc @room_size_msb
		sta map_ptr+1

		jmp @load_next
	
	@check_vert:
	lda scroll_dir
	and #UL_BIT
	beq @down

		;SCROLL UP
		ldy #ROOM_UPDOWN_IDX
		lda (map_ptr), y
		beq @up_in_level
			;TODO down scroll is into another level/area
			brk

		@up_in_level:

		;dec room_size_ptr to find size of preceding room
		lda room_size_ptr+0 ;special case if 00->FF
		bne :+ 
			dec room_size_ptr+1
		:
		dec room_size_ptr+0

		;add current value at room_size_ptr to map_ptr
		ldy #0
		lda (room_size_ptr), y
		sta @room_size
		
		;subtract size of previous room
		lda map_ptr+0
		;sub #2 
		sub @room_size
		sta map_ptr+0
		bcs :+
			dec map_ptr+1
		:
		jmp @load_next

	@down:

		ldy #ROOM_UPDOWN_IDX
		lda (map_ptr), y
		beq @down_in_level
			;TODO down scroll is into another level/area
			brk

		@down_in_level:
		;add current value at room_size_ptr to map_ptr
		ldy #0
		lda (room_size_ptr), y
		sta @room_size
		
		;increment room_size_ptr
		inc room_size_ptr+0
		bne :+ ;inc doesn't set carry, but will set Zflag
			inc room_size_ptr+1
		:
		
		;add 2 for next room down
		lda map_ptr+0
		add @room_size
		sta map_ptr+0
		bcc :+
			inc map_ptr+1
		:

	@load_next:
		;load next_nat_ptr
		ldy #ROOM_NTADDR_IDX
		lda (map_ptr), y
		sta next_nat_ptr+0
		iny
		lda (map_ptr), y
		sta next_nat_ptr+1

	rts 
;set_next_nat_ptr:


;pre-req: map_ptr points to new room (done by room_change)
;	  room_size_ptr also points to new room
init_room:
@room_size = reg0
	;initialize enemies in the room
	;TODO make this based on room number etc..

	ldy #0
	lda (room_size_ptr), y
	;sta @room_size ;number of bytes in room struct
	tay ;number of bytes in room struct

	;if == FIRST_OBJ_IDX then there's no object data for this room

	ldx #0; cur obj index
	;y-> first byte of next room struct
	jmp @next_object
;	cpy  #FIRST_OBJ_IDX
;	dey ;Y-loc of last object
	;ldy #FIRST_OBJ_IDX
	@copy_object:

		;copy Y location
		lda (map_ptr), y
		sta spr_y, x
		dey
		;copy X location
		lda (map_ptr), y
		sta spr_x, x
		dey
		;copy object type
		lda (map_ptr), y
		sta sprite_list, x
		inx
		@next_object:
		dey
		cpy  #FIRST_OBJ_IDX-1
		bne @copy_object



	;TODO orbs creation, collection, etc

	;room data must contain:
	;enemy type - store in sprite_list
	;enemy spawn location - load into enemy array


;	lda #SKEL
;	;sta sprite_list+NME0_IDX
;	sta sprite_list+0
;	mva spr0_x, #183
;	mva spr0_y, #71


;;	lda #84
;;	sta enemy_list+NME_ST*0+NME_X
;	mva enemy_list+NME_ST*0+NME_X, #84
;;	lda #119
;;	sta enemy_list+NME_ST*0+NME_Y
;	mva enemy_list+NME_ST*0+NME_Y, #119

	;lda #SKEL
	;sta sprite_list+4
	;mva spr4_x, #84
	;mva spr4_y, #119


	;more enemies to test flickering
	;lda #SKEL
	;sta sprite_list+12
	;mva spr12_x, #160
	;mva spr12_y, #119

	;lda #SKEL
	;sta sprite_list+8
	;mva spr8_x, #32
	;mva spr8_y, #119

;;	lda #183
;;	sta enemy_list+NME_ST*1+NME_X
;	mva enemy_list+NME_ST*1+NME_X, #183
;;	lda #71
;;	sta enemy_list+NME_ST*1+NME_Y
;	mva enemy_list+NME_ST*1+NME_Y, #71


	;return back to code bank and and initialize objects there
	;jsr enemy_init


rts ;init_room




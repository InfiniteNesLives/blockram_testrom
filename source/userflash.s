
;userflash_inst_str:
;.asciiz "running..."
userflash_line1_str:
.asciiz "copying code to wram"

;userflash_line3_str:
;.asciiz "esp B0 B1 P0 P1"

userflash_test_init:

;	mva_ptr ptr0, userflash_inst_str
;	ldx #LEFT_MARGIN
;	ldy #INST_ROW
;	jsr ppu_write_string
;
;	jsr ppu_update

	mva_ptr ptr0, userflash_line1_str
	ldx #LEFT_MARGIN
	ldy #LINE1
	jsr ppu_write_string

	jsr ppu_update


	;copy test's code to system RAM where it's been assembled to run

;Linker labels for code contained in ROM but assembled to run in RAM
.import __USERFLASH_RAMCODE_LOAD__
.import __USERFLASH_RAMCODE_RUN__
;.import __USERFLASH_RAMCODE_SIZE__ not overly useful because want to copy all 1KB even if segment isn't full

	lda #<__USERFLASH_RAMCODE_LOAD__
	sta ptr0_lo
	lda #>__USERFLASH_RAMCODE_LOAD__
	sta ptr0_hi

	lda #<__USERFLASH_RAMCODE_RUN__
	sta ptr1_lo
	lda #>__USERFLASH_RAMCODE_RUN__
	sta ptr1_hi

	;ldx #>__USERFLASH_RAMCODE_SIZE__ ;segment prob not full can't use this..
	ldx #4 ;4x 256 = 1KByte
	jsr memcpy_Xpages


	;copy blockram code into PRG-RAM
	;this is really just for the emulator to run in same location as cartridge
	;blockram is already initialized with this data, but easier to load it into blockram for emu support
;Linker labels for code contained in ROM but assembled to run in RAM
.import __BRAM_CODE_LOAD__
.import __BRAM_CODE_RUN__
;.import __USERFLASH_RAMCODE_SIZE__ not overly useful because want to copy all 1KB even if segment isn't full

	lda #<__BRAM_CODE_LOAD__
	sta ptr0_lo
	lda #>__BRAM_CODE_LOAD__
	sta ptr0_hi

	lda #<__BRAM_CODE_RUN__
	sta ptr1_lo
	lda #>__BRAM_CODE_RUN__
	sta ptr1_hi

	ldy	#0
	lda	(ptr1_lo), Y
	cmp	#'I'
	beq	:+

	;ldx #>__USERFLASH_RAMCODE_SIZE__ ;segment prob not full can't use this..
	ldx #4 ;4x 256 = 1KByte
	jsr memcpy_Xpages

	:

	;switch function pointer to run test
	mva_ptr test_fptr, userflash_init

rts;userflash_test_init:









;Quit access point:
;AT+CWQAP
;
;OK
;WIFI DISCONNECT



;esp_at_inst_str:
;.asciiz "instructions"

esp_at_tx_str:
.asciiz "t:"

esp_at_rx_str:
.asciiz "r:"

TX_LINE=LINE1
RX_LINE=TX_LINE+3

CR = $0D
LF = $0A

at_test_msg:
AT_TEST_MSG_LEN = 5
.byte "AT",CR,LF,0
;patcmd "" ;no command should reply "OK"

;at_test_sim:
;AT_TEST_SIM_LEN = 10
;.byte "AT",CR,LF,CR,LF,"OK",CR,LF

;esp at uart messages end with 0x0D, 0x0A (CR+LF)
;by default esp first repeats back the command it was sent
;then provides it's response to the command
;sent: AT<CR+LF>
;reply:AT<CR+LF><CR+LF>OK<CR+LF>
;00000 |  41 54 0d 0a 41 54 0d 0a 0d 0a 4f 4b 0d 0a       | AT..AT....OK..  


esp_at_test_init:

	;move cursor to ESP-AT test number
	ldy #NAME_ROW
	sty cursor_y
	ldx #LEFT_MARGIN+10
	stx cursor_x

	;print ESP-AT test number
	lda #0
	sta cur_at_test
	;ldy #NAME_ROW
	;ldx #LEFT_MARGIN+9
	dex
	jsr display_hex_byte

	jsr load_at_test

;	;print test specific instructions
;	mva_ptr ptr0, esp_at_inst_str
;	ldx #LEFT_MARGIN
;	ldy #INST_ROW
;	jsr ppu_write_string

	mva_ptr ptr0, esp_at_tx_str
	ldx #LEFT_MARGIN-2
	ldy #TX_LINE
	jsr ppu_write_string

	jsr ppu_update ; ppu_update waits until next NMI, turns rendering on (if not already)

	mva_ptr ptr0, esp_at_rx_str
	ldx #LEFT_MARGIN-2
	ldy #RX_LINE
	jsr ppu_write_string

	

	;init done update test_fptr so can change test number or run test
	mva_ptr test_fptr, esp_at_test_run


rts;esp_at_test_init:

esp_at_test_run:
@temp = reg0

	;check for test start/change:
	lda cursor_y
	cmp #NAME_ROW
	bne @not_name_row
		;check for start
		lda gamepad_new
		and #PAD_START
		beq :+
			jsr run_at_test
		:

		;check for select print reply
		lda gamepad_new
		and #PAD_SELECT
		beq :+

		;;test big string writting
		;mva_ptr ptr0, at_test14 ;36char 0-9,A-Z
		;mva_ptr ptr1, ESP_TX_BUFF
		;ldy #100 ;looks like got to T
		;;ldy #30 ;31 failed, 30 seems to be max
		;jsr memcpy_max256

			;clear all the rx lines
			ldy #RX_LINE
			lda #LAST_LINE-RX_LINE+1
			jsr clear_lines

			mva_ptr ptr0, ESP_RX_BUFF
			ldx #RX_LINE ;don't provide an X position, need Y for function indexing
			;ldx #RX_LINE+32 ;don't provide an X position, need Y for function indexing
			;ldx #LAST_VIS_LINE
			jsr display_long_string
		:
		
		;check for test change with B press
		lda gamepad
		and #PAD_B
		beq @b_not_pressed
			lda gamepad_new
			and #PAD_U
			beq :++
				inc cur_at_test
				lda cur_at_test
				cmp #LAST_AT_TEST+1
				bne :+
					lda #0
					sta cur_at_test
				:
				jsr load_at_test
			:
			lda gamepad_new
			and #PAD_D
			beq :++
				dec cur_at_test
				bpl :+
					lda #LAST_AT_TEST
					sta cur_at_test
				:
				jsr load_at_test
			:
			lda cur_at_test
			ldy #NAME_ROW
			ldx #LEFT_MARGIN+9
			jsr display_hex_byte
		@b_not_pressed:
	@not_name_row:


	;check if cursor on tx lines to entering line edit mode
	lda cursor_y
	cmp #TX_LINE
	bmi @jump_out_bounds
	cmp #RX_LINE
	bmi @continue ;bpl @out_bounds
		@jump_out_bounds:
		jmp @out_bounds
	@continue:

		;cursor is in tx rows
		;check if in margin bounds
		lda cursor_x
		sub #LEFT_MARGIN
		bmi @jump_out_bounds
		cmp #LINE_LIMIT
		bpl @jump_out_bounds

		lda gamepad
		and #PAD_B
		beq @not_tx_edit

			lda gamepad_new
			and #PAD_U
			beq :+
				lda #-1
				jsr modify_cursor_char
			:
			lda gamepad_new
			and #PAD_D
			beq :+
				lda #1
				jsr modify_cursor_char
			:
			lda gamepad_new
			and #PAD_L
			beq :+
				lda #-8
				jsr modify_cursor_char
			:
			lda gamepad_new
			and #PAD_R
			beq :+
				lda #8
				jsr modify_cursor_char
			:


		@not_tx_edit:

		;check for fancy typing with ascii pattern table
		lda gamepad_new
		and #PAD_A
		beq @out_bounds

			;toggle special mode since A was pressed
			lda cursor_mode
			eor #1
			sta cursor_mode

			;handle new mode
			beq @jump_not_typing ;skip over the typing code, don't want to immediately sense keyboard input

				;entering special edit mode
				;draw block cursor in location of current cursor
				lda cursor_spr_x
				sta block_spr_x

				;preserve cursor position for when exiting keyboard entry
				lda cursor_x
				sta temp_cursor_x
				
				;cursor could be blinking off screen
				lda cursor_y
				sta temp_cursor_y
				mul a, #8
				sub #1
				sta block_spr_y

				lda #BLOCK_TILE
				sta block_t

				;block cursor is in position
				;now move the real cursor to pattern table area
				;specifically the char currently printed

				jsr cursor_to_tx_index 
				;return A = cursor index
				sta cursor_edit_idx ;store the index being edited so can reference later for updating
				tax
				lda ESP_TX_BUFF, X
				tax

				;now that have value under cursor
				;map that to tile at top of screen 32 chars per line
				;lower 5bits = Xposition
				and #$1F
				sta cursor_x
				;upper 3bits = Yposition
				txa
				lsr
				lsr
				lsr
				lsr
				lsr
				sta cursor_y
				
				;now user can move cursor around to select next char
				;use exit special edit below to handle that mode change

				@jump_not_typing:
				jmp @not_typing


	@out_bounds:

	;if in special edit mode, check if cursor is in pattern table "keyboard area"
	lda cursor_mode
	beq @not_typing

	lda cursor_y
	cmp #KEYBOARD_LINE0
	bmi @not_typing
	cmp #KEYBOARD_LAST_LINE+1
	bpl @not_typing

		;we're typing and cursor is in keyboard area
		;just keep letting user move cursor around until they press A button
		;once pressed need to copy character under cursor to tx line & redraw
		lda gamepad_new
		and #PAD_A
		beq @not_typing

			;toggle special mode since A was pressed
			;user has selected a key!
			lda cursor_mode
			eor #1
			sta cursor_mode

			;move block cursor off screen
			lda #Y_OFFSCREEN
			sta block_spr_y

			;which key is selected?
			;lower 5bits <= Xposition
			;upper 3bits <= Yposition
			lda cursor_y
			sub #KEYBOARD_LINE0
			asl 
			asl 
			asl 
			asl 
			asl 
			sta @temp
			lda cursor_x
			and #$1F
			ora @temp
			;a contains the ascii value entered with the keyboard
			ldx cursor_edit_idx ;stored this when entered special edit mode
			sta ESP_TX_BUFF, X

			jsr print_tx_lines

			;now move cursor back to character that was just updated
			;block cursor holds the position reverse map that..?  
			;maybe just use temp ram variables..
			lda temp_cursor_x
			sta cursor_x
			lda temp_cursor_y
			sta cursor_y

	@not_typing:

rts; esp_at_test_run:

load_at_test:

	ldy #TX_LINE
	lda #3
	jsr clear_lines

	jsr clr_rx_buff

	lda cur_at_test
	beq :+
		;non-zero test numbers
		jmp find_at_test_num 
	:

	;copy command to ram tx buffer
	mva_ptr ptr0, at_test_msg
	mva_ptr ptr1, ESP_TX_BUFF
	ldy #AT_TEST_MSG_LEN
	jsr memcpy_max256

	;print the command
	;mva_ptr ptr0, ESP_TX_BUFF
	;;ldx #LEFT_MARGIN
	;;ldy #TX_LINE
	;;jsr ppu_write_string
	;;long
	;ldx #TX_LINE
	;jsr display_long_string
	jsr print_tx_lines

	;prepare to run the test if the first test special test is ran
	lda #AT_TEST_MSG_LEN-1
	sta cur_at_test_len

rts;load_at_test:


run_at_test:

	;flush RX buffer
	lda #0
	sta ESP_RX_CNT
	sta ESP_RX_CTL ;bit0 clear, allow incoming uart rx
	;jsr clr_rx_buff

	;tell mapper to tx buffer
	lda cur_at_test_len
	sta ESP_TX_CNT

	;poll for reply
;	@rx_wait:
;	lda ESP_RX_REG
;	beq @rx_wait
	;hack use button press for now

;	;fake the reply for emu
;	mva_ptr ptr0, at_test_sim
;	mva_ptr ptr1, ESP_RX_BUFF
;	ldy #AT_TEST_SIM_LEN
;	jsr memcpy_max256

;	;print reply
;	mva_ptr ptr0, ESP_RX_BUFF
;	ldx #LEFT_MARGIN
;	ldy #RX_LINE
;	jsr ppu_write_string
	
rts; run_at_test:

at_test_pre:
.asciiz "AT+"
at_test1:
.asciiz "RST"
at_test2:
.asciiz "GMR"
at_test3:
.asciiz "GSLP"
at_test4:
.asciiz "RESTORE"
at_test5:
.asciiz "UART_CUR?"
at_test6:
.asciiz "RFPWR=20"
at_test7:
.asciiz "SYSRAM?"
at_test8:
.asciiz "CWMODE=3" ;1-station 2-softAP 3-both

.asciiz "CWMODE=?" ;1-station 2-softAP 3-both
.asciiz "CWLAP"
.byte "CWJAP_CUR=", $22, "network_ssid", $22, ",", $22, "pass", $22, 0 ;IDK how to escape in ca65..
.asciiz "CWQAP" ;disconnect softAP
.asciiz "CWLIF"
.byte "CWJAP_DEF=", $22, "NESnet_guest", $22, ",", $22, "infinite", $22, 0 ;IDK how to escape in ca65..
.byte "CWLAP=", $22, "NESnet_guest", $22, 0 ;IDK how to escape in ca65..
.asciiz "CWSAP_CUR?" ;current softAP
.asciiz "CIFSR" ;get IP
.byte "CIPSTART=", $22, "TCP", $22, ",", $22, "djxmmx.net", $22, ",17", 0
.byte "PING=", $22, "192.168.1.1", $22, 0
.byte "PING=", $22, "google.com", $22, 0
.byte "PING=", $22, "infiniteneslives.com", $22, 0
.byte "PING=", $22, "nesdev.com", $22, 0
.asciiz "CIPSNTPCFG=1,-6"
.asciiz "CIPSNTPTIME?"
LAST_AT_TEST = 24
;at_test14:
;.asciiz "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz"
;at_test15:
;.asciiz ""


find_at_test_num:
;@test_temp = reg0
	;use the test number to figure out what string to load from above
	;based on null separators between test names

	;copy AT pre-string "AT+" into tx buff
	mva_ptr ptr0, at_test_pre	;read from ptr0
	mva_ptr ptr1, ESP_TX_BUFF	;write to ptr1
	jsr strcpy ;updates pointers to point to last copied byte for both read & write strings

	;ptr0 & ptr1 point to null terminator that was copied from their respective source/dest
	ldx cur_at_test
	ldy #0
	@next_str:
	dex
	beq @found_test
		;advance ptr0,Y to next null terminator
		:
		iny 
		bne :+	;handle Y rollover
			inc ptr0_hi
		:
		lda (ptr0), Y
		bne :--
		baeq @next_str
		
	@found_test:
	;update ptr0 to include Y incrementing + skip over null termintor currently pointing at
	iny ;skip over null termintor
	tya
	add ptr0_lo
	sta ptr0_lo
	bcc :+
		inc ptr0_hi
	:
	;copy string at current ptr0, ptr1 points to last copied null term which need to stomp
	jsr strcpy

	;append CR+LF+NULL to tx buff
	ldy #0
	lda #CR
	sta (ptr1), y
	iny
	lda #LF
	sta (ptr1), y
	iny
	lda #0
	sta (ptr1), y

	;print the tx buff
	;mva_ptr ptr0, ESP_TX_BUFF
	;ldx #LEFT_MARGIN
	;ldy #TX_LINE
	;jsr ppu_write_string
	jsr print_tx_lines


;	;flush RX buffer
;	lda #0
;	sta ESP_RX_CNT
;	sta ESP_RX_CTL ;bit0 clear, allow incoming uart rx
;;	jsr clr_rx_buff

	;tell mapper to tx buffer
	;need to know how many bytes to send depends on message length
	;assume no zeros until the end..
	ldx #$FF
	:
		inx
		lda ESP_TX_BUFF, X
	bne :-

	;x=index of first null terminator (zero indexed, but also equals count of bytes to transmit
	;stx ESP_TX_CNT
	stx cur_at_test_len


	
rts; find_at_test_num:

;ptr0 -> memory read start address with pascal style string of the AT command
;this function provides "AT+"
esp_at_cmd:
	;pre-decrement to account for last byte
	dey

	@next_byte:
		lda (ptr0),y
		sta (ptr1),y
		dey
		bne @next_byte

	;copy last (zero index) byte
	lda (ptr0),y
	sta (ptr1),y

rts;esp_at_cpy


clr_rx_buff:
	lda #0
	tax
	@next:
	.if ESP_RX_BUFF_LEN = $400
		sta ESP_RX_BUFF+$000, X
		sta ESP_RX_BUFF+$100, X
		sta ESP_RX_BUFF+$200, X
		sta ESP_RX_BUFF+$300, X
	.else
	.error "ESP_RX_BUFF_LEN not supported for clr_rx_buff"
	.endif
	inx
	bne @next

rts;clr_rx_buff:

;A = amount to change cursor char by
;cursor x/y over the value
;don't call if cursor isn't within margin bounds
modify_cursor_char:

	pha ;value to add

	jsr cursor_to_tx_index 
	;return A = cursor index
	tax
	pla
	add ESP_TX_BUFF, X
	sta ESP_TX_BUFF, X
	
	@reprint_tx_line:
	;mva_ptr ptr0, ESP_TX_BUFF
	;;ldx #LEFT_MARGIN
	;;ldy #TX_LINE
	;;jsr ppu_write_string
	;ldx #TX_LINE
	;;jsr display_long_string
jmp print_tx_lines
;rts;modify_cursor_char:

;need to know ram index that cursor is currently hovering on
;don't call unless cursor is in bounds
;return: A=byte index that cursor is under counting from 0 start of tx line:
cursor_to_tx_index:

	lda cursor_y
	sub #TX_LINE
	;A = number of TX line to modify (0-2)
	;multiply line number by LINE LIMIT with iterative adding
	tax
	lda #0
	cpx #0
	beq :++
		:
		add #LINE_LIMIT
		dex
		bne :-
	:

	add cursor_x
	sub #LEFT_MARGIN
	
	;return A= index of tx line that cursor is hovering over
rts; cursor_to_tx_index:

print_tx_lines:
	mva_ptr ptr0, ESP_TX_BUFF
	ldx #TX_LINE
jmp display_long_string
;rts;print_tx_lines:

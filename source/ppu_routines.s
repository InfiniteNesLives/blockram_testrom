

;reset_scroll:
;	lda #0
;	sta scroll_x
;	lda #1
;	sta scroll_nmt
;
;	lda #31	;default Y is below the HUD level
;	sta scroll_y
;	rts
;;reset_scroll:


; ppu_update: waits until next NMI, turns rendering on (if not already), uploads OAM, palette, and nametable update to PPU
ppu_update:
	lda #1
	sta nmi_ready
	:
		lda nmi_ready
		bne :-
	rts


; ppu_update_tile: can be used with rendering on, sets the tile at X/Y to tile A next time you call ppu_update
;A-tilenum X-xpos Y-ypos uses reg0
ppu_update_tile:
@temp = reg0
	pha ; temporarily store A on stack
	txa
	pha ; temporarily store X on stack
	ldx nmt_update_len
	tya
	lsr
	lsr
	lsr
	ora #$20 ; high bits of Y + $20
	sta nmt_update, X
	inx
	tya
	asl
	asl
	asl
	asl
	asl
	sta @temp
	pla ; recover X value (but put in A)
	ora @temp
	sta nmt_update, X
	inx
	pla ; recover A value (tile)
	sta nmt_update, X
	inx
	stx nmt_update_len

rts ;ppu_update_tile


;ptr0 -> string starting address (preserved)
;ascii string terminated with zero (not written)
;X = screen xpos start
;Y = screen ypos row to write to
ppu_write_string:
@idx = reg1
@row = reg2
@col = reg3
;.define ASCII_OFFSET $41-$0A

	tya
	sta @row ;temp store
	txa
	sta @col ;temp store

	ldy #0	;string idx0
	sty @idx

	@next_char:
		ldy @idx
		inc @idx ;for next time
		lda (ptr0), y
		;apply ascii offset
		;A = 0x41 -> tile 0x0A
;		sub #ASCII_OFFSET
;		cmp #'A'
		beq @end ;zero terminated string
		ldy @row
		ldx @col
		jsr ppu_update_tile	;A-tilenum X-xpos Y-ypos uses reg0
		inc @col
		jmp @next_char

	@end:
rts ;ppu_write_string:

;Y = name_table upper address
;X = number of 256B pages
;A = tile number to fill NT
;arg0 = increment value written by this amount after each write
ppu_clear_nt:

;input arg:
	pha	;input arg tile number
	lda $2002 ;reset latch

;input arg:
	sty $2006	;upper address byte
	ldy #$00
	sty $2006	;lower address byte
;input arg:
	;ldx #4 ;4*256=1k (1 NT)
	ldy #0 ;256

	pla	;tile number

	@next_page:
		@next_byte:
		sta $2007
		add arg0	;increment written value
		dey
		bne @next_byte

		dex
		bne @next_page

rts ;ppu_clear_nt

;NOTE!  file size must be increments of 256Bytes
;Y = name_table upper address
;X = number of 256B pages
;ptr0 -> data to load
ppu_load_file:

	lda $2002 ;reset latch

;input arg:
	sty $2006	;upper address byte
	ldy #$00
	sty $2006	;lower address byte
;input arg:
	;ldx #4 ;4*256=1k (1 NT)
	ldy #0 ;256

	@next_page:
		@next_byte:
		lda (ptr0), y
		sta $2007
		iny
		bne @next_byte

		inc ptr0_hi
		dex
		bne @next_page
	
rts ;ppu_load_file



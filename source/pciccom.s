
;pciccom_inst_str:
;.asciiz "running..."
pciccom_line1_str:
.asciiz "B:   U:   1:   2:   3:"

pciccom_line2_str:
.asciiz "game"
pciccom_line3_str:
.asciiz "esp B0 B1 P0 P1"

pciccom_test_init:

;	mva_ptr ptr0, pciccom_inst_str
;	ldx #LEFT_MARGIN
;	ldy #INST_ROW
;	jsr ppu_write_string
;
;	jsr ppu_update

	mva_ptr ptr0, pciccom_line1_str
	ldx #LEFT_MARGIN
	ldy #LINE1
	jsr ppu_write_string

	jsr ppu_update

	mva_ptr ptr0, pciccom_line2_str
	ldx #LEFT_MARGIN
	ldy #LINE2
	jsr ppu_write_string

	mva_ptr ptr0, pciccom_line3_str
	ldx #LEFT_MARGIN
	ldy #LINE3
	jsr ppu_write_string

	;draw lines above
	jsr ppu_update

	;#1 display PCICCOM startup value, should be open bus
	lda PCICCOM_R0
	ldx #LEFT_MARGIN+2
	ldy #LINE1
	jsr display_hex_byte

	;draw lines above
	jsr ppu_update

	jsr pciccom_open_comms
	;A= reply byte
	ldx #LEFT_MARGIN+7
	ldy #LINE1
	jsr display_hex_byte

	;dummy xor test
	lda #$1E
	sta PCICCOM_R1
	jsr delay_1ms

	lda #SPI_CMD_DUMMY
	sta PCICCOM_R0
	jsr delay_1ms

	lda PCICCOM_R0
	ldx #LEFT_MARGIN+12
	ldy #LINE1
	jsr display_hex_byte
	

	;switch function pointer to run test
	mva_ptr test_fptr, pciccom_run

rts;pciccom_test_init:

;A= ciccom reply byte
pciccom_open_comms:
	;open comms & read unlock reply
	lda #'I'
	sta PCICCOM_R0
	jsr delay_1ms

	lda #'N'
	sta PCICCOM_R1
	jsr delay_1ms

	lda #'L'
	sta PCICCOM_R2
	jsr delay_1ms

	lda PCICCOM_R0
rts;pciccom_open_comms


;A=
;pciccom_cmd:
;rts;pciccom_cmd

lock_mapper:
;if (prg_rw==0 & (prg_bank_lo==8'hC5) & (chr_bank_lo==8'hA3) & (map_mode==0) & (cpu_A[14]==0) & (romsel_n==0) & (m2_eff==1))
;                map_lock = 1;
	
	lda #0	;chr_bank_lo
	sta $C000
	lda #$A3
	sta $E000

	lda #2	;prg_bank_lo
	sta $C000
	lda #$C5
	sta $E000

	;;write to $8000 to perform mapper switch
	;;this corrupts vectors so cart will crash @ NMI..
	;sta $8000

	;;jump to reset vector
	;jmp ($FFFC)

	;copy STA-JMP to SRAM then jump there
	;Have to do this because even if we're executing from $6000 where next mapper is going to place
	;block ram, the RAM may be disabled by default/boot and we'll crash as soon as write to $8000
	;and won't be able to execute the jmp (), so have to execute STA-JMP from system RAM which
	;can't help but be visible at all times.
	;0000        STA $8000       8D 00 80
	;0000        STA $8D8D       8D 8D 8D just as good
	;0003        JMP ($FFFC)     6C FC FF

	lda #$8D
	sta reg0
	;lda #$80
	sta reg1
	;lda #$00
	sta reg2
	lda #$6C
	sta reg3
	lda #$FC
	sta reg4
	lda #$FF
	sta reg5

	jmp $0000

;no returning...
;rts ;lock_mapper: 

pciccom_run:

	lda gamepad_new
	and #PAD_START
	beq @no_start
		lda cursor_y
		cmp #LINE2
		bne:+

			baeq	lock_mapper
		:
		cmp #LINE3
		bne @not_test2
			lda cursor_x
			;.asciiz "esp B0 B1 P0 P1"
			;         0123456789ABCDE
			cmp #LEFT_MARGIN+4
			bne:+
				ldy	#SPI_ESPBL_PIN
				ldx	#0
				baeq	@set_pin
			:
			cmp #LEFT_MARGIN+7
			bne:+
				ldy	#SPI_ESPBL_PIN
				ldx	#1
				bane	@set_pin
			:
			cmp #LEFT_MARGIN+10
			bne:+
				ldy	#SPI_ESPPD_PIN
				ldx	#0
				baeq	@set_pin
			:
			cmp #LEFT_MARGIN+13
			bne:+
				ldy	#SPI_ESPPD_PIN
				ldx	#1
				bane	@set_pin
			:

			;got to this point, pressed start but not in proper location
			rts

			@set_pin:

			;Y=pin, X=level

			sty PCICCOM_R1	;pin number
			jsr delay_1ms
			lda #0
			sta PCICCOM_R2	;direction
			jsr delay_1ms
			stx PCICCOM_R3	;level/value
			jsr delay_1ms

			lda #SPI_CMD_GPIO
			sta PCICCOM_R0	;command
			jsr delay_1ms

			lda PCICCOM_R0
			ldx #LEFT_MARGIN+17
			ldy #LINE1
			jsr display_hex_byte

		@not_test2:


	@no_start:

rts;pciccom_run:


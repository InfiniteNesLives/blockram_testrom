
;TODO support changing where keyboard is drawn so can be moved out of overscan..
;left/right edges might always be in overscan...
KEYBOARD_LINE0 = 0
KEYBOARD_LINE1 = KEYBOARD_LINE0+1
KEYBOARD_LINE2 = KEYBOARD_LINE0+2
KEYBOARD_LINE3 = KEYBOARD_LINE0+3
KEYBOARD_LINE4 = KEYBOARD_LINE0+4
KEYBOARD_LINE5 = KEYBOARD_LINE0+5
KEYBOARD_LINE6 = KEYBOARD_LINE0+6
KEYBOARD_LINE7 = KEYBOARD_LINE0+7
KEYBOARD_LAST_LINE = KEYBOARD_LINE7


TITLE_ROW = 9
LEFT_MARGIN = 2
CENTER_MARGIN = LEFT_MARGIN+11

INSTR_ROW = TITLE_ROW+1

TEST_ROW = INSTR_ROW+1
TEST_COL = 17
TEST_COL_HI = TEST_COL+0
TEST_COL_LO = TEST_COL+1

NAME_ROW = TEST_ROW+1
INST_ROW = NAME_ROW+1
LINE1 = NAME_ROW+2
LINE2 = LINE1+1
LINE3 = LINE1+2
LINE4 = LINE1+3
LINE5 = LINE1+4
LINE6 = LINE1+5
LINE7 = LINE1+6
LINE8 = LINE1+7
LINE9 = LINE1+8
LINE10= LINE1+9
LINE11= LINE1+10
LINE12= LINE1+11
LINE13= LINE1+12
LINE14= LINE1+13
LINE15= LINE1+14
LINE16= LINE1+15        ;last visible line
LINE17= LINE1+16        ;last line of the nametable but likely cut from overscan
LAST_VIS_LINE = LINE16
LAST_LINE = LINE17

NEXT_PAGE = 32*3 ;adding next page to the row will draw in the 4th name table so ends up on 2nd page regardless of whether H/V mirroring

LINE_LIMIT = 28 ;max appears to be 30 based on current nmi routine
;instead let's limit to 28 which gives buffer and should be safe from overscan

CURSOR_INIT_Y = TEST_ROW
CURSOR_INIT_X = TEST_COL_LO




.byte "I", $01

.word load_hook
.word presave_hook
.word postsave_hook
.word ext_hook

UFLASH_START = $5000
;SAVERAM_START = $7A00 ;will stomp vectors of blockram testrom
SAVERAM_START = $7900
;SAVERAM_START = $0100


.scope ufl
.include "ufl_routines.s"
.endscope ;ufl

.proc load_hook

	;blindly copy save data from userflash to PRG-RAM
	ldx	#0

	@next_copy:

		lda	UFLASH_START	+$000, X
		sta	SAVERAM_START	+$000, X
		lda	UFLASH_START	+$100, X
		sta	SAVERAM_START	+$100, X
		lda	UFLASH_START	+$200, X
		sta	SAVERAM_START	+$200, X
		lda	UFLASH_START	+$300, X
		sta	SAVERAM_START	+$300, X
		lda	UFLASH_START	+$400, X
		sta	SAVERAM_START	+$400, X
		lda	UFLASH_START	+$500, X
		sta	SAVERAM_START	+$500, X

		inx
		bne	@next_copy


rts
.endproc ;load_hook

.proc presave_hook

rts
.endproc ;presave_hook

.proc postsave_hook
@saveram_ptr = reg0 ;input arg from calling function this points to the page of PRG-RAM that was just updated
@src_ptr = reg5	;-reg6 calling function is using $00-04
@cur_page = reg7 ;doesn't need to be ZPage

	;store save data into userflash
			;backup entirety of PRG-RAM into userflash
			;;flashing indication
			;mva_ptr ptr0, userflash_line2_str1
			;ldx #LEFT_MARGIN
			;ldy #LINE5
			;jsr ppu_write_string
		
			;jsr ppu_update

			;run CLR Page latches, write page latches, PEP, ERASE, WRITE steps separately
		
	;disable nmi
	lda #%00000000 ;sprites PT0
	sta $2000 ; set horizontal nametable increment

	;mva_ptr ptr0, $6000 ;source PRG-RAM
;	mva_ptr @src_ptr, SAVERAM_START

	lda #0
	sta @cur_page

	mva_ptr ufl::ufl_ptr0, SAVERAM_START ;source
		
	@next_page:
		;don't think this is actually needed if writting full page but can't hurt..
		jsr ufl::clear_page_latches

		;write the page latches, just need to memcpy into page latch address buffer range
		;mva_ptr ptr0, UFLASH_PAGE_BUFF ;source
		;mva_ptr ptr1, UFLASH_PGL_BASE ;dest
		;mva_ptr ufl::ufl_ptr0, SAVERAM_START ;source
		mva_ptr ufl::ufl_ptr1, UFLASH_PGL_BASE ;dest
		;page isn't selected here, it's selected during erase/write sequences
		ldy #0 ;func arg: Y=0: 256 bytes to copy from ptr0->ptr1
		jsr ufl::ram_memcpy_max256

		;Pre-Program bit is set, now run write sequence with Tpep to complete precharge step
		ldx #UFLASH_MODE_WRITE ;MODE=11xx, may as well use same MODE_WRITE, but could use MODE_WRITE | MODE_PEP for visibility since bit 1&0 don't matter..
		lda #UFLASH_TPEP ;meas Xus
		ldy @cur_page	;Xpage to erase
		jsr ufl::run_sequence

		;ptr0 already updated (source data for writes to page latches)
		ldx #UFLASH_MODE_ERASE
		lda #UFLASH_TPEE ;meas Xus
		;lda #UFLASH_TPEE_SPECIAL ;special case 0msec = write page buff during erase delay
		ldy @cur_page	;Xpage to erase
		jsr ufl::run_sequence

		ldx #UFLASH_MODE_WRITE
		lda #UFLASH_TPEW ;meas Xus
		ldy @cur_page	;Xpage to erase
		jsr ufl::run_sequence

		;update page & pointers
		inc ufl::ufl_ptr0_hi
		inc @cur_page
		lda @cur_page
		cmp #6 ;$7A00-7FFF = 6 pages total (0-5)
		bne @next_page

	;re-enable nmi
	;Pinobatch, "For now you can write out $80 and the game will sort itself out"
	lda #%10000000 ;sprites PT0
	sta $2000 ; set horizontal nametable increment
rts
.endproc ;postsave_hook

.proc ext_hook


rts
.endproc ;ext_hook



UFLASH_VARS:
.asciiz "INL_FLASH_LIB_END"


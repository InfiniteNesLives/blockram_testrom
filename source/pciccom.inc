
PCICCOM_BASE = $4020
PCICCOM_R0 = PCICCOM_BASE+0 ;only readable register
PCICCOM_R1 = PCICCOM_BASE+1 ;this & below are write only
PCICCOM_R2 = PCICCOM_BASE+2
PCICCOM_R3 = PCICCOM_BASE+3
PCICCOM_R4 = PCICCOM_BASE+4
PCICCOM_R5 = PCICCOM_BASE+5
PCICCOM_R6 = PCICCOM_BASE+6
PCICCOM_R7 = PCICCOM_BASE+7



SPI_CMD_DUMMY =  1       ;invert spi_buff+1 write back reply
SPI_CMD_GPIO  =  2       ;spi_buff+1 = PIN number, +2 = PIN direction, +3 = output value (if DIR==OUT), reply with "DONE" or read "VALUE"
 SPI_ESPPD_PIN =  1
 SPI_ESPBL_PIN =  2
SPI_CMD_EEP   =  3       ;read/write eeprom, spi_buff+1 RD/WR, +2 eeprom address, +3 value to write
 SPI_EEP_WR    =  $A5 ;special key to write else it's read
SPI_CMD_MAP   =  4       ;load the mapper register from provided arg (not from eeprom)
                                ;spi_buff+0 number of bits to write (max 16), +2+3 16bit value to write to mapper register
                                ;should be able to support larger mapper registers with multipe writes
SPI_CMD_MAPMODE = 5       ;set/clear the FPGA mapper lock register based on spi_buff+1 value (require special key?)
MAPMODE_KEY1 = $5A
MAPMODE_KEY2 = $3C


;have to include a copy of all the system routines we need to make this portable without PRG-ROM/RAM to execute from
.include "ram_system.s"

.include "userflash.inc"
.include "userflash_routines.s"


;This code runs from system RAM
;cannot assume userflash is present & readable because it won't be readable during erase/writes..


userflash_line2_str:
.asciiz "SEL all steps pg0"

userflash_line2_str1:
.asciiz "..."

userflash_line3_str:
.asciiz "CPL WPL PEP ERA WRI"

userflash_line4_str:
.asciiz "ran"

userflash_line5_str:
.asciiz "SEL PRG-RAM->USERFLASH"

userflash_line7_str:
.asciiz "LOAD PRES POST EXT"

.proc userflash_init

	;running from SRAM indication
	mva_ptr ptr0, userflash_line2_str
	ldx #LEFT_MARGIN
	ldy #LINE2
	jsr ppu_write_string

	jsr ppu_update

	mva_ptr ptr0, userflash_line3_str
	ldx #LEFT_MARGIN
	ldy #LINE3
	jsr ppu_write_string

	jsr ppu_update

	mva_ptr ptr0, userflash_line5_str
	ldx #LEFT_MARGIN
	ldy #LINE5
	jsr ppu_write_string

	jsr ppu_update

	mva_ptr ptr0, userflash_line7_str
	ldx #LEFT_MARGIN
	ldy #LINE7
	jsr ppu_write_string

	jsr ppu_update


	;initialize regs
	lda #0
	sta UFLASH_MODE
	sta UFLASH_SEQ
	sta UFLASH_XPAGE
	sta UFLASH_CLKCTL
;	sta UFLASH_ADDR
;	sta UFLASH_DATA

	;switch function pointer to run test for future frames
	mva_ptr test_fptr, userflash_run

;flow through to run code below!
.endproc ;userflash_init

.proc userflash_run
@cur_page = reg1


	;press select to run test
	lda gamepad_new
	and #PAD_SELECT
	bne :+
		jmp @check_start
	:

		lda cursor_y
		cmp #LINE2
		bne @check_backup_all

			;load page of data to be written into ram buffer
			ldx #$01
			@wr_next_byte:
				txa
				clc
				rol
				rol
				rol
				rol
				rol

				sta UFLASH_PAGE_BUFF, X

				inx
				bne @wr_next_byte

			;flashing indication
			mva_ptr ptr0, userflash_line2_str1
			ldx #LEFT_MARGIN
			ldy #LINE2
			jsr ppu_write_string
		
			jsr ppu_update

			;run CLR Page latches, write page latches, PEP, ERASE, WRITE steps separately
		
			;disable nmi
			lda #%00000000 ;sprites PT0
			sta $2000 ; set horizontal nametable increment
		
			;don't think this is actually needed if writting full page but can't hurt..
			jsr clear_page_latches

			;set ptr0 to point to location of data would like to write (ie page buffer)
		
			mva_ptr ptr0, UFLASH_PAGE_BUFF ;source
			mva_ptr ptr1, UFLASH_PGL_BASE ;dest
			;ldx #$00 ;flash page number
			;stx UFLASH_XPAGE
			;jsr write_page_latches
			ldy #0 ;func arg: Y=0: 256 bytes to copy from ptr0->ptr1
			jsr ram_memcpy_max256
		
			;X = MODE
			;A = Tpe spec (bit7 clear use delay_reg0, bit7 set use delay_Xms)
			;.proc run_sequence

			;PRE-PROGRAM seems to only set all bits of the flash when writting regardless of page latch value
			;ldx #UFLASH_MODE_PEP
			;lda #UFLASH_TPEP	;meas 200us
			;jsr run_sequence
			;precharge by setting the precharge bit, then running write seq with TPEP
			;run sequence does this for us now if delay = TPEP
			;ldx #UFLASH_MODE_PEP
			;ldy #UFLASH_MODE_PEP
			;jsr change_mode
			;Pre-Program bit is set, now run write sequence with Tpep to complete precharge step
			ldx #UFLASH_MODE_WRITE ;MODE=11xx, may as well use same MODE_WRITE, but could use MODE_WRITE | MODE_PEP for visibility since bit 1&0 don't matter..
			lda #UFLASH_TPEP ;meas Xus
			ldy @cur_page	;Xpage to erase
			jsr run_sequence
		
			;repeat for erase & write..
		
			mva_ptr ptr0, UFLASH_PAGE_BUFF ;source
			ldx #UFLASH_MODE_ERASE
			;lda #UFLASH_TPEE ;meas Xus
			;lda #UFLASH_TPEE-3+$40 ;meas Xus
			lda #$80 ;special case 0msec = write page buff during erase delay
			ldy #0	;Xpage to erase
			jsr run_sequence

			ldx #UFLASH_MODE_WRITE
			lda #UFLASH_TPEW ;meas Xus
			ldy #0	;Xpage to write
			jsr run_sequence

			;done indication
			mva_ptr ptr0, userflash_line4_str
			ldx #LEFT_MARGIN
			ldy #LINE2
			jsr ppu_write_string
		
			jmp @exit

		@check_backup_all:
		cmp #LINE5
		bne @check_fq_test

			;backup entirety of PRG-RAM into userflash
			;flashing indication
			mva_ptr ptr0, userflash_line2_str1
			ldx #LEFT_MARGIN
			ldy #LINE5
			jsr ppu_write_string
		
			jsr ppu_update

			;run CLR Page latches, write page latches, PEP, ERASE, WRITE steps separately
		
			;disable nmi
			lda #%00000000 ;sprites PT0
			sta $2000 ; set horizontal nametable increment

			mva_ptr ptr0, $6000 ;source PRG-RAM

			lda #0
			sta @cur_page
		
			@next_page:
				;don't think this is actually needed if writting full page but can't hurt..
				jsr clear_page_latches

				;write the page latches, just need to memcpy into page latch address buffer range
				;mva_ptr ptr0, UFLASH_PAGE_BUFF ;source
				mva_ptr ptr1, UFLASH_PGL_BASE ;dest
				;page isn't selected here, it's selected during erase/write sequences
				ldy #0 ;func arg: Y=0: 256 bytes to copy from ptr0->ptr1
				jsr ram_memcpy_max256

				;precharge by setting the precharge bit, then running write seq with TPEP
				;run sequence does this for us now if delay = TPEP
				;ldx #UFLASH_MODE_PEP
				;ldy #UFLASH_MODE_PEP
				;jsr change_mode

				;Pre-Program bit is set, now run write sequence with Tpep to complete precharge step
				ldx #UFLASH_MODE_WRITE ;MODE=11xx, may as well use same MODE_WRITE, but could use MODE_WRITE | MODE_PEP for visibility since bit 1&0 don't matter..
				lda #UFLASH_TPEP ;meas Xus
				ldy @cur_page	;Xpage to erase
				jsr run_sequence

				;ptr0 already updated (source data for writes to page latches)
				ldx #UFLASH_MODE_ERASE
				lda #UFLASH_TPEE ;meas Xus
				;lda #UFLASH_TPEE_SPECIAL ;special case 0msec = write page buff during erase delay
				ldy @cur_page	;Xpage to erase
				jsr run_sequence
	
				ldx #UFLASH_MODE_WRITE
				lda #UFLASH_TPEW ;meas Xus
				ldy @cur_page	;Xpage to erase
				jsr run_sequence

				;update page & pointers
				inc ptr0_hi
				inc @cur_page
				lda @cur_page
				cmp #32 ;$6000-7FFF = 32 pages total (0-31)
				bne @next_page

			;done indication
			mva_ptr ptr0, userflash_line4_str
			ldx #LEFT_MARGIN
			ldy #LINE5
			jsr ppu_write_string

			jmp @exit

		@check_fq_test:
		cmp #LINE7
		bne @check_start

			;which step was selected?
			lda cursor_x
			;.asciiz "LOAD PRES POST EXT"
			cmp #LEFT_MARGIN+0
			bne :+	;LOAD
				jmp ($7002)
			:
			cmp #LEFT_MARGIN+5
			bne :+	;PRES

				jmp ($7004)
			:
			cmp #LEFT_MARGIN+10
			bne :+	;PRES

				jmp ($7006)
			:
			cmp #LEFT_MARGIN+15
			bne :+	;EXT

				jmp ($7008)
			:

	@check_start:
	lda gamepad_new
	and #PAD_START
	bne @start_pressed
		@jmp_exit:
		jmp @exit
	@start_pressed:

	
		;disable nmi
		lda #%00000000 ;sprites PT0
		sta $2000 ; set horizontal nametable increment
		lda cursor_y
		cmp #LINE3
		bne @jmp_exit

			lda #0
			sta UFLASH_MODE
			sta UFLASH_SEQ
			sta UFLASH_XPAGE
			sta UFLASH_CLKCTL
			sta UFLASH_REG_BASE+4
			sta UFLASH_REG_BASE+5

		;which step was selected?
		lda cursor_x
		;.asciiz "CPL WPL PEP ERA WRI"
		cmp #LEFT_MARGIN+0
		bne :+
			jsr clear_page_latches
			ldx #LEFT_MARGIN+0
			jmp @display_run
		:
		cmp #LEFT_MARGIN+4
		bne :+
			mva_ptr ptr0, UFLASH_PAGE_BUFF ;source
			jsr write_page_latches
			ldx #LEFT_MARGIN+4
			jmp @display_run
		:
		cmp #LEFT_MARGIN+8
		bne :+
			;precharge by setting the precharge bit, then running write seq with TPEP
			;ldx #UFLASH_MODE_PEP
			;ldy #UFLASH_MODE_PEP
			;jsr change_mode
			;run sequence does this for us now if delay = TPEP
			;Pre-Program bit is set, now run write sequence with Tpep to complete precharge step
			ldx #UFLASH_MODE_WRITE ;MODE=11xx, may as well use same MODE_WRITE, but could use MODE_WRITE | MODE_PEP for visibility since bit 1&0 don't matter..
			lda #UFLASH_TPEP ;meas Xus (also used to tell if Pre-program bit needs set before starting)
			ldy @cur_page	;Xpage to erase
			jsr run_sequence
			ldx #LEFT_MARGIN+8
			jmp @display_run
		:
		cmp #LEFT_MARGIN+12
		bne :+
			ldx #UFLASH_MODE_ERASE
			lda #UFLASH_TPEE ;meas Xus
			ldy #0	;Xpage to erase
			jsr run_sequence
			ldx #LEFT_MARGIN+12
			jmp @display_run
		:
		cmp #LEFT_MARGIN+16
		bne :+
			ldx #UFLASH_MODE_WRITE
			lda #UFLASH_TPEW ;meas Xus
			ldy #0	;Xpage to write
			jsr run_sequence
			ldx #LEFT_MARGIN+16
			jmp @display_run
		:

		;no match
		jmp @exit

		@display_run:
	
		mva_ptr ptr0, userflash_line4_str
		;ldx #LEFT_MARGIN
		ldy #LINE4
		jsr ppu_write_string

@exit:

	
		;initialize regs
		lda #0
		sta UFLASH_MODE
		sta UFLASH_SEQ
		sta UFLASH_XPAGE
		sta UFLASH_CLKCTL
			sta UFLASH_REG_BASE+4
			sta UFLASH_REG_BASE+5


	;enable nmi
	lda #%10000000 ;sprites PT0
	sta $2000 ; set horizontal nametable increment


rts
.endproc ;userflash_run



; example.s -> alwa.s
; Brad Smith (rainwarrior), 4/06/2014
; http://rainwarrior.ca
;

;macros
.MACPACK generic
.MACPACK longbranch
.feature force_range	;forces literals to fit in a byte so don't have to use <

; iNES header
.segment "HEADER"
.include "ines_header_NESNET_32K.inc"

;CHR-ROM
;.segment "TILES"
;.include "pattern_tables.inc"



;;;;;;;;;;;;;;;;;;;;;;;;;;;
;2KB main board CPU ram
;;;;;;;;;;;;;;;;;;;;;;;;;;;

.segment "ZEROPAGE"
;//1st page of RAM $0000-00FF
zp:
.include "zp.inc"

;// HW stack
;//2nd page of RAM $0100-01FF
;//256Byte decending stack, SP initialized in reset routine
.segment "STACK"
stack: .res 256

;//Sprite OAM buffer
;//3rd page of RAM $0200-02FF
.segment "OAM"
oam: .res 256        ; sprite OAM data to be uploaded by DMA

;//Remaining RAM
.segment "BSS"
.include "ram.inc"


;;;;;;;;;;;;;;;;;;;;;;;;;;;
;PRG-ROM data & code
;;;;;;;;;;;;;;;;;;;;;;;;;;;


.include "macros.s"
.include "mapper_macros.s"

.segment "USERFLASH_RAMCODE"
;.incbin "data\lfsr_p3_1KB.bin"
.include "userflash_ramcode.s"

.segment "USERFLASH_DATA0"
;$8000-9FFF normally this is where PRG-RAM back up is stored
;.incbin "include\ramcode.bin"
.incbin "data\lfsr_p1.bin"
.segment "USERFLASH_DATA1"
.incbin "data\lfsr_p2.bin"

.segment "USERFLASH_ROMCODE"
;$D000-DFFF (mirrored again E000-EFFF on hardware)
.include "test_data.inc"
.include "pciccom.s"
.include "checksum.s"
.include "userflash.s"


.include "ppu_data.inc"
.include "ascii.inc"

.include "display.inc"
.include "esp_at.inc"
.include "pciccom.inc"

.include "esp_at.s"

.include "qspi_pump.inc"
.include "qspi_pump.s"

.segment "PRGTILES"
ascii_tiles:
;.incbin "graphics\alphanum_64tiles.chr"
.incbin "graphics\AlphaNESdoug_paulmod_128tiles.chr"
ascii_tiles_end:

.segment "RODATA"
prg_bank_table: ;only needed in last bank if always calling from and returning to last bank
;Therefore called subroutines in other banks can't call subroutines in other banks
;they can only call subroutines in their banks, and return to the last bank
;MUST also be aligned at begining of page, so best place to put it is at beinging of the bank
;.IFDEF PRG512
;.byte $00, $01, $02, $03, $04, $05, $06, $07, $08, $09, $0A, $0B, $0C, $0D, $0E, $0F   ;16banks of PRG-ROM CHR bank0
;.byte $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $1A, $1B, $1C, $1D, $1E, $1F   ;16banks of PRG-ROM CHR bank1
;.byte $20, $21, $22, $23, $24, $25, $26, $27, $28, $29, $2A, $2B, $2C, $2D, $2E, $2F   ;16banks of PRG-ROM CHR bank2
;.byte $30, $31, $32, $33, $34, $35, $36, $37, $38, $39, $3A, $3B, $3C, $3D, $3E, $3F   ;16banks of PRG-ROM CHR bank3
;.ELSE
;.byte $00, $01, $02, $03  ;4banks of PRG-ROM
;.ENDIF
;LAST_PRG_BANK = .bank(*) ;code goes in this bank
;prg_bank_id:
;.byte LAST_PRG_BANK
;.include "ppu_data.inc"
;.include "ascii.inc"
;
;.include "display.inc"
;.include "esp_at.inc"
;.include "pciccom.inc"


.segment "BRAM_CODE"
;.segment "CODE"
.include "uflash_lib.s"


.segment "CODE"
.include "tv_region.s" ;contains align 32 to ensure timing without page boundary variable timing
.include "system.s"

.include "nmi.s"
.include "irq.s"
.include "reset.s"
.include "main.s"
.include "ppu_routines.s"
.include "gamepad.s"
.include "cursor.s"
.include "display.s"
.include "controller_test.s"
.include "peekpoke.s"
;.include "nesnet.s"
;.include "esp_at.s"

;.include "famitone.s"
.include "famitone2/famitone2.inc"
.include "song.s"
;.include "sfx.s"

;.segment "BANK00"
;PT_BANK = .bank(*) ;define used for code in these banks so it doesn't swap PRG banks when changing CHR banks
;chr_bank_table ;we can only get back to the last bank, but can be any CHR bank
;prg_bank_id:
;.byte PT_BANK
;.include "pattern_routines.s" ;routines to load tiles needs to be in same bank
;.include "pattern_tables.inc" ;tiles need to be in PRG-ROM when using CHR-RAM


;.segment "BANK01"
;NT_BANK = .bank(*) ;define used for code in these banks so it doesn't swap PRG banks when changing CHR banks
;chr_bank_table ;we can only get back to the last bank, but can be any CHR bank
;prg_bank_id:
;.byte NT_BANK
;.include "nt_routines.s";routines to load tiles needs to be in same bank
;.include "nt_data.inc" ;tiles need to be in PRG-ROM when using CHR-RAM


;.segment "BANK02"
;MAP_BANK = .bank(*) ;define used for code in these banks so it doesn't swap PRG banks when changing CHR banks
;chr_bank_table ;we can only get back to the last bank, but can be any CHR bank
;prg_bank_id:
;.byte MAP_BANK
;.include "map_routines.s";routines to load tiles needs to be in same bank
;.include "rooms.inc"

; vectors placed at top 6 bytes of memory area
;.IFDEF MAP_CNROM
;.segment "VECTORS"
;.word nmi
;.word reset
;.word irq
;.ENDIF

;segments defined in stub.s depending on mapper
;includes reset vectors
.include "stub.s"

;ram definitions that depend on code labels, so needs to come after CODE segment
.segment "BSS"
.include "ram_end.inc"


;;;;;;;;;;;;;;;;;;;
; ca65 NOTES
;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;
;.IFDEF COLLISION_OPTIMIZE	;code included if defined to anything
;.IFNBLANK COLLISION_OPTIMIZE ;code included if defition doesn't exist
;.IF FOOBAR = 1	 ;code included if FOOBAR was defined as 1


SPR = 4

YPOS = 0
TILE = 1
ATTR = 2
XPOS = 3

Y_OFFSCREEN = $FF

CURSOR = 1	;OAM index of first sprite
BLOCK = 2	;secondary cursor for edit mode

cursor_spr_x = oam+(CURSOR*SPR)+XPOS
cursor_spr_y = oam+(CURSOR*SPR)+YPOS
cursor_t = oam+(CURSOR*SPR)+TILE
cursor_a = oam+(CURSOR*SPR)+ATTR

block_spr_x = oam+(BLOCK*SPR)+XPOS
block_spr_y = oam+(BLOCK*SPR)+YPOS
block_t = oam+(BLOCK*SPR)+TILE
block_a = oam+(BLOCK*SPR)+ATTR

BLOCK_TILE = 4

FLIP_HORZ = $40
FLIP_VERT = $80

cursor_init:

	;starting position
	lda #CURSOR_INIT_X
	sta cursor_x

	lda #CURSOR_INIT_Y
	sta cursor_y

	;set palette numbers for all sprites

	;state

	;starting tile
	lda #'_'
	sta cursor_t

	jsr cursor_update

rts ;cursor_init

;tap Dpad nothing pressed: move cursor
;hold B, then tap Dpad: update value
;	-> This calls all functions needed to change and update display of values
cursor_state:

	lda gamepad_new
	and #PAD_U
	beq @check_down
		;PRESSED UP
		lda gamepad
		and #PAD_B
		beq :+
			lda #1
			jsr update_cursor_val
			jmp @done
		:
		dec cursor_y
		jmp @reset_blink

	@check_down:
	lda gamepad_new
	and #PAD_D
	beq @check_left
		;PRESSED DOWN
		lda gamepad
		and #PAD_B
		beq :+
			lda #-1
			jsr update_cursor_val
			jmp @done
		:
		inc cursor_y
		jmp @reset_blink

	@check_left:
	lda gamepad_new
	and #PAD_L
	beq @check_right
		;PRESSED LEFT
		lda gamepad
		and #PAD_B
		beq :+
			lda #-4
			jsr update_cursor_val
			jmp @done
		:
		dec cursor_x
		jmp @reset_blink

	@check_right:
	lda gamepad_new
	and #PAD_R
	beq @done
		;PRESSED RIGHT
		lda gamepad
		and #PAD_B
		beq :+
			lda #4
			jsr update_cursor_val
			jmp @done
		:
		inc cursor_x
		jmp @reset_blink

	@reset_blink:
		lda #0
		sta cursor_blink
		jmp @done

	@done:
rts ;cursor_state

cursor_update:

	lda cursor_y
	;check for limits to manually wrap while keeping accurate position
	;valid range is 0-29
	bpl :+
		lda #29
		sta cursor_y
	:
	cmp #30
	bne :+
		lda #0
		sta cursor_y
	:
	mul a, #8
	sta cursor_spr_y

	lda cursor_x
	;mask out upper bits so wrapping around screen keeps accurate cursor position
	and #$1F
	sta cursor_x
	mul a, #8
	sta cursor_spr_x

	lda cursor_blink
	add #$04
	sta cursor_blink
	bpl @visible
		lda #Y_OFFSCREEN
		sta cursor_spr_y

	@visible:
rts ;cursor_update


;A = inc/dec val
;cursor_x/y -> value to update
update_cursor_val:

	pha

	;translate row to aptr0-3 -> X, then modify based on col
	lda cursor_y
	cmp #TEST_ROW
	bne @no_match
		;test row selected check which nibble
		lda cursor_x
		cmp #TEST_COL_HI
		bne :+
			pla
			mul a, #16
			jmp @add_test
		:
		cmp #TEST_COL_LO
		bne @no_match
			pla
			@add_test:
			add cur_test
			sta next_test
			rts
	@no_match:

	;let the test check if it should do something with cursor input
	pla
	sta cursor_arg
;	cmp #APTR0_ROW
;	bne :+
;		ldx #0 ;table index
;		baeq @check_col
;	:
;	cmp #APTR1_ROW
;	bne :+
;		ldx #2 ;table index
;		bane @check_col
;	:
;	cmp #APTR2_ROW
;	bne :+
;		ldx #4 ;table index
;		bane @check_col
;	:
;	cmp #APTR3_ROW
;	bne :+
;		ldx #6 ;table index
;		bane @check_col
;	:
;	;didn't match row
;	@no_match:
;	pla
;	rts
;
;	@check_col:
;	lda cursor_x
;	cmp #APTR_ADDR_HI
;	bne :+
;		pla
;		mul a, #16
;		add aptr_table_hi, x
;		sta aptr_table_hi, x
;		jmp @done
;	:
;	cmp #APTR_ADDR_HI+1
;	bne :+
;		pla
;		add aptr_table_hi, x
;		sta aptr_table_hi, x
;		jmp @done
;	:
;	cmp #APTR_ADDR_LO
;	bne :+
;		pla
;		mul a, #16
;		add aptr_table_lo, x
;		sta aptr_table_lo, x
;		jmp @done
;	:
;	cmp #APTR_ADDR_LO+1
;	bne :+
;		pla
;		add aptr_table_lo, x
;		sta aptr_table_lo, x
;		jmp @done
;	:
;	cmp #APTR_WR_VAL
;	bne :+
;		pla
;		mul a, #16
;		add aptr_table_wr, x
;		sta aptr_table_wr, x
;		jmp @done
;	:
;	cmp #APTR_WR_VAL+1
;	bne :+
;		pla
;		add aptr_table_wr, x
;		sta aptr_table_wr, x
;		jmp @done
;	:
;
;	;no col match
;	pla
;	rts
;
;	@done:
;	jsr display_aptr

rts ;update_cursor_val:

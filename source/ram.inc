


.align 256 ;//alignment matters
palette_buff:    .res 32  ; palette buffer for PPU update
			;//All palettes always updated, TODO maybe add flag at begining
			;//and set the flag if any/all need updating to save NMI time
palette_flag = palette_buff+28	;Last byte of palette's BG color indicates what to update
;BIT instruction sets N flag indicating update needed
UPDATE_BG	= $80	;V clear only one group, bit4 clr BG (mask out bit7 -> $00 = palette address LSByte
UPDATE_SP	= $90	;V clear only one group, bit4 set SP (mask out bit7 -> $10 = palette address LSByte
UPDATE_ALL	= $C0	;BIT instruction sets V flag indicating update both
WHICH_PAL_MASK  = $10	;and this value with palette flag to get starting address LSB of which palette to update
;takes ~275 cpu cycles to update single palette, ~500 cpu cycles to update both
;if reversed the order palette updates were stored in palette buff so could be copied with a decending counter 
;would save 2 cpu cycles per update (32-64 cycles per NMI with palette update)
;leave this as low hanging fruit for later, currently don't make many palette updates so wouldn't be beneficial
;could take this step further with more code to only update 1-2 palettes per frame
;storing starting address in the flag and not really caring about duplicate backdrop palettes which usually aren't visible



test_fptr: .res 2 ;test function pointer
cur_test: .res 1
next_test: .res 1
test_running: .res 1


scroll_x: .res 1
scroll_y: .res 1
scroll_nmt: .res 1

cursor_x: .res 1
cursor_y: .res 1
cursor_blink: .res 1
cursor_arg: .res 1

cursor_mode: .res 1
cursor_edit_idx: .res 1
temp_cursor_x: .res 1
temp_cursor_y: .res 1

cur_at_test: .res 1
cur_at_test_len: .res 1


system_type:	.res 1 ;TV system (0: NTSC, 1: PAL, 2: Dendy; 3: unknown)
;skip_frame:	.res 1 ;counter from 0-5 to skip every 6th frame (frame==0) 
;rely on value of zero to know when to skip scroll/game logic
;NTSC = 0-5 (positive values, use as a counter)
NTSC = $F6 ;-10 then convert to positive
PAL = $F7 ;-9
DENDY = $F8 ;-8
;for NTSC systems, skip_frame will be 0 every sixth frame
;can lda skip_frame, and branch on Z-flag as needed for all systems

;make these 2 bytes per address so same indexing can be used as ptr lo/hi
aptr_table_wr:
aptr0_wr: .res 1
aptr_table_rd:
aptr0_rd: .res 1
aptr1_wr: .res 1
aptr1_rd: .res 1
aptr2_wr: .res 1
aptr2_rd: .res 1
aptr3_wr: .res 1
aptr3_rd: .res 1


;// nmt_update buffer MUST be aligned to begining of a page of RAM.
;Doesn't appear to be true, maybe it was for alwa loading routines..?
;.align 256
nmt_update: .res 128; nametable update entry buffer for PPU update
;there's nothing stopping us for blowing past this definition into the ram beyond..
;generally I print till it crashes, or just print every few lines.
;TODO some sort of test/fail if nmt_update_len exceeds the definition here
;could catch this in nmi maybe?  Still doesn't cover case where updating buffer when nmi
;hits..  So yeah just a delicate thing that seems to have worked without much trouble so far.
			;//Maximum ADDR HI/LO/DATA is ~81Bytes (further testing shows
			;might be closer to 256B max..?  Maybe it's due to execution time
			;if updating the buffer when nmi hits I'm sure that's bad...
			;//Maximum ADDRHI/LO DATA0, DATA1, DATA2... is ~58Bytes

;esp_msg_buff: .res 128


;ram_end.inc follows after this file's definition



example_palette:
;.byte $0F,$15,$26,$37 ; bg0 purple/pink
;.byte $0F,$09,$19,$29 ; bg1 green
;.byte $0F,$01,$11,$21 ; bg2 blue
;.byte $0F,$00,$10,$30 ; bg3 greyscale
;.byte $0F,$18,$28,$38 ; sp0 yellow
;.byte $0F,$14,$24,$34 ; sp1 purple
;.byte $0F,$1B,$2B,$3B ; sp2 teal
;.byte $0F,$12,$22,$32 ; sp3 marine

;BG
.byte $0f,$0b,$19,$38,$0f,$07,$2d,$10,$0f,$07,$17,$37,$0f,$1c,$2c,$30
;SP
.byte $0f,$0f,$13,$30	;//Zoe's palette black, purple, white
;//.byte $0f,$2d,$13,$30	;//Zoe's palette dk grey, purple, white
.byte $0f,$27,$2d,$10	;//Zoe's skin palette, uses entry 1 only
.byte $0f,$07,$17,$37
;.byte $0f,$1c,$2c,$30

;upper nibble of last backdrop is flag value
.byte $Cf,$1c,$2c,$30 ;$Cx= update both palettes
;.byte $8f,$1c,$2c,$30 ;$8x= update lower palettes (BG only)
;.byte $9f,$1c,$2c,$30 ;$9x= update upper palettes (SP only)



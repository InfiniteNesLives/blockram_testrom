
;	;controller state
;	mva_ptr ptr0, ctlr_string
;	ldx #4
;	ldy #CTLR_ROW
;	jsr ppu_write_string
;

ctlr_test:

	lda gamepad
	cmp gamepad_last
	beq @check_pad2
		ldx #LEFT_MARGIN
		ldy #NAME_ROW+1
		jsr display_hex_byte

	@check_pad2:
	lda gamepad2
	cmp gamepad2_last
	beq @done
		ldx #LEFT_MARGIN+4
		ldy #NAME_ROW+1
		jsr display_hex_byte

	@done:

	;test never stops even if press start
	lda #1
	sta test_running

rts;ctlr_test:

;
;;prereq: PPU rendering must be disabled
;;	PPUCTRL assumes vram addr inc by 1
;;	clears all 4KB of onboard NT visible memory, this doesn't
;;	support extra NT's or single screen mapper selectable mirroring
;;stomps A, X, & Y
;clear_all_nametables:
;	lda	PPUSTATUS	;reset latch
;	lda	#0x20		;NT0 $2000
;	sta	PPUADDR
;	lda	#0x00
;	sta	PPUADDR
;
;	;now CPU can access PPU bus via PPUDATA
;	;PPU address will increment by 1 for each access
;	;this is was done back in the reset routine
;
;	;empty current name & attribute table
;	lda	#0xFF	;tile number for clearing
;	ldy	#0x80	;(30 rows + attr table) * 4 screens = 128
;	
;	clear_row:
;		ldx #32	;32 cols
;		next_entry:
;			sta	PPUDATA
;			dex
;			bne	next_entry
;		dey
;		bne	clear_row
;
;rts
;
;;prereq: PPU rendering must be disabled
;;	PPUCTRL assumes vram addr inc by 1
;;	clears all 4KB of onboard NT visible memory, this doesn't
;;	support extra NT's or single screen mapper selectable mirroring
;;stomps A, X, & Y
;clear_all_chrram:
;	lda	PPUSTATUS	;reset latch
;	lda	#0x00		;PT0
;	sta	PPUADDR
;	lda	#0x00
;	sta	PPUADDR
;
;	;now CPU can access PPU bus via PPUDATA
;	;PPU address will increment by 1 for each access
;	;this is was done back in the reset routine
;
;	;empty current name & attribute table
;	lda	#0x00	;tile data for clearing (0=universal background color)
;	ldy	#0x80	;64 * 128 screens = 8KB
;	
;	clear_64B:
;		ldx #64	
;		clear_byte:
;			sta	PPUDATA
;			dex
;			bne	clear_byte
;		dey
;		bne	clear_64B
;
;rts
;
;;initializes palette to nearby colors as conveient
;init_palettes:
;	lda	PPUSTATUS	;reset latch
;	lda	#0x3F
;	sta	PPUADDR
;	lda	#0x00
;	sta	PPUADDR
;
;	;pointing to Universal background palette now
;	lda	#LT_GREY
;	ldx	#0x20		;total number of palette entries
;	next_palette:
;		sta	PPUDATA
;		adc	#1	;increment color don't care about carry
;		cmp	#0x1D	;don't want multiple blacks
;		bne	next_color
;			adc	#3	;skip past black colors
;		next_color:
;		dex	
;		bne	next_palette
;	;universal background gets written for each palette
;	;so it was corrupted, fix now, abusing rollover of palette mirror
;	;$3F20 should mirror back to $3F00
;	lda	#BLACK
;	sta	PPUDATA
;
;rts
;
;;prereq: PPU rendering must be disabled
;;	PPUCTRL assumes vram addr inc by 1
;;stomps A, X, & Y
;load_64tiles_chrram:
;	lda	PPUSTATUS	;reset latch
;	lda	#0x00		;PT0 $0000
;	sta	PPUADDR
;	lda	#0x00
;	sta	PPUADDR
;
;	;now CPU can access PPU bus via PPUDATA
;	;PPU address will increment by 1 for each access
;	;this is was done back in the reset routine
;
;	;set ptr0 to point to begining of tile data
;	lda	#TILE_DATA_L
;	sta	ptr0
;	lda	#TILE_DATA_H
;	sta	ptr0+1
;
;	;empty current name & attribute table
;	ldx	#TILE_DATA_NUM	;16B per tile * 16 = 256 max count for x -> 0
;	
;	load_chrram:
;		ldy	#0	;need to load 16 bytes per dec of X
;		load_tile:
;			lda	(ptr0), y
;			sta	PPUDATA
;			iny
;			cpy	#16
;			bne	load_tile
;			;tile copied, update ptr0
;			tya
;			;carry set from cpy == 16
;			clc
;			adc	ptr0
;			sta	ptr0
;			bcc	no_carry
;				inc ptr0+1
;			no_carry:
;		dex
;		bne	load_chrram
;
;rts
;
;;writes value in A to screen
;;PPU address to write X/Y
;;assumes tile graphic matches number
;write_value:
;
;	pha
;	lda	PPUSTATUS	;reset latch
;	stx	PPUADDR
;	sty	PPUADDR
;	pla
;	tax
;	and	#0xF0	;mask out upper nibble
;	clc
;	ror	
;	clc
;	ror	
;	clc
;	ror	
;	clc
;	ror	
;	;now a contains the tile number to display
;	sta	PPUDATA
;	;now need to get lower nibble
;	txa
;	and	#0x0F
;	sta	PPUDATA
;
;rts


;A contains data to write
;X contains CICOp register number
;Y gets stomped
	;perhaps use Y to designate read/write only
	;perhaps load Y with upper CICOp register nibble to simplify preparations
;cicop_reg gets stomped and isn't valid on exit
cicop_write_read:


	;CAUTION care needs to be excercised to ensure the 6502 doesn't get interrupted durring this routine
	;all instructions are timing sensitive while CICOp enable bit is set in mapper reg
	;during this time the CICOp is in TLI "top level interrupt" and assumes timing of all instructions of this routine

	;it actually makes most sense to execute this routine from SRAM so it's self modifiable allowing 
	;this one routine to access multiple CICOp registers. 
	;Instead of this I've chosen to use Y register to index the CICOp register being accessed
	;this however only provides access to 16 CICOp registers....
	;Using ZP indirect pointer with 

	;store A on stack twice so we can retrieve it to setup write data
	pha
	pha

	;store cicop reg currently in X into ZP cicop_reg "pointer"
	;lower byte, lower nibble contains lower nibble of cico reg number
	;upper byte, lower nibble contains upper nibble of cico reg number
	stx	cicop_reg	;low nibble of CICOp reg num stored (D7-4 isn't visible to CICOp)
	txa	
	ror
	ror
	ror
	ror	;upper nibble shifted to lower nibble
	;need to point to somewhere in 6502 memory map that won't conflict with anything
	and	#0x0F
	ora	#CICOP_PORT_BYTE
	sta	cicop_reg+1	;upper nibble of CICOp reg num stored, and range that doesn't conflict.

	;retrieve write data from the stack
	pla
	ror
	ror
	ror
	ror
	tax	;upper nibble stored in X's lower nibble
	pla	;lower nibble stored in A's lower nibble

	;mapper CICOp bit is rising edge sensitive only
	;so we can clear it now, and set it to TLI the CICOp and not worry about clearing it
	;it will get cleared next entry to this routine
	;this is helpful as it ensures we control the edge
	ldy	#CICOP_BANK_DIS 
	sty	CICOP_ADDR_DIS 	;8C 00 C0 
	ldy	#CICOP_BANK_EN 	;A0 80 

	;trigger CICOp to start transfer operation:
;WARNING!!!  CICOp is snooping/sniffing the CPU Data bus lower nibble (D3-0) for all
; 6502 execution after this mapper register write.  Changing anything other than what's
; listed as don't care will break things and CICOp won't properly read/write data
	sty	CICOP_ADDR_EN		
	;8C 80 C0 

;TODO this would work better as self modifying code ran from SRAM
;could use LDA $5*x* with useful 6502->STM8 data in * addresses, and STM8 reply being loaded into A
;following self modifying code (store data in the lower nibbles of AddrHi and AddrLo)
;would "write" 3 bytes to the STM8, and "read" 3 nibbles from STM8 
;LDA $5*x*
;LDX $5*x*
;LDY $5*x*
;3 nibble reply is in A/X/Y (lower)
;this wouldn't have to be self modifying running from SRAM, but to make the data variable it would need to be.
;if didn't need to read data from STM8, could put data in A/X/Y before hand and use STA/X/Y (3 more nibbles of data)
;maybe always LDY at end for error checking, STA & STX could send another byte though.
;If created an SRAM data structure that simply stored data in RAM where desired (within the code) it
;would be pretty damn quick..
;Consider using BIT polling before sending data to allow STM8 to control when it's ready to send data?
;Would help quite a bit if STM8 had D7 and/or D6 though for V&S copying with BIT.
;IDK if annoyance of moving STM8 to upper nibble is worth it though...?  Maybe it doesn't matter
;if really wanting to send bytes, does matter whether shifting upper/lower?
;if adopting 4bit data structure, does it really matter?  Kind of since inc/dec on those vars is less useful
;however using the upper nibble does make the carry flag more useful when data is in upper nibble!
;using upper nibble means that can't store a full byte in LDA $5*x* though...
;BIT is still usable with lower nibble by putting desired mask in A.  If always finishing with load for error
;checking, maybe best to use LDA last, and could have STX & STY first.  That way don't care about trying to put
;data & BIT mask in A.  Maybe BIT testing with a full nibble is better insurance for timing alingment/dejittering.
;Considering all this, I think sticking with D0-3 is best.
;
;Using this store data in LoaD address method, it's pretty easy to send a long stream of data to the STM8.
;Getting a long stream of data would require interleaved LDA STA $ZP ideally for speed, but could be anywhere.
;PHA is same 2B 3C as STA $ZP though.  Maybe using the stack for STM8->6502 incoming data is most versatile.



	;first allow CICOp to sniff reg number, and write low nibble of data
	sta	(cicop_reg), y		;y doesn't actually matter CICOp can't see it
	;91 04 

	;now the CICOp has register H:L from ZP sniffing, and data L from A lower nibble

	;write upper nibble, contained in lower nibble of X
	stx	cicop_reg		;doesn't actually matter what ZP byte is written to
	;86 04 
	;cicop_reg gets stomped could use different ZP byte to avoid this
	
	;now the CICOp has data H from sniffing store of X to ZP

	;NOTE: considering changing order of sta and stx above.
	;doing so would allow STM8 isr jitter to be resolved with CPU R/W on stx T2 potentially
	;using M2 provides some extra timing protection though bc data is valid when M2 high
	;swapping order would also let CICOp sniff ZP byte address which could be used to 
	;provide more info to CICOp, perhaps for more registers based on ZP byte lower nibble address
	;used during transfer

	;now time to read data from CICOp
	;the data is returned in specific order, 
	;the CICOp doesn't know what register (X,Y,A) the 6502 is loading into

	ldx	CICOP_PORT	;data L
	;AE 00 50 
	ldy	CICOP_PORT	;data H
	;AC 00 50 
	lda	CICOP_PORT	;data E (error/verification)
	;AD 00 50

;END timing sensitive code.  The CICOp has now freed itself to go back to whatever it was doing
;don't actually have to clear mapper enable bit.  CICOp won't trigger until next rising edge

	;check that data/error code verifies successful transfer

	;piece together data L and data H

output_data:
	pha	;push A
	tya
	pha	;push Y

	;print data L
	txa
	ldx	#0x20
	ldy	#0x64
	jsr 	write_value

	;print data H
	pla	;pop Y
	ldx	#0x20
	ldy	#0x64+32
	jsr 	write_value

	;print data E
	pla	;pop A
	ldx	#0x20
	ldy	#0x64+64
	jsr 	write_value


rts

;prereq: PPU rendering must be disabled
;setup_bkgd:
;	
;	lda	PPUSTATUS	;reset latch
;	lda	#0x20
;	sta	PPUADDR
;	lda	#0x00
;	sta	PPUADDR
;
;	;now CPU can access PPU bus via PPUDATA
;	;PPU address will increment by 1 for each access
;	;this is was done back in the reset routine
;
;	;empty current name & attribute table
;	lda	#0
;	ldy	#32	;30 rows + attr table
;	
;	clear_row1:
;		ldx #32	;32 cols
;		clear_col1:
;			sta	PPUDATA
;			dex
;			bne	clear_col1
;		dey
;		bne	clear_row1
;
;	;NT0 fill an area with checker board	
;	lda	#1
;	ldy	#8	;start at row 8
;
;	next_row:	
;		pha	;store A, clobbered by subroutine
;		ldx	#8	;start at col 8
;		jsr	ppu_addr_tile 	;arg in X/Y
;		pla	;recover A
;
;		fill_row:
;			sta	PPUDATA	
;			eor	#3
;			inx
;			cpx	#(32-8)
;			bcc	fill_row
;
;		eor	#3
;		iny
;		cpy	#(30-8)
;		bcc	next_row
;
;	;NT1 fill with simple pattern
;		
;		
;rts
;
;
;;sets PPU to ADDR @ X/Y in preparation for an PPUDATA write
;;Y=0-31   NameTable0
;;Y=32-63  NameTable1
;;Y=64-95  NameTable2
;;Y=96-127 NameTable3
;;as if the nametables were aligned in one big vertical column
;;A & reg0 clobbered, X/Y preserved
;ppu_addr_tile:
;	lda 	PPUSTATUS	;reset latch
;	tya
;	lsr
;	lsr
;	lsr
;	ora 	#$20 	;high bits of Y + $20
;	sta 	PPUADDR
;	tya
;	asl
;	asl
;	asl
;	asl
;	asl
;	sta 	reg0
;	txa
;	ora 	reg0
;	sta 	PPUADDR	;low bits of Y + X
;rts
;
;update_ppu:
;	lda	#1
;	sta	nmi_ready
;	till_done:
;		lda	nmi_ready
;		bne	till_done
;rts



;.charmap        $41, $61        ; Map 'A' to 'a'
;.charmap        $61, $41        ; Map 'a' to 'A'
;.define ASCII_OFFSET $33 ; "A" = ascii 0x41 -> tile number 0x1E = 0x41 - 0x1E = 0x33 (can't use parenthesis)
.define ASCII_OFFSET $0 ; "A" = ascii 0x41 -> tile number 0x41 = 0x41 - 0x41 = 0x00 (can't use parenthesis)
;.define NUM_OFFSET $4 ;"0" is tile number 4
.define NUM_OFFSET $30 ;"0" is tile number 0x30
HEX_OFFSET = 'A'-'9'-1;$07 ;distance between "9" and "A" (remove/comment out definition if A tile follows 9
;.define TILE_BASE $10 ;starting tile number this allows tile start to be flexible but
.define TILE_BASE $0 ;starting tile number this allows tile start to be flexible but
			;tile loading routine requires table to start at begining of tile row
			;because this value is shifted >> 4 to determine first PPU address
;NOTE!  asciiz terminates strings with zero (0x00) which is also "0" if TILE_BASE=0
;ascii numbers translate directly to tile number
;.charmap        '0',  0+NUM_OFFSET+TILE_BASE
;.charmap        '1',  1+NUM_OFFSET+TILE_BASE
;.charmap        '2',  2+NUM_OFFSET+TILE_BASE
;.charmap        '3',  3+NUM_OFFSET+TILE_BASE
;.charmap        '4',  4+NUM_OFFSET+TILE_BASE
;.charmap        '5',  5+NUM_OFFSET+TILE_BASE
;.charmap        '6',  6+NUM_OFFSET+TILE_BASE
;.charmap        '7',  7+NUM_OFFSET+TILE_BASE
;.charmap        '8',  8+NUM_OFFSET+TILE_BASE
;.charmap        '9',  9+NUM_OFFSET+TILE_BASE
;
;
;;lower case letters need CAPITALIZED then ascii and tile location offset
;.charmap        'a', 'A'-ASCII_OFFSET+TILE_BASE
;.charmap        'b', 'B'-ASCII_OFFSET+TILE_BASE
;.charmap        'c', 'C'-ASCII_OFFSET+TILE_BASE
;.charmap        'd', 'D'-ASCII_OFFSET+TILE_BASE
;.charmap        'e', 'E'-ASCII_OFFSET+TILE_BASE
;.charmap        'f', 'F'-ASCII_OFFSET+TILE_BASE
;.charmap        'g', 'G'-ASCII_OFFSET+TILE_BASE
;.charmap        'h', 'H'-ASCII_OFFSET+TILE_BASE
;.charmap        'i', 'I'-ASCII_OFFSET+TILE_BASE
;.charmap        'j', 'J'-ASCII_OFFSET+TILE_BASE
;.charmap        'k', 'K'-ASCII_OFFSET+TILE_BASE
;.charmap        'l', 'L'-ASCII_OFFSET+TILE_BASE
;.charmap        'm', 'M'-ASCII_OFFSET+TILE_BASE
;.charmap        'n', 'N'-ASCII_OFFSET+TILE_BASE
;.charmap        'o', 'O'-ASCII_OFFSET+TILE_BASE
;.charmap        'p', 'P'-ASCII_OFFSET+TILE_BASE
;.charmap        'q', 'Q'-ASCII_OFFSET+TILE_BASE
;.charmap        'r', 'R'-ASCII_OFFSET+TILE_BASE
;.charmap        's', 'S'-ASCII_OFFSET+TILE_BASE
;.charmap        't', 'T'-ASCII_OFFSET+TILE_BASE
;.charmap        'u', 'U'-ASCII_OFFSET+TILE_BASE
;.charmap        'v', 'V'-ASCII_OFFSET+TILE_BASE
;.charmap        'w', 'W'-ASCII_OFFSET+TILE_BASE
;.charmap        'x', 'X'-ASCII_OFFSET+TILE_BASE
;.charmap        'y', 'Y'-ASCII_OFFSET+TILE_BASE
;.charmap        'z', 'Z'-ASCII_OFFSET+TILE_BASE
;
;;CAPITAL letters need ascii and tile location offset
;.charmap	'A', 'A'-ASCII_OFFSET+TILE_BASE
;.charmap	'B', 'B'-ASCII_OFFSET+TILE_BASE
;.charmap	'C', 'C'-ASCII_OFFSET+TILE_BASE
;.charmap	'D', 'D'-ASCII_OFFSET+TILE_BASE
;.charmap	'E', 'E'-ASCII_OFFSET+TILE_BASE
;.charmap	'F', 'F'-ASCII_OFFSET+TILE_BASE
;.charmap	'G', 'G'-ASCII_OFFSET+TILE_BASE
;.charmap	'H', 'H'-ASCII_OFFSET+TILE_BASE
;.charmap	'I', 'I'-ASCII_OFFSET+TILE_BASE
;.charmap	'J', 'J'-ASCII_OFFSET+TILE_BASE
;.charmap	'K', 'K'-ASCII_OFFSET+TILE_BASE
;.charmap	'L', 'L'-ASCII_OFFSET+TILE_BASE
;.charmap	'M', 'M'-ASCII_OFFSET+TILE_BASE
;.charmap	'N', 'N'-ASCII_OFFSET+TILE_BASE
;.charmap	'O', 'O'-ASCII_OFFSET+TILE_BASE
;.charmap	'P', 'P'-ASCII_OFFSET+TILE_BASE
;.charmap	'Q', 'Q'-ASCII_OFFSET+TILE_BASE
;.charmap	'R', 'R'-ASCII_OFFSET+TILE_BASE
;.charmap	'S', 'S'-ASCII_OFFSET+TILE_BASE
;.charmap	'T', 'T'-ASCII_OFFSET+TILE_BASE
;.charmap	'U', 'U'-ASCII_OFFSET+TILE_BASE
;.charmap	'V', 'V'-ASCII_OFFSET+TILE_BASE
;.charmap	'W', 'W'-ASCII_OFFSET+TILE_BASE
;.charmap	'X', 'X'-ASCII_OFFSET+TILE_BASE
;.charmap	'Y', 'Y'-ASCII_OFFSET+TILE_BASE
;.charmap	'Z', 'Z'-ASCII_OFFSET+TILE_BASE
;
;.charmap        ' ',  3+TILE_BASE
;.charmap        '!',  $28+TILE_BASE
;.charmap        '$',  $29+TILE_BASE
;.charmap        ':',  $2A+TILE_BASE
;.charmap        '-',  $2B+TILE_BASE
;.charmap        '.',  $2C+TILE_BASE
;.charmap        $27,  $2D+TILE_BASE  ; vim doesn't like ' doesn't like so just use ascii value
;.charmap        ',',  $2E+TILE_BASE
;.charmap        '_',  $2F+TILE_BASE
;.charmap        '[',  $30+TILE_BASE
;.charmap        ']',  $31+TILE_BASE
;.charmap        '/',  $32+TILE_BASE



;;NOTE routines using trampoline:  Y register gets stomped, along with N&Z flags due to LDY #return bank
;	ram_tramp_code:
;		;switch banks
;		;NOTE!  This expects there to be a PRG bank table at the beginning of each PRG bank, must be page aligned
;		ldy #$00 ;jump to bank
;		;sty $8000 ;bank_table ;needs to be in rom (calling bank)
;		sty prg_bank_table ;needs to be in rom (calling bank)
;		;USE MASTER TABLE in last bank
;		;jmp called_func 
;		;jsr called_func 
;		;jsr load_chrram 
;		;jsr $8000
;		jsr_far_func $8000; place holder, must be updated before using
;		;TODO we could jump to the function, if the function jumps back here
;		;return here when function complete
;		;ldy #LAST_PRG_BANK;03 ;return bank
;		;ldy #((CHR_BANK<<4)+LAST_PRG_BANK) ;CHR_BANK:return bank
;		ldy #$0F ;placeholder (CHR bank0)
;		;sty $8003 ;bank_table ;need to be in called bank
;		;sty prg_bank_table+LAST_PRG_BANK ;bank_table ;need to be in called bank
;		;sty prg_bank_table+CHR_BANK ;bank_table ;need to be in called bank
;		sty prg_bank_table ;placeholder
;		;USE CHR table in each bank
;		rts ;back to ROM, can't replace this with a jump as don't know where we were called from
;
;		;let's have the NMI vector jump here
;		;ram_tramp_nmi:
;		;read the prg_bank_id to determine currently selected bank
;		;update the nmi return bank to that value
;		;switch to last bank and jump to nmi handler
;		;jmp nmi
;		;switch back to return bank
;		;return from interrupt
;	ram_tramp_end:

;jsra_table_helper:
;	
;rts ;jsra_table_helper:

;if didn't want the pointer fixed could copy this function to ram
;but the code required to update the pointer address before calling
;would consume more bytes than a dedicated oneline function for each fptr..
call_test_fptr:
	jmp (test_fptr)
	;the callee will rts back from where this function was called
	;so this function doesn't need an rts
;rts call_test_fptr:

;main routine
main:
;turn off NMI so won't trigger NMI during init
	lda #$00
	sta $2000

	;initialize the ram trampoline

	;copy trampoline code to ram
;	ldx #ram_tramp_end-ram_tramp_code-1 ;counting to zero
;	@copy_next:
;		lda ram_tramp_code, x
;		sta ram_tramp_exe, x
;		dex
;		bpl @copy_next

	
	; setup
	ldx #31 ;highest palette index
	:
		lda example_palette, X
		sta palette_buff, X
		dex
		bpl :-
	

	lda #1
	sta rand_seed ;init PRNG to anything but zero at startup

	;load first test
	lda #FIRST_TEST
	sta cur_test
	sta next_test
	lda #START_RUNTEST
	sta test_running

	;LOAD CHR-RAM BANKS WITH DUMMY STRIPE DATA
	;select CHR-RAM bank0
;	sel_chr_bankY 0, LAST_PRG_BANK
	;load PT0&1 of first CHR-RAM bank
	ldx #8*1024/256	;number of pages
	ldy #$00	;MSB
	sty arg0 ;inc val
	lda #$01 ;tile data
	jsr ppu_clear_nt

;	sel_chr_bankY 1, LAST_PRG_BANK
;	;load PT0&1 of first CHR-RAM bank
;	ldx #8*1024/256	;number of pages
;	ldy #$00	;MSB
;	sty arg0 ;inc val
;	lda #$05 ;tile data
;	jsr ppu_clear_nt
;
;	sel_chr_bankY 2, LAST_PRG_BANK
;	;load PT0&1 of first CHR-RAM bank
;	ldx #8*1024/256	;number of pages
;	ldy #$00	;MSB
;	sty arg0 ;inc val
;	lda #$15 ;tile data
;	jsr ppu_clear_nt
;
;	sel_chr_bankY 3, LAST_PRG_BANK
;	;load PT0&1 of first CHR-RAM bank
;	ldx #8*1024/256	;number of pages
;	ldy #$00	;MSB
;	sty arg0 ;inc val
;	lda #$55 ;tile data
;	jsr ppu_clear_nt

	;load text into first PT0 first bank
;	sel_chr_bankY 0, LAST_PRG_BANK
	;use a macro:
	mva_ptr ptr0, ascii_tiles
	ldx #((ascii_tiles_end-ascii_tiles)/256) ;number of 256B pages to copy
	;ldy #$0C ;PPU MSB
	ldy #(TILE_BASE>>4) ;PPU_MSB
	jsr ppu_load_file


	load_nametables:

	;clear last name/attr table which should be the offscreen one regardless if H/V mirroring
	ldx #4 		;1024 per NT/AT
	ldy #$2C	;NT3 starting address MSB
	lda #0	 ;add this value to each subsequent write
	sta arg0
	;lda #$00	;value to write to PPU address
	jsr ppu_clear_nt

	;load all name/attr tables
	;ldx #48 ;pages of PPU address space to clear 48 * 256 = 12288 = 12KB = 8KB CHR-RAM + 4KB NAMETABLES
	;ldy #$00 ;MSB PT0 start address
	;ldx #4*1024/256
	;display all tiles in first 4 rows
	ldx #1 		;256B per PT
	ldy #$20	;NT0 start
	lda #1 ;increment tile number for each byte of the NT, if tiles are all the same won't see a difference
		;aside from attribute tables
	sta arg0
	lda #$00	;value to write to PPU address
	jsr ppu_clear_nt

	;clear rest of name/attr table
	ldx #3 		;256B per PT
	ldy #$21	;1/4 down NT0
	lda #0	 ;add this value to each subsequent write
	sta arg0
	jsr ppu_clear_nt


	;enable NMI, 8x8 sprites, BG PT0, SP PT0, NT0, VRAM INC+1
	lda #$80
	sta $2000

	;can now draw with nmt_update, but can only draw so much per frame

	;rom title
	mva_ptr ptr0, title_string
	ldx #LEFT_MARGIN
	ldy #TITLE_ROW
	jsr ppu_write_string

	;instructions
	mva_ptr ptr0, instruction_str
	ldx #LEFT_MARGIN
	ldy #INSTR_ROW
	jsr ppu_write_string

	;draw lines above
	jsr ppu_update

	;current test
	mva_ptr ptr0, test_string
	ldx #LEFT_MARGIN
	ldy #TEST_ROW
	jsr ppu_write_string

	lda cur_test
	dec cur_test ;will trigger false change for the first comparison
	ldx #TEST_COL
	ldy #TEST_ROW
	jsr display_hex_byte


	jsr cursor_init

	jsr ppu_update ; ppu_update waits until next NMI, turns rendering on (if not already)
			;uploads OAM, palette, and nametable update to PPU

;	;END OF INIT


;;;;;;;;;;;;;;;;;;;;;;;;
;;  MAIN LOOP
;;;;;;;;;;;;;;;;;;;;;;;;
main_loop:
;@temp = reg7 ;hard defined du
;TODO figure out what's breaking the scope

	;nmi set the scroll to $2000 top left corner for HUD

	;ppu_update is complete
	;we may still be in vblank though

;	;count frames, if on NTSC, skip processing every 6th frame to process the game at 50Hz
;	;lda system_type
;	lda skip_frame ;will be negative for PAL/DENDY, positive for NTSC
;	;TODO shouldn't need 2 separate variables, change encoding to make PAL & DENDY negative numbers
;	;then decide to dec frame if pl/mi
;	bmi @non_ntsc_system
;		;NTSC SYSTEM
;		dec skip_frame
;		bpl @non_ntsc_system
;			;wrap from -1 to 5
;			lda #5
;			sta skip_frame
;		
;	@non_ntsc_system:

	;store last frame's gamepad for edge detection
	lda gamepad
	sta gamepad_last
	lda gamepad2
	sta gamepad2_last

	;read gamepads & determine new presses
	ldy #0	;pad1
	jsr gamepad_poll
	sta gamepad
	eor gamepad_last ;invert bits that were presses last frame
	and gamepad	 ;clear bits that were released
	sta gamepad_new

	;if gamepad data changed, send it to esp
;	jsr send_gamepad1

	ldy #1  ;pad2
	jsr gamepad_poll
;	jsr get_pad2_online

	sta gamepad2
	eor gamepad2_last
	and gamepad2
	sta gamepad2_new

;	jsr display_gamepad


	;check for scrolling by holding START+SEL and using d-pad
SCROLL_STEP_Y = 24 ;best when scroll amount is multiple of 240
SCROLL_STEP_X = 32 ;best when scroll amount is multiple of 256
	lda gamepad_new
	and #PAD_DIRS
	beq @no_scroll
	lda gamepad
	and #PAD_SELECT+PAD_START
	cmp #PAD_SELECT+PAD_START
	bne @no_scroll

		;move scroll_x/y by SCROLL_STEP in direction pressed
		lda gamepad
		and #PAD_U
		beq @no_up
			lda scroll_y
			sub #SCROLL_STEP_Y
			sta scroll_y
			;bcs :+
			bcs @no_down ;up and down can't both be pressed
				sbc #15 ;-16 (carry clear) skip over attribute tables
				bacs @skip_at
			;	;just need to flip bit1 double dec/inc does it
			;	dec scroll_nmt
			;	dec scroll_nmt
		@no_up:
		lda gamepad
		and #PAD_D
		beq @no_down
			lda scroll_y
			add #SCROLL_STEP_Y
			sta scroll_y
			adc #16 ;skip over attribute tables
			bcc @no_down
				@skip_at:
				;lda #0 math above already did this
				sta scroll_y
				;just need to flip bit1 double dec/inc does it
				inc scroll_nmt
				inc scroll_nmt
		@no_down:
		lda gamepad
		and #PAD_L
		beq @no_left
			lda scroll_x
			sub #SCROLL_STEP_X
			sta scroll_x
			bcc @next_nt
			;could skip over right checking but doesn't save much for the cost of couple bytes
			;bcs :+
			;	;flip bit0 without changing bit1
			;	lda scroll_nmt
			;	eor #1
			;	sta scroll_nmt
			;:
		@no_left:
		lda gamepad
		and #PAD_R
		beq @no_right
			lda scroll_x
			add #SCROLL_STEP_X
			sta scroll_x
			bcc @no_right
				@next_nt:
				;flip bit0 without changing bit1
				lda scroll_nmt
				eor #1
				sta scroll_nmt
		@no_right:

		;don't want to scroll and move cursor
		;cursor doesn't move if Start+select pressed
		jmp @skip_cursor
	@no_scroll:

	;move cursor and go update variables if changed
	jsr cursor_state
	jsr cursor_update
	@skip_cursor:


	lda next_test
	cmp cur_test
	beq @no_new_test
		;new test selected, handle rollover
		cmp #$FF
		bne :+	;decremented below zero
			lda #((test_table_end-test_table)>>TT_WORDS_PER_ENTRY)-1
		:
		cmp #((test_table_end-test_table)>>TT_WORDS_PER_ENTRY)
		bcc :+	;incremented past last test
			lda #0
		:
		sta cur_test
		ldx #TEST_COL
		ldy #TEST_ROW
		jsr display_hex_byte

		;display test name
		lda cur_test

		.if TT_WORDS_PER_ENTRY = 1
			mul a, #2
		.elseif TT_WORDS_PER_ENTRY = 2
			mul a, #4
		.else
			.ERROR: TT_WORDS_PER_ENTRY NOT SUPPORTED
		.endif

		tax
		pha ;test table index 
		mva_ptr_tablex ptr0, test_table_names
		ldx #LEFT_MARGIN
		ldy #NAME_ROW
		jsr ppu_write_string

		;update the test function pointer
		pla ;test table index
		tax
		mva_ptr_tablex test_fptr, test_table_fptrs


	@no_new_test:

	;START TEST
	lda gamepad_new
	and #PAD_START
	beq @no_start_test

		;check if cursor on test row to start/stop test
		;otherwise the start button could have different functions depending on the test
		lda cursor_y
		cmp #TEST_ROW
		bne @no_start_test
			;proper row selected, are we starting or stopping?
			lda test_running
			bne @stop_test

				lda #1
				sta test_running
				bane @no_start_test

			@stop_test:
				lda #0
				sta test_running
				;redraw screen
				;turn off NMI so won't trigger NMI during init
				lda #2 ;turn off rendering
				sta nmi_ready
				@render_off_wait:
				lda nmi_ready
				bne @render_off_wait
				jmp load_nametables

	@no_start_test:

	;run the current test each frame
	lda test_running
	beq :+;not_running
		;jsri test_fptr this is slow and macro is bigger and slower than single line jmp () subroutine..
		jsr call_test_fptr
	:;not_running:




nmi_wait:
	;wait for end of frame
	;draw everything in NMI and finish the frame
	jsr ppu_update

	;keep doing this forever!
	jmp main_loop



;for bus conflicted bank switching, need a self referencing label
;if we put a label inside a macro, we will have duplicate labels next time the macro is used
;create incrementing label so we'll have a unique label each time

.macro chr_bank_table
.scope
	.byte $0F, $1F, $2F, $3F  ;4banks of CHR-ROM, always select last PRG bank
.endscope
.endmacro

;label generator
;https://cc65.github.io/doc/ca65.html#toc6.2
;bank_label_cnt 	  .set 0	;BL000: counter
;.macro	sel_chr_bank num, cur_prg_bank ;CHR-RAM BANK NUMBER, uses Y register and writes on top of ldy value to prevent bus conflicts
;Dumb version, I had problems with labels already being defined when reusing, can simply use scope!
;	;select desired chr-ram bank
;
;	bank_label_cnt .set bank_label_cnt + 1
;	.ident (.sprintf ("BL%04X", bank_label_cnt)): ;BL000 label
;	;@bank0:
;	
;	;ldy #(num<<4)	;CCCC LLPP
;	ldy #((num<<MAP_CHR_SHIFT)+(cur_prg_bank<<MAP_PRG_SHIFT))	;define in ines_**.inc file
;	;sty @bank0+1
;	sty .ident (.sprintf ("BL%04X", bank_label_cnt))+1 ;BL000 label
;.endmacro
;NOTE: unfortunately, this breaks local scoping, and functions using this can't call local labels accross this macro..
;TODO some research into if this is a solvable problem with ca65, would go long way to making program structures usable..
.macro	sel_chr_bankY num, cur_prg_bank ;CHR-RAM BANK NUMBER, uses Y register and writes on top of ldy value to prevent bus conflicts
.scope ;select desired chr-ram bank
	ldy_label:
	;ldy #(num<<4)	;CCCC LLPP
	ldy #((num<<MAP_CHR_SHIFT)+(cur_prg_bank<<MAP_PRG_SHIFT))	;define in ines_**.inc file
	sty ldy_label+1 ;immediate value of the ldy (previous instruction)
.endscope
.endmacro

;local scope freindly macro for loading chr bank with shift & cur PRG bank
.macro ldy_chr_bank num, cur_prg_bank
	;ldy #(num<<4)	;CCCC LLPP
	ldy #((num<<MAP_CHR_SHIFT)+(cur_prg_bank<<MAP_PRG_SHIFT))	;define in ines_**.inc file
	;sty label+1 ;immediate value of the ldy (previous instruction)
.endmacro


;chr_bank doesn't need to be provided
;init and Vblank, don't care about the CHR bank usually, but during rendering we will care!
;if not provided, will select bank 0 on the jump away from code (last) bank.
;on the return to code bank, it will use the chr bank that was last used for a return (self modifying code)
.macro jsr_far prg_bank, funcname, chr_bank
	;ldy #prg_bank;PT_BANK ;prg_bank we're jumping to
	.ifnblank chr_bank
	ldy #((chr_bank<<MAP_CHR_SHIFT)+(prg_bank<<MAP_PRG_SHIFT))	;define in ines_**.inc file
	.else
	ldy #(prg_bank<<MAP_PRG_SHIFT)	;if chr_bank not defined, default to 0
	.endif
	sty ram_tramp_exe+1 ;ldy #00
	sty ram_tramp_exe+3 ;sty $8000 prg_bank_table MASTER in last PRG bank
	ldy #<funcname;load_chrram	
	sty ram_tramp_exe+6 ;jsr load_chrram function address LOW byte
	ldy #>funcname;load_chrram
	sty ram_tramp_exe+7 ;jsr load_chrram function address HIGH byte
	;RETURN CHR BANK
	.ifnblank chr_bank
	ldy #((chr_bank<<MAP_CHR_SHIFT)+(LAST_PRG_BANK<<MAP_PRG_SHIFT))	;define in ines_**.inc file
	sty ram_tramp_exe+9 ;ldy #0F  CHR-BANK
	ldy #chr_bank
	sty ram_tramp_exe+11 ;sty chr_bank_table address low byte  CHR-BANK
	.endif
	jsr ram_tramp_exe
.endmacro

;use jmp instead of jsr to functions in other banks
;JMP_TO_BANKED_CODE = 1

.macro jsr_far_func func
.IFDEF JMP_TO_BANKED_CODE
	jmp func
.ELSE
	jsr func
.ENDIF
.endmacro

.macro rts_far_func
.IFDEF JMP_TO_BANKED_CODE
	jmp ram_tramp_exe+8;-> ldy #LAST_PRG_BANK;03 ;return bank
.ELSE
	rts
.ENDIF
.endmacro




;.DEFINE MAP_CDREAMS
MAP_CDREAM = 1 ;TRUE define bankswitching routines, etc
MAP_CHR_SHIFT = 4 ;number of shifts to left needed for CHR bank number
MAP_PRG_SHIFT = 0 ;number of shifts to left needed for PRG bank number

INES_MAPPER = 11 ; = ColorDreams
INES_MIRROR = 0 ; 0 = horizontal mirroring, 1 = vertical mirroring
INES_SRAM   = 0 ; 1 = battery backed SRAM at $6000-7FFF

.byte 'N', 'E', 'S', $1A ; ID
;.byte $02 ; 16k PRG chunk count
;PRG512 = 1 ;define for more than 128KB
;PRG128 = 1 ;define for 128KB
PRG32 = 1 ;define for only 32KB
.IFDEF PRG512
.byte $20 ; $20=32*16=512KB colordream max 16k PRG chunk count
LAST_PRG_BANK = $0F 
.ELSE
;128KB
;.byte $08 ; $8*16=128KB colordream orig max 16k PRG chunk count
;LAST_PRG_BANK = $03 ;code goes in this bank
;32KB
.byte $02 ; $8*16=128KB colordream orig max 16k PRG chunk count
LAST_PRG_BANK = $00 ;code goes in this bank
.ENDIF
CODE_BANK = LAST_PRG_BANK ;code goes in this bank

.byte $00 ; 0k CHR-ROM chunk count
.byte INES_MIRROR | (INES_SRAM << 1) | ((INES_MAPPER & $f) << 4)
;.byte (INES_MAPPER & %11110000) ;nes 1.0
.byte (INES_MAPPER & %11110000) + $08 ;nes 2.0
;8 mapper/submaper
.byte $0
;9 PRG/CHR extra size
.byte $0
;10 WRAM size
.byte 7 ;7=8KByte 64<<7 = 8192

;11 CHR-RAM size
.byte $9 ;32KByte
;12-15 empty
.byte $0, $0, $0, $0 ; padding



;cursor_x/y set to row would like to update
;X word indexed= 0:aptr0, 2:aptr1, 4:aptr2..
display_aptr:
	txa
	pha ;push X
	pha ;push X
	lda aptr_table_hi, x
	ldx #APTR_ADDR_HI
	ldy cursor_y
	jsr display_hex_byte
	plx
	lda aptr_table_lo, x
	ldx #APTR_ADDR_LO
	ldy cursor_y
	jsr display_hex_byte
	plx
	lda aptr_table_wr, x
	ldx #APTR_WR_VAL
	ldy cursor_y
	jsr display_hex_byte
rts ;display_aptr

;cursor_x/y set to row would like to update
;X word indexed= 0:aptr0, 2:aptr1, 4:aptr2..
display_aptr_addr:
	txa
	pha ;push X
	lda aptr_table_hi, x
	ldx #APTR_ADDR_HI
	ldy cursor_y
	jsr display_hex_byte
	plx
	lda aptr_table_lo, x
	ldx #APTR_ADDR_LO
	ldy cursor_y
	jsr display_hex_byte
rts ;display_aptr_addr

;A hex value to display (gets stomped)
;X col (MSB, LSB->X+1)
;Y row
display_hex_byte:
;ppu_update_tile reg0
@temp = reg1

	sta @temp
	txa
	pha
	tya
	pha

	lda @temp
	lsr
	lsr
	lsr
	lsr
.IFDEF HEX_OFFSET ;'A' doesn't follow '9' in tileset
	cmp #10
	bmi :+
		adc #HEX_OFFSET-1
	:
	adc #NUM_OFFSET+TILE_BASE
.ELSE
	add #NUM_OFFSET+TILE_BASE
.ENDIF
	jsr ppu_update_tile	;A-tilenum X-xpos Y-ypos uses reg0
	pla
	tay
	pla
	tax
	inx
	lda @temp
	and #$0F
.IFDEF HEX_OFFSET ;'A' doesn't follow '9' in tileset
	cmp #10
	bmi :+
		adc #HEX_OFFSET-1
	:
	adc #NUM_OFFSET+TILE_BASE
.ELSE
	add #NUM_OFFSET+TILE_BASE
.ENDIF
	jsr ppu_update_tile	;A-tilenum X-xpos Y-ypos uses reg0

rts ;display_hex_byte


;write string of unknown length, 
;similar args as ppu_write string
;but limits to 28 chars & increments line until all lines printed
;will hang for required number of frames until printing complete
;	call example:
;	mva_ptr ptr0, ESP_RX_BUFF
;	;always starts at LEFT_MARGIN
;	ldx #RX_LINE (y unused for (),Y addressing
;	jsr display_long_string
;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
;!!! CAUTION !!! string must be in writeable RAM! (cart or system doesn't matter) 
;!!! doesnt' work if source is ROM would have to modify routine
;!!! to copy into ram, or handle segmenting in different manner
;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
display_long_string:

	;if LINE_LIMIT exceeded, print one line per frame
	ldy #$FF
	@check_next:
		iny
		cpy #LINE_LIMIT
		beq @print_line
		@check_now:
		lda (ptr0),Y
		bne @check_next
		;Y = index of null terminator

		;(remaining) string fits
		txa
		tay
		ldx #LEFT_MARGIN
		;jump and let that function return for where we were called
		jmp ppu_write_string

	@print_line:
		;stop max char
		ldy #LINE_LIMIT
		lda (ptr0), Y
		pha ;temp store of stomped value
		lda #0
		sta (ptr0), Y

		;move X->Y (and preserve)
		txa 
		pha
		tay ;row to print on
		ldx #LEFT_MARGIN
		jsr ppu_write_string

		;restore & increment printing row
		pla ;line just printed to 
		tax
		inx ;next line to print to
		;check if X past end of screen, if so set to next page
		cpx #LAST_LINE+1
		bne :+
			ldx #NEXT_PAGE
		:

		;advance ptr0 to stomped location
		;it'll be start of next line
		lda #LINE_LIMIT
		add ptr0_lo
		sta ptr0_lo
		bcc :+
			inc ptr0_hi
		:

		;restore stomped value
		ldy #0
		pla ;stomped value
		sta (ptr0), Y

		;wait for frame to be drawn
		jsr ppu_update

		jmp @check_now
	
;this function doesn't have rts, it rts' via final jump to ppu_write_string
;rts;display_long_string:

;A = number of lines to clear
;Y = first line to clear
;clear from LEFT_MARGIN to LINE_LIMIT
;NOTE!  cannot exceed lines on the given nametable
;if want to clear multiple name tables call separately for each
;will wait for each line to update to ppu before drawing next, takes 1 frame per line
clear_lines:
@row = reg1
@col = reg2

	pha ;num lines
	sty @row
	ldx #LEFT_MARGIN
	stx @col
	@next_col:
		ldx @col
		ldy @row
		lda #0 ;tile number
		jsr ppu_update_tile
		ldy @row
		inc @col
		ldx @col
		cpx #LINE_LIMIT+2 ;not 100% sure why this is guess off by one on how I thought it worked...
		bne @next_col

		;draw that line before continuing
		jsr ppu_update

		pla ;num rows
		sub #1
		bne :+
			rts
		:
		pha ;num remaining rows

		ldx #LEFT_MARGIN
		stx @col
		inc @row
		ldy @row ;should never be loading row zero as next row
		bane @next_col

	
rts;clear_lines

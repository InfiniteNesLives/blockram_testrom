
qspipump_line1_str:
.asciiz "Mf Ty Cp - ManID DevID"
qspipump_line2_str:
.asciiz "Unique ID"
qspipump_line3_str:
.asciiz "SR1 SR2"
qspipump_line4_str:
.asciiz "ser - quad -fast      - last"

;qspipump_line2_str:
;.asciiz "game"
;qspipump_line3_str:
;.asciiz "esp B0 B1 P0 P1"

qspipump_test_init:

;	mva_ptr ptr0, qspipump_inst_str
;	ldx #LEFT_MARGIN
;	ldy #INST_ROW
;	jsr ppu_write_string
;
;	jsr ppu_update

	mva_ptr ptr0, qspipump_line1_str
	ldx #LEFT_MARGIN
	ldy #LINE1
	jsr ppu_write_string

	jsr ppu_update

	mva_ptr ptr0, qspipump_line2_str
	ldx #LEFT_MARGIN
	ldy #LINE3
	jsr ppu_write_string

	jsr ppu_update

	mva_ptr ptr0, qspipump_line3_str
	ldx #LEFT_MARGIN
	ldy #LINE5
	jsr ppu_write_string

	jsr ppu_update

	mva_ptr ptr0, qspipump_line4_str
	ldx #LEFT_MARGIN
	ldy #LINE7
	jsr ppu_write_string

	jsr ppu_update
;	;draw lines above
;	jsr ppu_update
;
;	;#1 display PCICCOM startup value, should be open bus
;	lda PCICCOM_R0
;	ldx #LEFT_MARGIN+2
;	ldy #LINE1
;	jsr display_hex_byte
;
;	;draw lines above
;	jsr ppu_update
;

	jsr probe_flash

	jsr qspipump_prime

;	jsr qspipump_open_comms
;	;A= reply byte
;	ldx #LEFT_MARGIN+7
;	ldy #LINE1
;	jsr display_hex_byte
;
;	;dummy xor test
;	lda #$1E
;	sta PCICCOM_R1
;	jsr delay_1ms
;
;	lda #SPI_CMD_DUMMY
;	sta PCICCOM_R0
;	jsr delay_1ms
;
;	lda PCICCOM_R0
;	ldx #LEFT_MARGIN+12
;	ldy #LINE1
;	jsr display_hex_byte
	

	;switch function pointer to run test
	mva_ptr test_fptr, qspipump_run

rts;qspipump_test_init:


qspipump_run:

rts;qspipump_run:

;read the status registers & ID registers, print to the screen
;pre: spi flash is free to access via serial (not currently in continuous read mode/etc)
probe_flash:

	;we should be in slow speed serial mode
	;toggle the /CS pin
	lda #%00000001
	sta SPI_CTL

	;reset the QSPI flash
	lda #%00000000
	sta SPI_CTL
	;write command
	lda #SPI_CMD_ENRST
	sta SPI_PDATA
	;end command
	lda #%00000001
	sta SPI_CTL

	lda #%00000000
	sta SPI_CTL
	;write command
	lda #SPI_CMD_RST
	sta SPI_PDATA
	;end command
	lda #%00000001
	sta SPI_CTL

;Read the manf ID, Mem Type, Capacity

	lda #%00000000
	sta SPI_CTL

	;write command
	lda #SPI_CMD_ID
	sta SPI_PDATA

	;read & display first byte
	sta SPI_PDATA	;trigger the shift
	lda SPI_PDATA	;read the result
	ldx #LEFT_MARGIN
	ldy #LINE2
	jsr display_hex_byte

	sta SPI_PDATA
	lda SPI_PDATA
	ldx #LEFT_MARGIN+3
	ldy #LINE2
	jsr display_hex_byte

	sta SPI_PDATA
	lda SPI_PDATA
	ldx #LEFT_MARGIN+6
	ldy #LINE2
	jsr display_hex_byte

	;disable /CS
	lda #%00000001
	sta SPI_CTL

	jsr ppu_update


;Read Manf ID & Dev ID
	lda #%00000000
	sta SPI_CTL

	;write command
	lda #SPI_CMD_MID
	sta SPI_PDATA

	;read & display first byte
	sta SPI_PDATA	;trigger the shift
	lda SPI_PDATA	;read the result
	ldx #LEFT_MARGIN+9
	ldy #LINE2
	jsr display_hex_byte

	sta SPI_PDATA
	lda SPI_PDATA
	ldx #LEFT_MARGIN+12
	ldy #LINE2
	jsr display_hex_byte

	sta SPI_PDATA
	lda SPI_PDATA
	ldx #LEFT_MARGIN+15
	ldy #LINE2
	jsr display_hex_byte

	sta SPI_PDATA
	lda SPI_PDATA
	ldx #LEFT_MARGIN+18
	ldy #LINE2
	jsr display_hex_byte

	sta SPI_PDATA
	lda SPI_PDATA
	ldx #LEFT_MARGIN+21
	ldy #LINE2
	jsr display_hex_byte
	;disable /CS
	lda #%00000001
	sta SPI_CTL

	jsr ppu_update

;Read Unique ID
	lda #%00000000
	sta SPI_CTL

	;write command
	lda #SPI_CMD_UID
	sta SPI_PDATA

	;purge 4 dummy bytes
	sta SPI_PDATA	;trigger the shift
	sta SPI_PDATA	;trigger the shift
	sta SPI_PDATA	;trigger the shift
	sta SPI_PDATA	;trigger the shift

	;read & display first byte
	sta SPI_PDATA	;trigger the shift
	lda SPI_PDATA	;read the result
	ldx #LEFT_MARGIN+0
	ldy #LINE4
	jsr display_hex_byte

	sta SPI_PDATA
	lda SPI_PDATA
	ldx #LEFT_MARGIN+2
	ldy #LINE4
	jsr display_hex_byte

	sta SPI_PDATA
	lda SPI_PDATA
	ldx #LEFT_MARGIN+4
	ldy #LINE4
	jsr display_hex_byte

	sta SPI_PDATA
	lda SPI_PDATA
	ldx #LEFT_MARGIN+6
	ldy #LINE4
	jsr display_hex_byte

	sta SPI_PDATA
	lda SPI_PDATA
	ldx #LEFT_MARGIN+8
	ldy #LINE4
	jsr display_hex_byte

	sta SPI_PDATA
	lda SPI_PDATA
	ldx #LEFT_MARGIN+10
	ldy #LINE4
	jsr display_hex_byte

	sta SPI_PDATA
	lda SPI_PDATA
	ldx #LEFT_MARGIN+12
	ldy #LINE4
	jsr display_hex_byte

	sta SPI_PDATA
	lda SPI_PDATA
	ldx #LEFT_MARGIN+14
	ldy #LINE4
	jsr display_hex_byte

	sta SPI_PDATA
	lda SPI_PDATA
	ldx #LEFT_MARGIN+16
	ldy #LINE4
	jsr display_hex_byte

	sta SPI_PDATA
	lda SPI_PDATA
	ldx #LEFT_MARGIN+18
	ldy #LINE4
	jsr display_hex_byte

	sta SPI_PDATA
	lda SPI_PDATA
	ldx #LEFT_MARGIN+20
	ldy #LINE4
	jsr display_hex_byte

	sta SPI_PDATA
	lda SPI_PDATA
	ldx #LEFT_MARGIN+22
	ldy #LINE4
	jsr display_hex_byte

	sta SPI_PDATA
	lda SPI_PDATA
	ldx #LEFT_MARGIN+24
	ldy #LINE4
	jsr display_hex_byte
	;disable /CS
	lda #%00000001
	sta SPI_CTL

	jsr ppu_update


;read status registers

	;enable /CS
	lda #%00000000
	sta SPI_CTL

	;write command
	lda #SPI_CMD_RSR1
	sta SPI_PDATA

	;read & display first byte
	sta SPI_PDATA	;trigger the shift
	lda SPI_PDATA	;read the result
	ldx #LEFT_MARGIN+0
	ldy #LINE6
	jsr display_hex_byte

	;disable /CS
	lda #%00000001
	sta SPI_CTL



	;enable /CS
	lda #%00000000
	sta SPI_CTL

	;write command
	lda #SPI_CMD_RSR2
	sta SPI_PDATA

	;read & display first byte
	sta SPI_PDATA	;trigger the shift
	lda SPI_PDATA	;read the result
	ldx #LEFT_MARGIN+4
	ldy #LINE6
	jsr display_hex_byte

	;disable /CS
	lda #%00000001
	sta SPI_CTL

	jsr ppu_update

;serial read
	;enable /CS
	lda #%00000000
	sta SPI_CTL

	;write command
	lda #SPI_CMD_READ
	sta SPI_PDATA

	;read address
	lda #$01
	sta SPI_PDATA
	lda #$23
	sta SPI_PDATA
	lda #$45
	sta SPI_PDATA

	;read & display first byte
	sta SPI_PDATA	;trigger the shift
	lda SPI_PDATA	;read the result
	ldx #LEFT_MARGIN+0
	ldy #LINE8
	jsr display_hex_byte

	;disable /CS
	lda #%00000001
	sta SPI_CTL



	;enable /CS
	lda #%00000000
	sta SPI_CTL

	;write command
	lda #SPI_CMD_READ
	sta SPI_PDATA

	;read address
	lda #$0A
	sta SPI_PDATA
	lda #$5A
	sta SPI_PDATA
	lda #$5A
	sta SPI_PDATA

	;read & display first byte
	sta SPI_PDATA	;trigger the shift
	lda SPI_PDATA	;read the result
	ldx #LEFT_MARGIN+3
	ldy #LINE8
	jsr display_hex_byte

	;disable /CS
	lda #%00000001
	sta SPI_CTL
	jsr ppu_update
	


;QE bit is nvm apparently, it's still set
;so lets just perform a quad read

; 	 tmp = quad_spi_read(0x001_23_45, false, false, true)
;        print("fast quad read", help.hex(tmp))
;        tmp = quad_spi_read(0x00A_5A_5A, false, false, true)

;        --write the address to a test register
;        local temp = ((addr >> 16 ) & 0xFF)
;        print(help.hex(temp))
;        dict.nes("NES_CPU_WR", SPI_QADDR+2, temp)  --write A23-16
	lda #$01
	sta SPI_QADDR+2
;        temp = ((addr >> 8 ) & 0xFF)
;        print(help.hex(temp))
;        dict.nes("NES_CPU_WR", SPI_QADDR+1, temp)  --write A15-8
	lda #$23
	sta SPI_QADDR+1
;        temp = ((addr >> 0 ) & 0xFF)
;        print(help.hex(temp))
;        dict.nes("NES_CPU_WR", SPI_QADDR, temp)  --write A7-0
	lda #$45
	sta SPI_QADDR+0

;
;        --.spi_cs_n     (spi_ctl[0]),
;        --.spi_rw       (spi_access),
;        --.pdata_in     (data),
;        --.pdata_out    (spi_pdata_out),
;        --.quad_mode    (spi_ctl[2]),
;        --.quad_start   (spi_ctl[1]),
;        --.quad_exit    (spi_ctl[3]),
;
;        if (start==true) then
;                                        --0bxxxx_0010
;                dict.nes("NES_CPU_WR", SPI_CTL, 0x02)  --enable quad start & /CS
	lda #$02
	sta SPI_CTL
;
;                dict.nes("NES_CPU_WR", SPI_PDATA, 0xEB)  --write spi data reg
	lda #SPI_CMD_QREAD
	sta SPI_PDATA
;
;                                        --0bxxxx_0110
;                dict.nes("NES_CPU_WR", SPI_CTL, 0x06)  --enable quad start & quad mode read controller
	lda #$06
	sta SPI_CTL
;
;                dict.nes("NES_CPU_WR", SPI_PDATA, 0x00)  --just need to trigger the read
	sta SPI_PDATA
;                                        --0bxxxx_0100
;                dict.nes("NES_CPU_WR", SPI_CTL, 0x04)  --done with the start, just stay in quad mode
	lda #$04
	sta SPI_CTL
;
;                rv = dict.nes("NES_CPU_RD", SPI_QDATA, 0x00)  --read the quad value
	lda SPI_QDATA
	ldx #LEFT_MARGIN+6
	ldy #LINE8
	jsr display_hex_byte



;Now perform the first consecutive read (non-fast)
	lda #$0A
	sta SPI_QADDR+2
	lda #$5A
	sta SPI_QADDR+1
	lda #$5A
	sta SPI_QADDR+0

;
;
;        elseif (exit==true) then
;                                        --0bxxxx_1100
;                dict.nes("NES_CPU_WR", SPI_CTL, 0x06)  --enable quad exit & quad mode read controller
;                dict.nes("NES_CPU_WR", SPI_PDATA, 0x00)  --just need to trigger the read
;                rv = dict.nes("NES_CPU_RD", SPI_QDATA, 0x00)  --read the quad value
;                                        --0bxxxx_1100
;                dict.nes("NES_CPU_WR", SPI_CTL, 0x01)  --disable quad mode and disable /CS
;
;        else
;                --leave modes as-is just trigger and perform the read
;                if (fast == true) then
;                                        --0bxxx1_0100
;                        dict.nes("NES_CPU_WR", SPI_CTL, 0x14)  --disable quad mode and disable /CS
;                else
;                                        --0bxxx0_0100
;                        dict.nes("NES_CPU_WR", SPI_CTL, 0x04)  --disable quad mode and disable /CS
	lda #$04
	sta SPI_CTL
;                end
;                dict.nes("NES_CPU_WR", SPI_PDATA, 0x00)  --just need to trigger the read
	sta SPI_PDATA
;                rv = dict.nes("NES_CPU_RD", SPI_QDATA, 0x00)  --read the quad value
;                dict.nes("NES_CPU_WR", SPI_CTL, 0x01)  --disable quad mode and disable /CS
	lda SPI_QDATA
	ldx #LEFT_MARGIN+9
	ldy #LINE8
	jsr display_hex_byte
	jsr ppu_update
;
;        end


;Now perform a fast read 
	lda #$01
	sta SPI_QADDR+2
	lda #$23
	sta SPI_QADDR+1
	lda #$45
	sta SPI_QADDR+0

	lda #$14
	sta SPI_CTL
	sta SPI_PDATA
	lda SPI_QDATA
	ldx #LEFT_MARGIN+12
	ldy #LINE8
	jsr display_hex_byte


	lda #$0A
	sta SPI_QADDR+2
	lda #$5A
	sta SPI_QADDR+1
	lda #$5A
	sta SPI_QADDR+0

	sta SPI_PDATA
	lda SPI_QDATA
	ldx #LEFT_MARGIN+15
	ldy #LINE8
	jsr display_hex_byte


	lda #$01
	sta SPI_QADDR+2
	lda #$23
	sta SPI_QADDR+1
	lda #$45
	sta SPI_QADDR+0

	lda #$14
	sta SPI_CTL
	sta SPI_PDATA
	lda SPI_QDATA
	ldx #LEFT_MARGIN+18
	ldy #LINE8
	jsr display_hex_byte

	lda #$0A
	sta SPI_QADDR+2
	lda #$5A
	sta SPI_QADDR+1
	lda #$5A
	sta SPI_QADDR+0

	sta SPI_PDATA
	lda SPI_QDATA
	ldx #LEFT_MARGIN+21
	ldy #LINE8
	jsr display_hex_byte


;;perform the last fast read
;	lda #$01
;	sta SPI_QADDR+2
;	lda #$23
;	sta SPI_QADDR+1
;	lda #$45
;	sta SPI_QADDR+0
;
;;        elseif (exit==true) then
;;                                        --0bxxxx_1100
;;                dict.nes("NES_CPU_WR", SPI_CTL, 0x06)  --enable quad exit & quad mode read controller
;	lda #$06
;	sta SPI_CTL
;;                dict.nes("NES_CPU_WR", SPI_PDATA, 0x00)  --just need to trigger the read
;	sta SPI_PDATA
;;                rv = dict.nes("NES_CPU_RD", SPI_QDATA, 0x00)  --read the quad value
;	nop
;	nop
;	nop
;	nop
;	lda SPI_QDATA
;	ldx #LEFT_MARGIN+24
;	ldy #LINE8
;	jsr display_hex_byte
;;                                        --0bxxxx_1100
;;                dict.nes("NES_CPU_WR", SPI_CTL, 0x01)  --disable quad mode and disable /CS
;	lda #$01
;	sta SPI_CTL


;set the qspi pump to continuous read/execute mode
	lda #$34
	sta SPI_CTL


;set the bootmapper to QSPI at $8000-BFFF
	lda #$20
	sta $C000


	jsr ppu_update
	

rts;probe_flash

;local function spi_access( reg, addr, len, wr_data )
;
;        local rv
;
;        dict.nes("NES_CPU_WR", 0x5000, 0x00)  --write spi control register (/CS enabled, serial mode)
;        dict.nes("NES_CPU_WR", 0x5001, reg)  --write spi data reg (spi read SR1 command)
;
;        print("\naccess reg:", help.hex(reg))
;
;        if addr ~= nil then
;                local temp = ((addr >> 16 ) & 0xFF)
;                print(help.hex(temp))
;                dict.nes("NES_CPU_WR", 0x5001, temp)  --write A23-16
;                temp = ((addr >> 8 ) & 0xFF)
;                print(help.hex(temp))
;                dict.nes("NES_CPU_WR", 0x5001, temp)  --write A15-8
;                temp = ((addr >> 0 ) & 0xFF)
;                print(help.hex(temp))
;                dict.nes("NES_CPU_WR", 0x5001, temp)  --write A7-0
;        end
;
;        while len > 0 do
;                if wr_data == nil then wr_data = 0x00 end
;                dict.nes("NES_CPU_WR", 0x5001, (wr_data&0xFF))  --shift again to read first output byte
;                rv = dict.nes("NES_CPU_RD", 0x5001)
;                print(help.hex(rv))
;                len = len - 1
;                wr_data = wr_data >> 8;
;        end
;
;        dict.nes("NES_CPU_WR", 0x5000, 0x01)  --write spi control register (/CS disabled serial mode)
;
;        return rv
;
;end
;
;local function quad_spi_read( addr, start, exit, fast )
;
;        local rv
;
;        --write the address to a test register
;        local temp = ((addr >> 16 ) & 0xFF)
;        print(help.hex(temp))
;        dict.nes("NES_CPU_WR", 0x5012, temp)  --write A23-16
;        temp = ((addr >> 8 ) & 0xFF)
;        print(help.hex(temp))
;        dict.nes("NES_CPU_WR", 0x5011, temp)  --write A15-8
;        temp = ((addr >> 0 ) & 0xFF)
;        print(help.hex(temp))
;        dict.nes("NES_CPU_WR", 0x5010, temp)  --write A7-0
;
;        --.spi_cs_n     (spi_ctl[0]),
;        --.spi_rw       (spi_access),
;        --.pdata_in     (data),
;        --.pdata_out    (spi_pdata_out),
;        --.quad_mode    (spi_ctl[2]),
;        --.quad_start   (spi_ctl[1]),
;        --.quad_exit    (spi_ctl[3]),
;
;        if (start==true) then
;                                        --0bxxxx_0010
;                dict.nes("NES_CPU_WR", 0x5000, 0x02)  --enable quad start & /CS
;
;                dict.nes("NES_CPU_WR", 0x5001, 0xEB)  --write spi data reg
;
;                                        --0bxxxx_0110
;                dict.nes("NES_CPU_WR", 0x5000, 0x06)  --enable quad start & quad mode read controller
;
;                dict.nes("NES_CPU_WR", 0x5001, 0x00)  --just need to trigger the read
;                                        --0bxxxx_0100
;                dict.nes("NES_CPU_WR", 0x5000, 0x04)  --done with the start, just stay in quad mode
;
;                rv = dict.nes("NES_CPU_RD", 0x5002, 0x00)  --read the quad value
;
;
;        elseif (exit==true) then
;                                        --0bxxxx_1100
;                dict.nes("NES_CPU_WR", 0x5000, 0x06)  --enable quad exit & quad mode read controller
;                dict.nes("NES_CPU_WR", 0x5001, 0x00)  --just need to trigger the read
;                rv = dict.nes("NES_CPU_RD", 0x5002, 0x00)  --read the quad value
;                                        --0bxxxx_1100
;                dict.nes("NES_CPU_WR", 0x5000, 0x01)  --disable quad mode and disable /CS
;
;        else
;                --leave modes as-is just trigger and perform the read
;                if (fast == true) then
;                                        --0bxxx1_0100
;                        dict.nes("NES_CPU_WR", 0x5000, 0x14)  --disable quad mode and disable /CS
;                else
;                                        --0bxxx0_0100
;                        dict.nes("NES_CPU_WR", 0x5000, 0x04)  --disable quad mode and disable /CS
;                end
;                dict.nes("NES_CPU_WR", 0x5001, 0x00)  --just need to trigger the read
;                rv = dict.nes("NES_CPU_RD", 0x5002, 0x00)  --read the quad value
;                dict.nes("NES_CPU_WR", 0x5000, 0x01)  --disable quad mode and disable /CS
;
;        end
;
;        return rv
;
;end

;Get the qspi pump running at full speed
qspipump_prime:

;spi_access( 0x05, nil, 1)
;--              dict.nes("NES_CPU_WR", 0x5000, 0x00)  --write spi control register (/CS enabled, serial mode)
;--              dict.nes("NES_CPU_WR", 0x5001, 0x05)  --write spi data reg (spi read SR1 command)
;--              dict.nes("NES_CPU_WR", 0x5001, 0x00)  --shift again to read first output byte
;--              print(help.hex(dict.nes("NES_CPU_RD", 0x5001)), "$5001 expect 0x??")
;--              dict.nes("NES_CPU_WR", 0x5001, 0x00)  --shift again to read first output byte
;--              print(help.hex(dict.nes("NES_CPU_RD", 0x5001)), "$5001 expect 0x??")
;--              dict.nes("NES_CPU_WR", 0x5000, 0x01)  --write spi control register (/CS disabled serial mode)
;
;                spi_access( 0x35, nil, 1)
;
;                --clear all status register bits
;                spi_access( 0x06, nil, 0)
;                spi_access( 0x05, nil, 1)
;                spi_access( 0x35, nil, 1)
;                spi_access( 0x01, nil, 2, 0x0000)
;                spi_access( 0x05, nil, 1)
;                spi_access( 0x35, nil, 1)
;
;                --read device ID
;                spi_access( 0xAB, nil, 4)
;                spi_access( 0x9F, nil, 3)
;                spi_access( 0x90, nil, 5)
;                --spi_access( 0x4B, nil, 20)
;
;                --write enable
;                spi_access( 0x06, nil, 0)
;                spi_access( 0x05, nil, 1)
;
;
;                --chip erase
;                spi_access( 0xC7, nil, 0)
;                spi_access( 0x05, nil, 1)
;                time.sleep(0.5);
;                spi_access( 0x05, nil, 1)
;
;
;                --write disable
;                spi_access( 0x04, nil, 0)
;                spi_access( 0x05, nil, 1)
;
;                --read byte
;                print("read")
;                spi_access( 0x03, 0x0012345, 1)
;
;                --write enable
;                spi_access( 0x06, nil, 0)
;                spi_access( 0x05, nil, 1)
;
;                --page program
;		print("write")
;                spi_access( 0x02, 0x0A5A5A, 1, 0xC3)
;                spi_access( 0x05, nil, 1)
;
;
;                --write disable
;                spi_access( 0x04, nil, 0)
;                spi_access( 0x05, nil, 1)
;
;
;
;                --read byte
;                print("serial read")
;                spi_access( 0x03, 0x0012345, 1)
;                print("serial read")
;                spi_access( 0x03, 0x00A5A5A, 1)
;
;
;                --enable quad mode on flash
;                spi_access( 0x06, nil, 0)       --write enable
;                spi_access( 0x05, nil, 1)       --read SR1
;                spi_access( 0x35, nil, 1)       --read SR2
;
;                print("write QE bit")
;                spi_access( 0x01, nil, 2, 0x0200) --write SR2_SR1
;                spi_access( 0x05, nil, 1)       --read SR1
;                spi_access( 0x35, nil, 1)       --read SR2
;
;                --perform first quad read
;                print("first quad read")
;                --time.sleep(5)
;
;                local tmp
;                tmp = quad_spi_read(0x0012345, true, false)
;                print("first quad read", help.hex(tmp))
;
;
;                --perform continuous quad read
;                --time.sleep(5)
;                tmp = quad_spi_read(0x0012345, false, false)
;                print("continuous quad read", help.hex(tmp))
;
;                tmp = quad_spi_read(0x0012345, false, false, true)
;                print("fast quad read", help.hex(tmp))
;                tmp = quad_spi_read(0x00A5A5A, false, false, true)
;                print("fast quad read", help.hex(tmp))
;                tmp = quad_spi_read(0x0012345, false, false, true)
;                print("fast quad read", help.hex(tmp))
;                tmp = quad_spi_read(0x00A5A5A, false, false, true)
;                print("fast quad read", help.hex(tmp))
;
;                --perform final quad read
;                tmp = quad_spi_read(0x0012345, false, true)
;                print("last quad read", help.hex(tmp))
;
;
;                --clear quad enable bit
;		spi_access( 0x06, nil, 0)       --write enable
;                spi_access( 0x05, nil, 1)       --read SR1
;                spi_access( 0x35, nil, 1)       --read SR2
;
;                print("clear QE bit")
;                spi_access( 0x01, nil, 2, 0x0000) --write SR2_SR1
;                spi_access( 0x04, nil, 0)
;                spi_access( 0x05, nil, 1)       --read SR1
;                spi_access( 0x35, nil, 1)       --read SR2
;                --TODO why is QE bit still set..??
;
;
;                --perform normal read
;                print("serial read")
;                spi_access( 0x03, 0x0012345, 1)
	
rts;qspipump_prime:


; nmi routine
nmi:
	; preserve registers
	phaxy
	;phy

	; prevent NMI re-entry
	lda nmi_lock
	beq :+
		jmp @nmi_end
	:
	lda #1
	sta nmi_lock
	; increment frame counter
	inc nmi_count
	;
	lda nmi_ready
	bne :+ ; nmi_ready == 0 not ready to update PPU
		jmp @ppu_update_end
	:
	cmp #2 ; nmi_ready == 2 turns rendering off
	bne :+
		lda #%00000000
		sta $2001
		ldx #0
		stx nmi_ready
		;stx rendering_on
		jmp @ppu_update_end
	:

	;rendering is on
	;ldx #1
	;stx rendering_on

	; sprite OAM DMA
	ldx #0
	stx $2003
	lda #>oam
	sta $4014

	; palettes
	;lda #%10001000 ;sprites PT1
	lda #%10000000 ;sprites PT0
	sta $2000 ; set horizontal nametable increment

	;check if palette update needed
	bit palette_flag ;N set if update needed, V set if both pallettes to be updated, bit4 1-spr only, 0-bg only
	beq @done_pal_update
	ldy #32 ;number of palette entries to update (assume all 32 for now, change later if not needed)
	bvs @all_pal_update
		;only one palette needs updated
		;mask out bit7 of palette_flag to provide address LSByte of which palette to update
		lda palette_flag
		and #WHICH_PAL_MASK
		tax ;store in X, will be written as starting address below
		ldy #16 ;only update 16bytes

	@all_pal_update:
		;X set to starting palette LSByte
		;Y set to number of palette entries to update
		lda $2002
		lda #$3F
		sta $2006
		stx $2006 ; set PPU address to $3F00
		ldx #0
		:
			;TODO could reverse the order of pallette_buff to allow using post-dec loop without cpx
			;would save 2 cpu cycles per palette update only using Y indexing
			lda palette_buff, X
			sta $2007
			inx
			;cpx #32	;number of palettes
			;bcc :-
			dey	;remaining updates
			bne :-

		;clear the flag so don't come back until another update is requested
		;can cut this out or load something else to see worst case updating palettes every frame
		;lda #0
		;sta palette_flag

	@done_pal_update:


;	;select HUD bank
;	;lda #0
;	;jsr set_chr_bank
;	;sel_chr_bank 0, CODE_BANK breaks scope...
;	@sty_label:
;	;ldy #(num<<4)	;CCCC LLPP
;	ldy_chr_bank 0, CODE_BANK
;	sty @sty_label+1


	; nametable update
	ldx #0	;if zero no updates needed
	cpx nmt_update_len
	jcs @scroll

;FAST_HORZ = 58	;//constant for nmt_update to use fast auto inc
;LAST_HORZ_NT = 50 ;//last NT update is only 48 Bytes still not div by 3
	ldx #FAST_HORZ ;58 not divisible by 3, so designates fast auto inc update
			;also used for tile rows (32+2)
		;NOTE!  changing this requires loop
	cpx nmt_update_len
	beq @nmt_autoinc_update

	ldx #LAST_HORZ_NT  ;50 not div by 3
	;THIS WAS MISSING...?
	cpx nmt_update_len
	beq @nmt_autoinc_update

	ldx #TILE_ROW  ;34 not div by 3
	cpx nmt_update_len
	beq @nmt_autoinc_update

	ldx #AT_ROW_WR  ;10 not div by 3
	cpx nmt_update_len
	beq @nmt_autoinc_update

;AT_ROW_RD = 11 write attr table row (8B)
	ldx #AT_ROW_RD
	cpx nmt_update_len
	beq @nmt_at_row_rd
	
	;NORMAL UPDATE allow updating non-sequential bytes
	ldx #0
	;data structure addr_hi, addr_lo, data, ....
	@nmt_update_loop:
		lda nmt_update, X
		sta $2006
		inx
		lda nmt_update, X
		sta $2006
		inx
		lda nmt_update, X
		sta $2007
		inx
		cpx nmt_update_len
		bcc @nmt_update_loop

	lda #0
	sta nmt_update_len
	jmp @scroll

	@nmt_autoinc_update:
;use the buffer for fast updates where $2006 auto increments
;and nmt_update buffer: first 2 bytes start address, remainder data
;have time for ~81 $2006/2007 writes per NMI
; 56 bytes per NMI = 15.5 frames to update play field (888B total)
; When we only want to update a tile row this same function works as well.

		ldx #0	;set to beginning of buffer

		;load start address
		lda nmt_update, X
		sta $2006
		inx
		lda nmt_update, X
		sta $2006
		inx

		@nmt_autoinc_loop:
	;$2000 already set to inc by 1
		lda nmt_update, X
		sta $2007
		inx
		cpx nmt_update_len
		bcc @nmt_autoinc_loop
	lda #0
	sta nmt_update_len
	;done with updates
	jmp @scroll

	;READ a row of AT bytes
	;DATA STRUCTURE: ADDRHI - ADDRLO - DATA0 - DATA1 ...- AND-MASK
	@nmt_at_row_rd:

		ldx #0	;set to beginning of buffer

		;load start address
		lda nmt_update, X
		sta $2006
		inx
		lda nmt_update, X
		sta $2006
		inx
	
		;nmt_update_len is 1 extra for option AND-MASK
		;11 -2addr -1'READ' = 8 total bytes to read
		lda $2007	
		;inx ;discard first read and increment over AND-MASK

		;load Y with AND-MASK index
		;this could be hard coded if only using for one function
		dec nmt_update_len
		ldy nmt_update_len ;account for MASK not being stomped

		@at_rd_loop:
		;$2000 already set to inc by 1
			lda $2007
			;TODO perhaps AND in the 3rd byte
			;and nmt_update+2 ;and provided AND-MASK
			and nmt_update, Y ;and provided AND-MASK
					;(ie 0xF0/0x0F to mask a nibble)
					;could be hardcoded if len always 11
			sta nmt_update, X
			inx
			cpx nmt_update_len
			bcc @at_rd_loop
		lda #0
		sta nmt_update_len

	;set scroll to top of HUD
@scroll:
	lda scroll_nmt
	;HUD is always in NT0
	;lda #0
	and #%00000011 	;keep only lowest 2 bits to prevent error
	;ora #%10001000 ;sprites PT1
	ora #%10000000
	sta $2000
	lda scroll_x
	;lda #0	;top left $2000
	sta $2005
	lda scroll_y
	sta $2005

	; enable rendering
	lda #%00011110
	sta $2001
	; flag PPU update complete
	ldx #0
	stx nmi_ready

@ppu_update_end:
	; if this engine had music/sound, this would be a good place to play it
	; unlock re-entry flag
	lda #0
	sta nmi_lock

@nmi_end:
	; restore registers and return
	plaxy
	rti
	;ply
	;jmp nmi_exit_stub
	;jmp $FFE8


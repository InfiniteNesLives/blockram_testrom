
;REGISTER RULES
;any function can assume free use of these first registers
;you should push these registers before calling a function
;if you want them to be preserved
reg0:           .res 1 ; temp register
reg1:		.res 1 ; temp register
reg2:		.res 1 ; temp register
reg3:		.res 1 ; temp register
arg0 = reg0	;reg0 used as function argument for readability
arg1 = reg1
arg2 = reg2
arg3 = reg3
;'main thread' or a function that calls another function
; can assume these registers are preserved when calling
; a lower function.
; CAUTION!  Where you draw this line exactly isn't easily
; defined...
;IF THESE REGISTERS ARE USED, SHOULD BE LISTED/ASSIGNED DIRECTLY ABOVE FUNCTION DEFINITION
;this makes it easy to determine if calling said function will corrupt these registers
reg4:		.res 1
reg5:		.res 1
reg6:		.res 1
reg7:		.res 1
reg8:		.res 1
reg9:		.res 1
reg10:		.res 1
reg11:		.res 1
reg12:		.res 1
reg13:		.res 1
reg14:		.res 1
reg15:		.res 1

ptr0:		.res 2 ; 16bit pointer
ptr0_lo = ptr0
ptr0_hi = ptr0+1
ptr1:		.res 2 ; 16bit pointer
ptr1_lo = ptr1
ptr1_hi = ptr1+1


gamepad: .res 1
gamepad_last: .res 1	;last frame's gamepad value for edge detection
gamepad_new: .res 1	;buttons which were newly pressed this frame

gamepad2: .res 1
gamepad2_last: .res 1	;last frame's gamepad value for edge detection
gamepad2_new: .res 1	;buttons which were newly pressed this frame

nmi_lock:       .res 1 ; prevents NMI re-entry

;timing.s sensing of TV system needs ZP variable, can then use it in main..
nmis:		;.res 1
nmi_count:      .res 1 ; is incremented every NMI


nmi_ready:      .res 1 ; set to 1 to push a PPU frame update, 2 to turn rendering off next NMI
;ppu_temp_addr:	.res 2 ; used to temporarily store PPU address for scrolling
			;//this is a big endian address for $2006 reasons
nmt_update_len: .res 1 ; number of bytes in nmt_update buffer
;//normal len is multiple of 3 AHI-ALO-BYTE;
;//DEFINES musn't be a multiple of 3, these define special updates
;// don't want a normal update to trigger a special one! TRUST THIS IS TRUE!
FAST_HORZ = 58	;//constant for nmt_update to use fast auto inc
		;//update a tile row(32B) for vertical scrolls
LAST_HORZ_NT = 50 ;//last NT update is only 48 Bytes still not div by 3
TILE_ROW = 34 ;//update a tile row(32B) for vertical scrolls
AT_ROW_WR = 10;//write AT row (8B) + 2B addr
AT_ROW_RD = 11;//rd attr table row (8B) + 2B AT-ADDR + 1B AND-MASK
		;// NOTE! This value is used for setting AND-MASK!


rand_seed: .res 2	;16bit LFSR to generate 8bit random number
			;init to anything besides zero at startup


;peek poke access pointer table
aptr_table:
aptr_table_lo = aptr0_lo
aptr_table_hi = aptr0_hi

aptr0: .res 2 ;16bit ptr
aptr0_lo = aptr0
aptr0_hi = aptr0+1
aptr1: .res 2 ;16bit ptr
aptr1_lo = aptr1
aptr1_hi = aptr1+1
aptr2: .res 2 ;16bit ptr
aptr2_lo = aptr2
aptr2_hi = aptr2+1
aptr3: .res 2 ;16bit ptr
aptr3_lo = aptr3
aptr3_hi = aptr3+1


;pat_banktable:
;.byte $00, $10, $20, $30  ;4banks of CHR-RAM

load_chrram:

	;CHR-RAM BANK 0
	;select desired chr-ram bank
;	@bank0:
;	ldy #0	;chr0 prg0
;	sty @bank0+1
	sel_chr_bankY 0, PT_BANK
	;jsr set_chr_bank

	;select desired pattern table
	lda #$00	;BG
	sta reg0

	;select source data
	lda #>hud_pt	;hi byte
	sta nat_ptr+1
	lda #<hud_pt	;lo byte
	sta nat_ptr
	jsr load_pattern_table


	;select desired pattern table
	lda #$10	;SPR
	sta reg0

	;select source data
	lda #>zoe_pt	;hi byte
	sta nat_ptr+1
	lda #<zoe_pt	;lo byte
	sta nat_ptr
	jsr load_pattern_table


	;CHR-RAM BANK 1
	;select desired chr-ram bank
	;lda #1
	;jsr set_chr_bank
	;@bank1:
	;ldy #$10	;chr1 prg0
	;sty @bank1+1
	sel_chr_bankY 1, PT_BANK

	;select desired pattern table
	lda #$00	;BG
	sta reg0

	;select source data
	lda #>fnvalley_pt	;hi byte
	sta nat_ptr+1
	lda #<fnvalley_pt	;lo byte
	sta nat_ptr
	jsr load_pattern_table


	;select desired pattern table
	lda #$10	;SPR
	sta reg0

	;select source data
	lda #>zoe_pt	;hi byte
	sta nat_ptr+1
	lda #<zoe_pt	;lo byte
	sta nat_ptr
	jsr load_pattern_table

;rts ;load_chrram
rts_far_func ;if using jmp for far code, need to jump back

;reg0 contain $00/$10 upper address byte of pattern table address
;nat_ptr set to source tile data to be copied over
load_pattern_table:

	; first nametable, start by clearing to empty
	ldx $2002 ; reset latch
	;lda #$20 ; default HUD address MSB
	lda reg0
	sta $2006
	lda #0	  ; LSB of address
	sta $2006

	;copy over 4KBytes of data
	ldx #15  ;page counter 15-0 (16x 256B pages)
	ldy #0	 ;Byte counter
	jmp @next_page

	@start_page:
	inc nat_ptr+1 ;MSByte of pointer
	dex 
	bpl @next_page

	;we're done
	rts

		@next_page:
			lda (nat_ptr), y
			sta $2007
			iny
			bne @next_page

			baeq @start_page


	;rts done up there ^
;load_chrram:


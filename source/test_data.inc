
title_string:
.asciiz "Paul's INL test rom! :]"
;.asciiz "Pauls INL !$:-.',_[]/"

instruction_str:
.asciiz "B+pad:chg val, St:run/clr"

test_string:
.asciiz "Select test:"

;TT_WORDS_PER_ENTRY = 1
TT_WORDS_PER_ENTRY = 2

FIRST_TEST = 7
START_RUNTEST = 0 ;0-false 1-true

test_table:

test_table_names:
;#0
.addr (tv_string)
test_table_fptrs:
.addr (tv_test)

;#1
.addr (ctlr_string)
.addr (ctlr_test)

;#2
.addr (peekpoke_string)
.addr (peekpoke_test_init)

;#3
.addr (pciccom_string)
.addr (pciccom_test_init)

;#4
.addr (esp_at_string)
.addr (esp_at_test_init)

;#5
.addr (checksum_string)
.addr (checksum_test_init)

;#6
.addr (userflash_string)
.addr (userflash_test_init)

;#7
.addr (qspipump_string)
.addr (qspipump_test_init)

test_table_end:

tv_string:
.asciiz "TV Region"

ctlr_string:
.asciiz "Cntlr 1-2"

peekpoke_string:
.asciiz "Peek Poke"

pciccom_string:
.asciiz "SPIciccom"

esp_at_string:
.asciiz "ESP-AT T#"

checksum_string:
.asciiz "Check sum"

userflash_string:
.asciiz "UserFlash"

qspipump_string:
.asciiz "QSPI pump"



;move using Accumulator		dst <- src
.macro	mva	dst,  src
	lda src
	sta dst
.endmacro

;move using X register		dst <- src
.macro	mvx	dst,  src
	ldx src
	stx dst
.endmacro

;move using Y register		dst <- src
.macro	mvy	dst,  src
	ldy src
	sty dst
.endmacro

;move address into pointer using A
.macro	mva_ptr	ptr, addr 
	lda #<(addr)
	sta ptr+0 ;lo byte
	lda #>(addr) 	;parenthesis very important allows for calling with offsets (addr+1)
	sta ptr+1 ;hi byte
.endmacro

;move address into pointer using A with X offset
.macro	mva_ptr_tablex ptr, table 
	lda table, X
	sta ptr+0 ;lo byte
	lda table+1, X	;parenthesis very important allows for calling with offsets (addr+1)
	sta ptr+1 ;hi byte
.endmacro

;stack operations with X/Y
;NOTE!  Accumulator is stomped of course!
.macro phx
	txa
	pha
.endmacro
.macro phy
	tya
	pha
.endmacro
.macro plx
	pla
	tax
.endmacro
.macro ply
	pla
	tay
.endmacro
;push all registers, these depend on eachother for assumed order
.macro phaxy ;push A, X, Y
	pha 
	phx
	phy
.endmacro
.macro plaxy ;pull Y, X, A
	ply 
	plx
	pla
.endmacro


;branch always macros
;kind of dumb, but denotes that the instruction is intended to be a branch always
;and is assuming the state of the given flag
;using 'branch always' saves 1 Byte and 0 cycles (can assume branch taken)
;if a page boundary is crossed, this instruction actually takes 4 cycles (1 more than jmp)
;so in some cases this instruction trades a byte for a cycle (crossing page boundary)
;for non-page boundary crossings it saves a byte..  worth the mental gymnastics..?  Prob not..
;but if one decides to do such a thing, then these macros improve readability
.macro bacc label
	bcc label
.endmacro
.macro bacs label
	bcs label
.endmacro
.macro bane label
	bne label
.endmacro
.macro baeq label
	beq label
.endmacro
.macro bapl label
	bpl label
.endmacro
.macro bami label
	bmi label
.endmacro
.macro bavs label
	bvs label
.endmacro
.macro bavc label
	bvc label
.endmacro

;RTS jumping
.macro pha_fptr_y ptr_msb
	lda ptr_msb, y ;Big endian MSByte
	pha
	iny
	lda ptr_msb, y ;Big endian LSByte
	pha
.endmacro
.macro pha_fptr_x ptr_msb
	lda ptr_msb, x ;Big endian MSByte
	pha
	inx
	lda ptr_msb, x ;Big endian LSByte
	pha
.endmacro
.macro jsp  ;jump to the address on the stack (StackPointer points to)
	rts
.endmacro
.macro rtj label ;need to know label of return location
	jmp label
.endmacro

;fptr must be in little endian (LSB first, MSB last)
;pushing jump address to the stack, MSB must be pushed first
;fptr must also be the function address minus 1, since RTS increments the stacked addr
;note Acc is corrupted, with update to use ptr_lsb+1 the X/Y register is unchanged
.macro ph_fptr ptr_lsb, idx_reg

;.match is used (and works) I think due to X and Y being reserved keywords
;.warning messages can be uncommented for macro debugging

	.if ((.match ({idx_reg}, y)))
	;Abs, Y indexed addressing
	lda ptr_lsb+1, y ;MSByte
	pha
	lda ptr_lsb, y ;Little endian LSByte
	pha
;	.warning "ph_fptr, y"

	.elseif ((.match ({idx_reg}, x)))
	;Abs, X indexed addressing
	lda ptr_lsb+1, x ;MSByte
	pha
	lda ptr_lsb, x ;Little endian LSByte
	pha
;	.warning "ph_fptr, x"

	.elseif (.blank(idx_reg))
	;Absolute non-indexed addressing
	lda ptr_lsb+1 ;MSByte
	pha
	lda ptr_lsb ;Little endian LSByte
	pha
;	.warning "ph_fptr abs"
	.else
		;idx_reg must be blank, x, or y
		.error "Syntax error"
	.endif

.endmacro

;Jump Through the Stack
;can use Absolute, or Absolute X/Y addressing
;fptr must be in big endian (MSB first, LSB last)
;A gets corrupted while pushing jump address, X/Y unchanged
;returning from the function requires jumping to the address that follows
.macro jts fptr_msb, idx_reg
	ph_fptr fptr_msb, idx_reg
	jsp
.endmacro

;simpler option that uses function pointers using indirect jump
;effectively indirect jsr
;push return address before jumping 
;caution this breaks scope and emits a label so can't local label reference across it
;and un-named labels may reference into it...
;TODO if used the same pointer repeatedly it would make sense to put this in a subroutine with fixed 
;function pointer address.  Even if only used ~once I think it's faster and fewer bytes...
;so while this works, it can't beat jsr to a one line "jmp (fptr)" subroutine.
;.macro jsri fptr
;.scope 
;	lda #>(@ret_label-1)
;	pha ;hi byte
;	lda #<(@ret_label-1)
;	pha ;lo byte
;	jmp (fptr)
;	@ret_label:
;.endscope
;.endmacro

;One step further to make function calls operate like subroutines
;need an actual subroutine to call so that can return from there.
.macro jsra table
.endmacro


;ca65 match example:
.macro  asr     arg ;arithmetic shift right
	.if (.not .blank(arg)) .and (.not .match ({arg}, a))
		.error "Syntax error"
	.endif
		cmp     #$80            ; Bit 7 into carry
		lsr     a               ; Shift carry into bit 7
.endmacro
									  

;multiply by power of 2, result stored in arg (multiplicand)
;supports Acc, ZP, & Absolute addressing
;could be modified to support X indexed addressing since ASL supports ZP, X, and Abs, X
.macro mul arg, val
	.if (.blank(arg) .or .blank(val))
		.error "Syntax error, must provide multiplicand and multiplier"
	.endif

;xmatch required to be used otherwise any number equals any number
;think if xmatch as "exact" I think..
	.if ((.xmatch ({val}, #2)))
		asl arg	;arg * 2
	;	.warning "mul 2"
	.elseif ((.xmatch ({val}, #4)))
		asl arg ;arg * 2
		asl arg ;arg * 4
	;	.warning "mul 4"
	.elseif ((.xmatch ({val}, #8)))
		asl arg ;arg * 2
		asl arg ;arg * 4
		asl arg ;arg * 8
	;	.warning "mul 8"
	.elseif ((.xmatch ({val}, #16)))
		asl arg ;arg * 2
		asl arg ;arg * 4
		asl arg ;arg * 8
		asl arg ;arg * 16
	;	.warning "mul 16"
	.elseif ((.xmatch ({val}, #32)))
		asl arg ;arg * 2
		asl arg ;arg * 4
		asl arg ;arg * 8
		asl arg ;arg * 16
		asl arg ;arg * 32
	;	.warning "mul 32"
;ROR with and might be better..?
;	.elseif ((.match ({val}, #64)))
;		asl arg ;arg * 2
;		asl arg ;arg * 4
;		asl arg ;arg * 8
;		asl arg ;arg * 16
;		asl arg ;arg * 32
;		asl arg ;arg * 64
;	.elseif ((.match ({val}, #128)))
;		asl arg ;arg * 2
;		asl arg ;arg * 4
;		asl arg ;arg * 8
;		asl arg ;arg * 16
;		asl arg ;arg * 32
;		asl arg ;arg * 64
;		asl arg ;arg * 128
	.else
		.error "Syntax error, multipler must be immediate & power of 2 < 64"
	.endif
.endmacro

;divide by power of 2, result (quotent) stored in dividend arg
;supports Acc, ZP, & Absolute addressing
;could be modified to support X indexed addressing since LSR supports ZP, X, and Abs, X
.macro div dividend, divisor
	.if (.blank(dividend) .or .blank(divisor))
		.error "Syntax error, must provide divisor and immediate divisor"
	.endif

	.if ((.xmatch ({divisor}, #2)))
		lsr dividend	;arg / 2
	;	.warning "div 2"
	.elseif ((.xmatch ({divisor}, #4)))
		lsr dividend ;arg / 2
		lsr dividend ;arg / 4
	;	.warning "div 4"
	.elseif ((.xmatch ({divisor}, #8)))
		lsr dividend ;arg / 2
		lsr dividend ;arg / 4
		lsr dividend ;arg / 8
	;	.warning "div 8"
	.elseif ((.xmatch ({divisor}, #16)))
		lsr dividend ;arg / 2
		lsr dividend ;arg / 4
		lsr dividend ;arg / 8
		lsr dividend ;arg / 16
	;	.warning "div 16"
	.elseif ((.xmatch ({divisor}, #32)))
		lsr dividend ;arg / 2
		lsr dividend ;arg / 4
		lsr dividend ;arg / 8
		lsr dividend ;arg / 16
		lsr dividend ;arg / 32
	;	.warning "div 32"
;ROL with and might be better..?
;	.elseif ((.match ({divisor}, #64)))
;		lsr dividend ;arg / 2
;		lsr dividend ;arg / 4
;		lsr dividend ;arg / 8
;		lsr dividend ;arg / 16
;		lsr dividend ;arg / 32
;		lsr dividend ;arg / 64
;	.elseif ((.match ({divisor}, #128)))
;		lsr dividend ;arg / 2
;		lsr dividend ;arg / 4
;		lsr dividend ;arg / 8
;		lsr dividend ;arg / 16
;		lsr dividend ;arg / 32
;		lsr dividend ;arg / 64
;		lsr dividend ;arg / 128
	.else
		.error "Syntax error, divisor must be immediate & power of 2 < 64"
	.endif
.endmacro


;insert string of hex values separated by spaces
;example use: hex 01 03 a0 ba 23
;found here: http://forum.6502.org/viewtopic.php?f=2&t=2570&start=15#p29096
;.macro hex data
;    .ifblank data
;        .error "Please provide some data!"
;    .endif
;    .byte $.left(1,data) ;This line errors, IDK what's going on enough to fix...
;    .if .tcount(data) > 1
;        hex .mid(1, .tcount(data) - 1, data)
;    .endif
;.endmacro

;nibble swap of Accumuator
;written by Garth Wilson idea by David Galloway
;http://6502.org/source/general/SWN.html
;8Bytes 12Cycles
.macro swn arg
	.if (.not .blank(arg)) .and (.not .match ({arg}, a))
		.error "Syntax error, nibble swap must operate on A"
	.endif
	asl  a ;Accumulator Nibble swap
	adc  #$80
	rol  a
	asl  a
	adc  #$80
	rol  a
.endmacro


;https://cc65.github.io/doc/ca65.html#ss10.23
;The following macro encodes a string as a pascal style string with a leading length byte.
.macro  pstring Arg
	.byte   .strlen(Arg), Arg
.endmacro

.macro  pline Arg
	.byte   (.strlen(Arg)+1), Arg
	.byte $0A ;LF
.endmacro

.macro  patcmd Arg
	.byte   (.strlen(Arg)+2), Arg
	.byte $0D ;CR
	.byte $0A ;LF
.endmacro

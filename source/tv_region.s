
;
; NES TV system detection code
; Copyright 2011 Damian Yerrick
;
; Copying and distribution of this file, with or without
; modification, are permitted in any medium without royalty provided
; the copyright notice and this notice are preserved in all source
; code copies.  This file is offered as-is, without any warranty.
;
;What to change
;    PAL NES needs different music pitch from NTSC and Dendy.
;    NTSC needs different music speed from PAL NES and Dendy.
;    PAL NES needs different raster effects from NTSC and Dendy.
;    NTSC needs different scrolling and sprite movement speeds from PAL and Dendy.
;    PAL NES needs OAM DMA done first in vblank, while NTSC benefits from doing it 
;	last if using timed code to work around the DMC DMA glitch.
;    If you want to take advantage of PAL's longer vertical blank to load more tiles 
;	into CHR RAM, Dendy needs different detection of the end of rendering.
;
;The following code detects the TV system. A game can use it to compensate for 
;  differences in clock rate among various NES models. It has been tested on NTSC NES 
;  and Famicom, PAL NES, and PAL famiclones using the Dendy chipset, as well as emulators 
;  in NTSC, PAL, and Dendy modes. 
;
;https://wiki.nesdev.com/w/index.php/Detect_TV_system

.export getTVSystem
;.importzp nmis moved to zp.inc

.align 32  ; ensure that branches do not cross a page boundary

;;
; Detects which of NTSC, PAL, or Dendy is in use by counting cycles
; between NMIs.
;
; NTSC NES produces 262 scanlines, with 341/3 CPU cycles per line.
; PAL NES produces 312 scanlines, with 341/3.2 CPU cycles per line.
; Its vblank is longer than NTSC, and its CPU is slower.
; Dendy is a Russian famiclone distributed by Steepler that uses the
; PAL signal with a CPU as fast as the NTSC CPU.  Its vblank is as
; long as PAL's, but its NMI occurs toward the end of vblank (line
; 291 instead of 241) so that cycle offsets from NMI remain the same
; as NTSC, keeping Balloon Fight and any game using a CPU cycle-
; counting mapper (e.g. FDS, Konami VRC) working.
;
; nmis is a variable that the NMI handler modifies every frame.
; Make sure your NMI handler finishes within 1500 or so cycles (not
; taking the whole NMI or waiting for sprite 0) while calling this,
; or the result in A will be wrong.
;
; @return A: TV system (0: NTSC, 1: PAL, 2: Dendy; 3: unknown
;         Y: high byte of iterations used (1 iteration = 11 cycles)
;         X: low byte of iterations used
.proc getTVSystem
    ldx #0
    ldy #0
    lda nmis
nmiwait1:
    cmp nmis
    beq nmiwait1
    lda nmis

nmiwait2:
    ; Each iteration takes 11 cycles.
    ; NTSC NES: 29780 cycles or 2707 = $A93 iterations
    ; PAL NES:  33247 cycles or 3022 = $BCE iterations
    ; Dendy:    35464 cycles or 3224 = $C98 iterations
    ; so we can divide by $100 (rounding down), subtract ten,
    ; and end up with 0=ntsc, 1=pal, 2=dendy, 3=unknown
    inx
    bne :+
    iny
:
    cmp nmis
    beq nmiwait2
    tya
    sec
    ;sbc #10
    sbc #20 ;want negative values for PAL/DENDY will adjust to make NTSC positive for frame counter
;    cmp #3
;    bcc notAbove3
;    lda #3	;unknown
;notAbove3:
    rts
.endproc


tv_test:
	;determine what TV system/console we're running on
	jsr getTVSystem
	sta system_type
	; @return A: TV system (-10: NTSC, -9: PAL, -8: Dendy
	; @return A: TV system ($F6: NTSC, F7: PAL, F8: Dendy
	;pha ;region
	;ldx #LEFT_MARGIN
	;ldy #NAME_ROW+1
	;jsr display_hex_byte

	;;display name
	;pla ;region
	cmp #NTSC
	bne :+
		mva_ptr ptr0, ntsc_string
	:
	cmp #PAL
	bne :+
		mva_ptr ptr0, pal_string
	:
	cmp #DENDY
	bne :+
		mva_ptr ptr0, dendy_string
	:

	ldx #LEFT_MARGIN
	ldy #NAME_ROW+1
	jsr ppu_write_string

	;test only runs one frame
	mva_ptr test_fptr, @done_testing
	@done_testing:

;	cmp #NTSC
;	bne :+
;		adc #-NTSC+4 ;carry is set so it'll be 5 now
;	:
;	sta skip_frame	 ;initial value
rts ;tv_test

ntsc_string:
.asciiz "NTSC"
pal_string:
.asciiz "PAL"
dendy_string:
.asciiz "DENDY"

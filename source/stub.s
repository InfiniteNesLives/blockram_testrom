
.macro resetstub_in segname
.segment segname
.scope

prg_bank_id:
.byte .bank(*)

resetstub_entry:
	sei		;1B  - not sure we actually need this in every bank...
	@bankval:
	ldx #$FF	;2B
	stx @bankval+1	;3B
	;now the last bank is selected
	jmp reset	;3B
  .addr nmi, resetstub_entry, irq ;6B starting at $FFFA
  ;.addr nmistub_entry, resetstub_entry, irq ;6B starting at $FFFA
;NOTE: the nmi & irq vectors are in each bank, but the code/handlers for them isn't, so it would be a prob if they happened..
;might make more sense to put something meaningful in those vectors, or have stubs that aren't code bank have a small handler
;that gets to the code bank for the handler and back again..
.endscope
.endmacro

;segment is the argument to the resetstub_in macro
resetstub_in "STUB00"
;.IFNDEF PRG32
;resetstub_in "STUB01"
;resetstub_in "STUB02"
;resetstub_in "STUB03"
;.ENDIF
;.IFDEF PRG512
;resetstub_in "STUB04"
;resetstub_in "STUB05"
;resetstub_in "STUB06"
;resetstub_in "STUB07"
;resetstub_in "STUB08"
;resetstub_in "STUB09"
;resetstub_in "STUB10"
;resetstub_in "STUB11"
;resetstub_in "STUB12"
;resetstub_in "STUB13"
;resetstub_in "STUB14"
;resetstub_in "STUB15"
;.ENDIF

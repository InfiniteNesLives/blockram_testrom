
;START ALIGNMENT CONCERNS
.align 8  ; ensure that delay_reg0 bne does not cross a page boundary

;delay ~1ms on NTSC systems with 1.79Mhz sysclk
delay_1ms:
;reg0 used
	;lda #222 ;1792 cycles total = 1.001msec on NTSC
	lda #221 ;go for one less to be more accurate with delay_Xms
		;measured 221 to be perfect! 10.00055msec from jsr/rts delay_Xms
	sta reg0
;roll through to function below with desired arg

;about 8CPU cycles per count of reg0
;max=255 -> 2051 cycles from jsr delay_reg0 -> instruction that follows from calling routine
;255*8+15 jsr here, and rts back
;about 4.47usec per value of reg0, max 255 = ~1.146msec
.proc delay_reg0
	dec reg0
	bne delay_reg0
;END ALIGNMENT CONCERNS (8B total)
rts
.endproc ;delay_reg0

;X=number of ms would like to wait
;ldx #10 = 17901cyc = 10.00055msec perfect!
.proc delay_Xms
;reg0 used by called function
	jsr delay_1ms
	dex
	bne delay_Xms
rts
.endproc ;delay_Xms


;ptr0 -> memory read start address
;ptr1 -> memory write start address
;Y = number of bytes to copy (max = 0 = 256) also used for indexing
memcpy_max256:
	;pre-decrement to account for last byte
	dey

	@next_byte:
		lda (ptr0),y
		sta (ptr1),y
		dey
		bne @next_byte

	;copy last (zero index) byte
	lda (ptr0),y
	sta (ptr1),y

rts;memcpy_max256

;use memcpy_max256 to move multiple pages
;ptr0 -> memory read start address
;ptr1 -> memory write start address
;X = number of 256B pages
.proc memcpy_Xpages

	ldy #0	;arg 0->256 for memcpy_max256
	@next_page:
		jsr memcpy_max256 ;Y=0 on exit
		inc ptr0_hi
		inc ptr1_hi
		dex
		bne @next_page

rts
.endproc ;memcpy_Xpages

;ptr0 -> memory read start address
;ptr1 -> memory write start address
;copy string including null terminator, then stop
;updates pointers to point to last copied byte for both read & write strings
;this allows recalling this function to stitch multiple strings together with only 1 null terminator at end
strcpy:
;X unused

	ldy #$FF ;will predec to zero for first copy

	@next:
		iny 
		lda (ptr0),y
		sta (ptr1),y
		bne @next

	;update pointers to point to last copied byte for both read & write strings (null term)
	tya
	add ptr0_lo
	sta ptr0_lo
	bcc :+
		inc ptr0_hi
	:
	tya
	add ptr1_lo
	sta ptr1_lo
	bcc :+
		inc ptr1_hi
	:

rts;strcpy


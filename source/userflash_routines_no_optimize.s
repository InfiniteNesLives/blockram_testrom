
;This code runs from system RAM
;cannot assume userflash is present & readable because it won't be readable during erase/writes..

;X = MODE
;Y = Xpage to run sequence on (erase/write)
;A = Tpe spec (if == UFLASH_TPEP will set the preprogram bit before running seq)
;ptr0 -> 256B of data would like to copy into page buffer (if using special write page latches while erasing)
.proc run_sequence
;#3 Preprogram the selected memory as virtual "1";
;After being erased, the data would be "0"; and after write operation,
;the data would be "1". An erased location "0" can be programmed to "1",
;but a programmed location"1" can not be programmed to "0", so erasing is
;always needed for a new write operation.
;
;Before erase and write operations, the selected memory needs to be
;written to the virtual "1" through the pre-program operation, which first
;requires to set PEP (pre-program) to MODE "0001" and then write to the
;selected at high-level, which takes hundreds of microseconds.

	pha ;Tpe spec

	sty UFLASH_XPAGE

	;switch to manual aclk
	;mva_uflash_clk_lo ;stop the clk
	ldy #UFLASH_CLKCTL_MAN0 ;used below too!
	sty UFLASH_CLKCTL

	;If Pre-Program, set the pre-program bit first
	cmp #UFLASH_TPEP
	bne :+
		lda #UFLASH_MODE_PEP
		sta UFLASH_MODE
		mva_uflash_clk_hi ;clock in the new mode
		sty UFLASH_CLKCTL ;clk->lo
		
	:


	;select mode, seq, & Xpage
;SEQ1
	;lda #UFLASH_MODE_PEP
	stx UFLASH_MODE ;PASSED ARG
	lda #UFLASH_SEQ1
	sta UFLASH_SEQ
	;Xpage done above
	
	mva_uflash_clk_hi ;clock the mode change
	mva_uflash_clk_lo

;TS1 = 15+usec

;meas: 10.1usec, add delay
	nop ;1.1us each
	nop
	nop
	nop 
	nop ;15.6us
	nop
	nop ;17.9us

;SEQ2
	lda #UFLASH_SEQ2
	sta UFLASH_SEQ

	
	mva_uflash_clk_hi ;clock the mode change
	;mva_uflash_clk_lo

;TS2P = 5-10usec
;6usec = 10.7 CPU cycles...

;meas: 3.9us, add delay
	nop ;1.1us, each
	nop
	nop ;6.74us
	nop ;7.86us

	mva_uflash_clk_pe_set

;TPEP = 190-210usec
	;reg0=22 for every 100usec
	;lda #40	;meas 191us
	;lda #41	;meas 195us
	;lda #42	;meas 200us
	pla ;PASSED ARG
	bpl @short_delay
		eor #$80 ;clear the bit7 passed arg for using delay_Xms
		;check if write while waiting for erase delay

		;delay for below Xms
		tax

;While this worked, I later learned how precharge is supposed to work, so added that step back in
;and no longer want to guess at how I might be allowed to write to flash to save time..
;		;if value of $80 was sent (msec with no amount of time, then detect as special
;		;case where want to erase & write to page buffer
;		bne @delay_only
;
;			;clear page latches, don't think actually need this if writting the full page
;			;can't do it now as we can't modify MODE while erasing.  
;			;Needs to be done before hand if going to clear page latches!
;
;			;done on entry
;			;mva_ptr ptr0, UFLASH_PAGE_BUFF ;source
;			mva_ptr ptr1, UFLASH_PGL_BASE ;dest
;
;			;page isn't selected here, it's selected during erase/write sequences
;			;ldx #$00 ;flash page number
;			;stx UFLASH_XPAGE
;
;			;jsr write_page_latches
;			ldy #0 ;func arg: Y=0: 256 bytes to copy from ptr0->ptr1
;			jsr ram_memcpy_max256
;
;			;mem copy takes ~2.227msec
;
;			ldx #UFLASH_TPEE-3-$80 ;subtract 3msec
;			jsr ram_delay_Xms
;
;			lda #149	;add ~0.773msec delay to align
;			bane @short_delay
;
;		@delay_only:

		jsr ram_delay_Xms
		;jsr -> lda = 10,745 cycles before modification to include writes
		;jsr above to mva below = 10,914 cycles
		;add in copy, now= 14,901 cycles.  Need to remove 3987cycles = 2.227msec
		;3ms + 50 below = 9926 cycles
		;3ms + 173 below = 10,910 cycles, seems good match!
		lda #1	;0->256
		;sta reg0 ;flow through to below for any fine tuning..
	@short_delay:
	sta reg0
	jsr ram_delay_reg0

	mva_uflash_clk_pe_clr

;TSP3 = 60+usec

;meas: 6.7usec, add delay
	;reg0=22 for every 100usec
	lda #15	;meas 84.9s, prob good
	sta reg0
	jsr ram_delay_reg0

;SEQ3
	lda #UFLASH_SEQ3
	sta UFLASH_SEQ

	;save time below
	ldx #UFLASH_SEQ0
	ldy #UFLASH_CLKCTL_MAN0

	mva_uflash_clk_hi ;clock the mode change
	;mva_uflash_clk_lo
	sty UFLASH_CLKCTL

;TS3 = 5-10usec

;meas: 10.1us, need to shorten... use X register for SEQ
;meas: 8.94us, within spec, cut further with use of STY
;meas: 6.71us using sty, stx, sta, add nop
	nop
	
;SEQ0
	;lda #UFLASH_SEQ0
	stx UFLASH_SEQ

	;mva_uflash_clk_hi ;clock the mode change
	sta UFLASH_CLKCTL
	mva_uflash_clk_lo

;TS0 = 6+usec

;meas: 10.1us, should be good

	lda #0
	sta UFLASH_MODE
	sta UFLASH_SEQ
	sta UFLASH_CLKCTL
	
rts
.endproc ;run_sequence


.proc clear_page_latches
	;#1 clear page latches
	;Clear Page Latch
	;Unlike page latch write, page latch clear is controlled by ACLK. When
	;MODE is set as "0100", the user flash enters into clear page latch mode at
	;rising edge of ACLK. In this mode, ACLK ??SEQ?? [1: 0] should be "00" and page
	;latch data will be cleared in one ACLK cycle.

	;we can't make single cycle aclk write without switching to manual clock mode currently..
	mva_uflash_clk_lo ;stop the clk
	lda #UFLASH_MODE_CLRPGL
	sta UFLASH_MODE
	mva_uflash_clk_hi ;clock the mode change
	;mva_uflash_clk_lo ;not needed, free running below will clear
	lda #UFLASH_MODE_READ ;back to read mode
	sta UFLASH_MODE
	mva_uflash_clk_osc ;free running clk

	;just do it a ton of times without individual clocking
	;fpga clock is very fast so it'll get clocked repeatedly.
	;only potential problem with this is M2 isn't sync'd to the FPGA clock
	;so could clock an undefined value on a transition....
	;lda #UFLASH_MODE_CLRPGL
	;sta UFLASH_MODE
	;lda #UFLASH_MODE_READ ;back to read mode
	;sta UFLASH_MODE
rts
.endproc ;clear_page_latches

;X = new mode
;Y = exit mode TODO???
.proc change_mode
	;#1 clear page latches
	;Clear Page Latch
	;Unlike page latch write, page latch clear is controlled by ACLK. When
	;MODE is set as "0100", the user flash enters into clear page latch mode at
	;rising edge of ACLK. In this mode, ACLK ??SEQ?? [1: 0] should be "00" and page
	;latch data will be cleared in one ACLK cycle.

	;we can't make single cycle aclk write without switching to manual clock mode currently..
	mva_uflash_clk_lo ;stop the clk
	;lda #UFLASH_MODE_CLRPGL
	;sta UFLASH_MODE
	stx UFLASH_MODE
	mva_uflash_clk_hi ;clock the mode change
	;mva_uflash_clk_lo ;not needed, free running below will clear
	;lda #UFLASH_MODE_READ ;back to read mode
	sty UFLASH_MODE
	mva_uflash_clk_osc ;free running clk

	;just do it a ton of times without individual clocking
	;fpga clock is very fast so it'll get clocked repeatedly.
	;only potential problem with this is M2 isn't sync'd to the FPGA clock
	;so could clock an undefined value on a transition....
	;lda #UFLASH_MODE_CLRPGL
	;sta UFLASH_MODE
	;lda #UFLASH_MODE_READ ;back to read mode
	;sta UFLASH_MODE
rts
.endproc ;change_mode


;ptr0 -> 256B of data would like to copy into page buffer
;A = page number of flash to write to
.proc write_page_latches
;#2 write data into the page latch
;Page latch can be regarded as one SRAM that will be wrote into Flash.
;Page latch write-in operation is controlled by PW signal, independent of
;ACLK. PA (Page Address) signal specifies the address to write to the page
;latches.
;You should clear page latches before writing. Write Page latches one
;by one. Set MODE value as "0000" and MODE [1: 0] as "00". Page latch
;write and data read operation are completely independent.

	;set the 256B Xpage page latch
	;don't think this matters yet, but may as well set it now
	;currently logic doesn't change ra unless mode is non-zero
;	lda #$00 ;TODO input arg
;	sta UFLASH_XPAGE

	;write 256B to page latch 
	;for now, simply write 0->0 1->1, 2->2, etc...
	ldx #$00
	@wr_next_byte:
		txa
		clc
		rol
		rol
		rol
		rol
		rol

		;page latches $4100-41FF
		sta UFLASH_PGL_BASE, X

		;;page latch registers HW option
		;stx UFLASH_ADDR	;addr first
		;sta UFLASH_DATA	;data, this write clocks the page latch for current address

		;;manual PW clocking HW option
		;mva_uflash_pw_clk

		inx
		bne @wr_next_byte

rts
.endproc ;write_page_latches

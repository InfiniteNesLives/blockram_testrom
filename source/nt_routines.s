;Name table routines
;this code needs access to NT data which is spread accross multiple banks
;so this code needs to be copied into each of those banks..


load_nt_row:
	;first byte of data
	ldx #1
	ldy #0

	;load 32 bytes of data into nmt_update [2-33]
	@load_nt_row:
		lda (nat_ptr), y	  ;load nt data byte
		iny
		inx
		sta nmt_update, x ;store nt byte in buffer
		cpx #33
		bne @load_nt_row

rts ;load_nt_row

;input arg andmask - reg0
load_at_down:
@andmask = reg0
	;first byte of data
	ldx #1
	ldy #0

	;OR in 8 bytes of data into nmt_update [2-9]
	@load_at_down:
		lda (at_ptr), y	  ;load at data byte
		;mask out nibble we're not updating
		and @andmask
		iny
		inx
		ora nmt_update, x ;OR in the other nibble
		sta nmt_update, x ;store nt byte in buffer
		cpx #9
		bne @load_at_down
rts; load_at_down

;prereq nat_ptr is set
;	scroll_cnt == 0 on first call used to trigger init
;	nmt_update [0,1] contains starting PPU address for updates
;		currently this must also be row4 col0 of a NT
;	it also expects both nat_ptr and nmt_update PPUaddr to
;	be pre-dec by 56, TODO update init to change this..?
;
;return Zflag == 0 if done
;NT_BANK this routine is accessing NT_BANK data
;does not jsr to other routines
draw_pf_vblank:
	;want to call set_nat_ptr if scroll_dir is not yet a counter
	;ie we only call it the first time
	lda scroll_cnt	;will init to 0, and dec to 0 last time
	bne @fill_buffer
		;jsr set_nat_ptr_PPUaddr

		;TODO pre-dec any pointers as desired so the calling
		;function doesn't have to do so itself

		lda #16	;16-1=16 total number of frames for full play field
		sta scroll_cnt  ;use as frame counter

		;initialize scroll counter for vertical scrolls
		;208 total pixels scrolled
		
	@fill_buffer:

	;increment nat_ptr and buffer address
	lda nat_ptr
	clc
	adc #56
	sta nat_ptr
	bcc :+
		inc nat_ptr+1
	:

	;increment the first two bytes of buffer which
	;is written as PPU address
	lda nmt_update+1	;nt address low byte
	clc
	adc #56
	sta nmt_update+1
	bcc :+
		inc nmt_update	;nt address high byte
	:
	
	;choose between fast auto inc updates or random addr/data
	lda #FAST_HORZ 	;14 full * 56 = 784B  scroll_cnt 16-3
	sta nmt_update_len

	;if last NT update 
	;only update 50Bytes (nmt_update_len)
	;but nat_ptr was incremented by 56Byte (over by 8bytes)
	lda scroll_cnt
	cmp #2
	bne :+
	lda #LAST_HORZ_NT
	sta nmt_update_len
	:

	lda scroll_cnt
	cmp #1
	bne @nt_update 
	;nmt_ptr is 8B over extended
	;PPU ADDR pointer (first 2B of buffer) was incremented
	;by 8Bytes.  Which is same value need to skip HUD
	;for left/write scrolls

	;update to AT address
	;128B + 784B + 56B = $23C8/$27C8
	;want $23C8/$27C8 so the PPU address is perfect because we under
	;wrote by 8Bytes

	;handle upward scrolling case where started drawing in top row
	;if PPU addr = $2xC8 it's normal case, leave it,
	;else it's special case, set to C0
	lda nmt_update+1 ;LSByte
	cmp #$C8
	beq @std_case
		;special case of where started drawing top row of NT
		lda #$C0
		sta nmt_update+1

	@std_case:
	;But the file pointer is always 8Bytes too high
	lda nat_ptr	;nt address low byte
	sec
	sbc #8
	sta nat_ptr
	bcs :+
		dec nat_ptr+1	;nt address high byte
	:

	;store starting address
	;ldx #0
	;lda #$20 ;nametable 0
	;sta nmt_update, x
	;inx
	;lda #$80 ;start of row 4 (5th row)
	;sta nmt_update, x

	@nt_update:
	;determine tile count of the playfield, for generating collision map data
	;$2080 or $2480 is top left tile => tile 0
	;upper byte shouldn't matter, just lower
	lda nmt_update+1
	sub #$80 ;offset for HUD
	sta reg0 ;contains tile count

	;first byte of data
	ldx #1
	ldy #0

	;TODO load 56 bytes of data into nmt_update [2-57]
	@load_56Bytes:

		;if > 207 don't generate map data
		lda map_tile_cnt
		cmp #NO_GEN
		beq @no_map_gen

		;if even row & col, generate data
		;lda reg0 ;tile count
		;and #$20 ;even row
		;bne @no_map_gen
		;lda reg0 ;tile count
		;and #$01 ;even col

		;this code is same as above but uses bit instruction
		;just as efficient as above, but bit as benefit of copying b7-6 ->NVflags
		lda #$20
		bit reg0 ;tile count
		bne @no_map_gen
		lda #$01
		bit reg0 ;tile count

		bne @no_map_gen
			;we're accessing even row & col -> top left tile
			;preserve X
			txa
			pha
			
			lda map_tile_cnt 
			tax ;X

			lda (nat_ptr), y	  ;load nt data byte

			;for testing just write the tile number
			;sta collision_map, x

			;make range check
			cmp #OUCH_ROW
			bcc @check_block	
			;don't check ladder since they're non-blocking

				;hit pokey thing
				lda #OUCH_BIT+BLOCK_BIT
				bne @store_a

			@check_block:
			cmp #BLOCK_ROW
			bcc @check_object 
			;don't bother checking ouch as they're also block

				;blocked by obstacle
				lda #BLOCK_BIT
				bne @store_a

			@check_object:
			cmp #UP_OBJ_LAST
			bcs @clear_byte 

				;an interactable object with up button
				;A still contains tile number
				;is this the special tile zero?
				cmp #0
				bne @not_tile_zero
					lda #OBJECT_BIT+TILE_ZERO
					bne @store_a

				@not_tile_zero:
				lda #OBJECT_BIT
				bne @store_a
			

			@clear_byte:
			;clear the byte since nothing was found
			lda #0

			;store map data
			@store_a:
			sta collision_map, x

			inc map_tile_cnt
			;restore X
			pla
			tax

		@no_map_gen:
		lda (nat_ptr), y	  ;load nt data byte

		inc reg0 ;increment tile count
		iny
		inx
		sta nmt_update, x ;store nt byte in buffer
		cpx #57
		bne @load_56Bytes

	;count frame number
	dec scroll_cnt
	;return Zflag == 0 if done

	;can't use Z flag due to trampoline
	;use A instead
;	bne @end
;		
;	
;	@end:
	rts 
;draw_pf_vblank:



;A contains MSByte of Nametable address
;X/Y number of bytes.?
;HUD = 128NTB 8ATB = 136 total = 8 * 17
;playfield = 888B total, 832 NTB, 56 ATB = 8 * 111

;hard coded default NT0 & first row
write_hud:

	;select first CHR bank
	;this is done during rendering
	;lda #0
	;jsr set_chr_bank

	; first nametable, start by clearing to empty
	ldx $2002 ; reset latch
	lda #$20 ; default HUD address MSB
	sta $2006
	lda #$00
	sta $2006
	; empty nametable
	lda #0
;	sta reg0
	lda #>hud_nat	;hi byte
	sta nat_ptr+1
	lda #<hud_nat	;lo byte
	sta nat_ptr
	;ldx #30 ; 30 rows
	;ldx #32 ; 30 rows + AT data
	ldx #4 ; 4 rows 
	ldy #0 ;nt byte
	jmp @next_in_row
	
	@start_row:

		;inc nat_ptr to next row
		lda #32
		clc
		adc nat_ptr
		sta nat_ptr
		bcc @clear_col_count
		inc nat_ptr+1

		@clear_col_count:
		ldy #0 ; reset to first in row

		dex	;counting rows + AT
		bne @next_in_row
		
		;done
		;rts
		jmp @load_at

		@next_in_row:
			lda (nat_ptr), y
			;lda #AA
			sta $2007
			iny
			tya
			cmp #32
			bne @next_in_row

			jmp @start_row
	
	@load_at:

	; first nametable, start by clearing to empty
	ldx $2002 ; reset latch
	lda #$23 ; default HUD address MSB
	sta $2006
	lda #$C0  ;first AT byte of screen
	sta $2006

	;ldx #30 ; 30 rows
	;ldx #32 ; 30 rows + AT data
	ldx #1 ;  row 
	ldy #0 ;at byte
	jmp @next_in_at_row

	@start_at_row:

		;inc nat_ptr to next row
		lda #8
		clc
		adc nat_ptr
		sta nat_ptr
		bcc @clear_at_col_count
		inc nat_ptr+1

		@clear_at_col_count:
		ldy #0 ; reset to first in row

		dex	;counting rows + AT
		bne @next_in_at_row
		
		;done
		rts

		@next_in_at_row:
			lda (nat_ptr), y
			;lda #AA
			sta $2007
			iny
			tya
			cmp #8
			bne @next_in_at_row

			jmp @start_at_row
	
	;rts done up there ^
;write_hud:


;reg0 contain $20/$24 upper address byte of nametable address
;TODO don't use this function
write_playfield:

	; first nametable, start by clearing to empty
	ldx $2002 ; reset latch
	;lda #$20 ; default HUD address MSB
	lda reg0
	sta $2006
	lda #128  ;skip over first 4 rows of HUD
	sta $2006
	; empty nametable
	lda #0
	lda #>scn0_nt	;hi byte
	sta nat_ptr+1
	lda #<scn0_nt	;lo byte
	sta nat_ptr
	ldx #26 ; 4 rows 
	ldy #0 ;nt byte
	jmp @next_in_row
	
	@start_row:

		;inc nat_ptr to next row
		lda #32
		clc
		adc nat_ptr
		sta nat_ptr
		bcc @clear_col_count
		inc nat_ptr+1

		@clear_col_count:
		ldy #0 ; reset to first in row

		dex	;counting rows + AT
		bne @next_in_row
		
		;done
		;rts
		jmp @load_at

		@next_in_row:
			lda (nat_ptr), y
			;lda #AA
			sta $2007
			iny
			tya
			cmp #32
			bne @next_in_row

			jmp @start_row
	
	@load_at:

	; first nametable, start by clearing to empty
	ldx $2002 ; reset latch
	;lda #$20 ; default HUD address MSB
	inc reg0
	inc reg0
	inc reg0 ;increment to start of AT
	lda reg0 ; default HUD address MSB
	
	sta $2006
	lda #$C8  ;first AT byte of second row of AT data
	sta $2006

	;ldx #30 ; 30 rows
	;ldx #32 ; 30 rows + AT data
	ldx #7 ;rows
	ldy #0 ;at byte
	jmp @next_in_at_row

	@start_at_row:

		;inc nat_ptr to next row
		lda #8
		clc
		adc nat_ptr
		sta nat_ptr
		bcc @clear_at_col_count
		inc nat_ptr+1

		@clear_at_col_count:
		ldy #0 ; reset to first in row

		dex	;counting rows + AT
		bne @next_in_at_row
		
		;done
		rts

		@next_in_at_row:
			lda (nat_ptr), y
			;lda #AA
			sta $2007
			iny
			tya
			cmp #8
			bne @next_in_at_row

			jmp @start_at_row
	

	;rts done up there ^
;write_playfield:





;
; gamepad
;

;ZP gamepad
PAD_A      = $01
PAD_B      = $02
PAD_SELECT = $04
PAD_START  = $08
PAD_U      = $10
PAD_D      = $20
PAD_L      = $40
PAD_R      = $80
PAD_DIRS   = PAD_U+PAD_D+PAD_L+PAD_R


; gamepad_poll: this reads the gamepad state into the variable labelled "gamepad"
;   This only reads the first gamepad, and also if DPCM samples are played they can
;   conflict with gamepad reading, which may give incorrect results.
;input Y = 0 gamepad1, 1 gamepad2
;return A returned gamepad data
gamepad_poll:
;input arg: Y = 0/1 gamepad 1/2

	; strobe the gamepad to latch current button state
	lda #1
	sta $4016
	lda #0
	sta $4016
	; read 8 bytes from the interface at $4016
	ldx #8
	:
		pha
		lda $4016, y	;y= 0-pad1 1-pad2
		; combine low two bits and store in carry bit
		and #%00000011
		cmp #%00000001
		pla
		; rotate carry into gamepad variable
		ror
		dex
		bne :-

	;return:
	;sta gamepad
rts



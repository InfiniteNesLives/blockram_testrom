
checksum_inst_str:
.asciiz "set range:"
;pciccom_line1_str:
;.asciiz "B:   U:   1:   2:   3:"
;
;pciccom_line2_str:
;.asciiz "game"
;pciccom_line3_str:
;.asciiz "esp B0 B1 P0 P1"

checksum_str1:
.asciiz "$xxxx Start"
checksum_str2:
.asciiz "$xxxx Last"
checksum_str3:
.asciiz "num bytes: $"
NUM_BYTES_POS = LEFT_MARGIN+12
checksum_str4:
.asciiz "sum: $"
SUM_POS = LEFT_MARGIN+6

SPTR0_ROW = LINE1
SPTR1_ROW = SPTR0_ROW+1
SPTR_ADDR_HI = LEFT_MARGIN+1;5
SPTR_ADDR_LO = LEFT_MARGIN+3;7

.proc checksum_test_init

	mva_ptr ptr0, checksum_inst_str
	ldx #LEFT_MARGIN
	ldy #INST_ROW
	jsr ppu_write_string

	mva_ptr ptr0, checksum_str1
	ldx #LEFT_MARGIN
	ldy #SPTR0_ROW
	jsr ppu_write_string

	jsr ppu_update

	mva_ptr ptr0, checksum_str2
	ldx #LEFT_MARGIN
	ldy #SPTR1_ROW
	jsr ppu_write_string

	mva_ptr ptr0, checksum_str3
	ldx #LEFT_MARGIN
	ldy #LINE3
	jsr ppu_write_string

	mva_ptr ptr0, checksum_str4
	ldx #LEFT_MARGIN
	ldy #LINE4
	jsr ppu_write_string

	jsr ppu_update

	;move cursor to start address line
	lda #LINE1
	sta cursor_y
	lda #LEFT_MARGIN+1
	sta cursor_x

	;convience assume end ptr is $xFFF
	lda #$FF
	sta aptr1_lo
	sta aptr1_hi

	;switch function pointer to run test
	mva_ptr test_fptr, checksum_run

rts
.endproc ;checksum_test_init

.proc checksum_run

	;if cursor is on pointer rows, check for updates/run
	lda cursor_y
	cmp #SPTR0_ROW
	bpl :+
		rts
	:
	cmp #SPTR1_ROW+1
	bmi :+
		rts
	:

	;SELECT PEEK
	lda gamepad_new
	and #PAD_START
	beq :+
		jsr calculate_sum
	:
;	;SELECT POKE
;	lda gamepad_new
;	and #PAD_SELECT
;	beq :+
;		jsr poke_row
;	:

	;check for cursor input
	lda cursor_arg
	bne :+
		rts
	:

	pha ;cursor arg value (amount to change the value under the cursor by)

	;clear so don't come back
	lda #0
	sta cursor_arg

	;input was given, check row
	lda cursor_y

	cmp #SPTR0_ROW
	bne :+
		ldx #0 ;table index
		baeq @check_col
	:
	cmp #SPTR1_ROW
	bne :+
		ldx #2 ;table index
		bane @check_col
	:

	;didn't match row
	pla
	rts

	@check_col:
	lda cursor_x
	cmp #SPTR_ADDR_HI
	bne :+
		pla;lda cursor_arg
		mul a, #16
		add aptr_table_hi, x
		sta aptr_table_hi, x
		jmp @done
	:
	cmp #SPTR_ADDR_HI+1
	bne :+
		pla;lda cursor_arg
		add aptr_table_hi, x
		sta aptr_table_hi, x
		jmp @done
	:
	cmp #SPTR_ADDR_LO
	bne :+
		pla;lda cursor_arg
		mul a, #16
		add aptr_table_lo, x
		sta aptr_table_lo, x
		jmp @done
	:
	cmp #SPTR_ADDR_LO+1
	bne :+
		pla;lda cursor_arg
		add aptr_table_lo, x
		sta aptr_table_lo, x
		jmp @done
	:

	;no col match
	pla
	rts

	@done:
	jsr display_aptr_addr

rts
.endproc ;checksum_run


checksum_running:
.asciiz "running."

.proc calculate_sum
@num_bytes_lo = reg4
@num_bytes_hi = reg5
@sum_LSB = reg6;-reg9 4Bytes

	;calculate number of bytes to check
	lda aptr1_lo
	sub aptr0_lo
	sta @num_bytes_lo
	lda aptr1_hi
	sbc aptr0_hi
	sta @num_bytes_hi

	;need to count byte 0
	inc @num_bytes_lo
	bne :+
		inc @num_bytes_hi
	:

	;don't want to corrupt aptr0 for the calculation, so copy to temp ptr & use that
	lda aptr0_lo
	sta ptr1_lo
	lda aptr0_hi
	sta ptr1_hi

	;display number of bytes:
	lda @num_bytes_hi
	ldy #LINE3
	ldx #NUM_BYTES_POS
	jsr display_hex_byte

	lda @num_bytes_lo
	ldy #LINE3
	ldx #NUM_BYTES_POS+2
	jsr display_hex_byte

	;clear sum
	lda #0
	sta @sum_LSB
	sta @sum_LSB+1
	sta @sum_LSB+2
	sta @sum_LSB+3

	;display "running"
	mva_ptr ptr0, checksum_running
	ldx #SUM_POS
	ldy #LINE4
	jsr ppu_write_string

	jsr ppu_update

	;calculate checksum from aptr0-aptr1
	ldy #0
	@next_sum:

		;add next byte to sum
		lda @sum_LSB
		add (ptr1), Y
		sta @sum_LSB
		bcc @no_carry
			;add carry to next significant byte
		;	lda @sum_LSB+1
		;	adc #0
		;	sta @sum_LSB+1
		;	bcc @no_carry

		;	lda @sum_LSB+2
		;	adc #0
		;	sta @sum_LSB+3
		;	bcc @no_carry

		;	lda @sum_LSB+3
		;	adc #0
		;	sta @sum_LSB+3

			inc @sum_LSB+1
			bne @no_carry
			inc @sum_LSB+2
			bne @no_carry
			inc @sum_LSB+3
		@no_carry:
		
		;update Y/ptr
		iny
		bne :+
			inc ptr1_hi
		:

		;update counter, check if done
		lda @num_bytes_lo
		sub #1
		sta @num_bytes_lo
		bcs :+
			dec @num_bytes_hi
		:
		lda @num_bytes_lo
		bne @next_sum
		lda @num_bytes_hi
		bne @next_sum

	;done, display sum
	lda @sum_LSB+3
	ldy #LINE4
	ldx #SUM_POS+0
	jsr display_hex_byte

	lda @sum_LSB+2
	ldy #LINE4
	ldx #SUM_POS+2
	jsr display_hex_byte

	lda @sum_LSB+1
	ldy #LINE4
	ldx #SUM_POS+4
	jsr display_hex_byte

	lda @sum_LSB+0
	ldy #LINE4
	ldx #SUM_POS+6
	jsr display_hex_byte


rts
.endproc ;calculate_sum

;;about 8CPU cycles per count of reg0
;;max=255 -> 2051 cycles from jsr delay_reg0 -> instruction that follows from calling routine
;;255*8+15 jsr here, and rts back
;;about 4.47usec per value of reg0, max 255 = ~1.146msec
;delay_ms:
;	lda	#222 ;1792 cycles total = 1.001msec on NTSC
;	sta	reg0
;delay_reg0:
;	dec	reg0
;	bne	delay_reg0
;rts;delay
;
;;A= ciccom reply byte
;pciccom_open_comms:
;	;open comms & read unlock reply
;	lda #'I'
;	sta PCICCOM_R0
;	jsr delay_ms
;
;	lda #'N'
;	sta PCICCOM_R1
;	jsr delay_ms
;
;	lda #'L'
;	sta PCICCOM_R2
;	jsr delay_ms
;
;	lda PCICCOM_R0
;rts;pciccom_open_comms
;
;
;;A=
;;pciccom_cmd:
;;rts;pciccom_cmd
;
;lock_mapper:
;;if (prg_rw==0 & (prg_bank_lo==8'hC5) & (chr_bank_lo==8'hA3) & (map_mode==0) & (cpu_A[14]==0) & (romsel_n==0) & (m2_eff==1))
;;                map_lock = 1;
;	
;	lda #0	;chr_bank_lo
;	sta $C000
;	lda #$A3
;	sta $E000
;
;	lda #2	;prg_bank_lo
;	sta $C000
;	lda #$C5
;	sta $E000
;
;	;;write to $8000 to perform mapper switch
;	;;this corrupts vectors so cart will crash @ NMI..
;	;sta $8000
;
;	;;jump to reset vector
;	;jmp ($FFFC)
;
;	;copy STA-JMP to SRAM then jump there
;	;Have to do this because even if we're executing from $6000 where next mapper is going to place
;	;block ram, the RAM may be disabled by default/boot and we'll crash as soon as write to $8000
;	;and won't be able to execute the jmp (), so have to execute STA-JMP from system RAM which
;	;can't help but be visible at all times.
;	;0000        STA $8000       8D 00 80
;	;0000        STA $8D8D       8D 8D 8D just as good
;	;0003        JMP ($FFFC)     6C FC FF
;
;	lda #$8D
;	sta 0
;	;lda #$80
;	sta 1
;	;lda #$00
;	sta 2
;	lda #$6C
;	sta 3
;	lda #$FC
;	sta 4
;	lda #$FF
;	sta 5
;
;	jmp $0000
;
;;no returning...
;;rts ;lock_mapper: 
;
;pciccom_run:
;
;	lda gamepad_new
;	and #PAD_START
;	beq @no_start
;		lda cursor_y
;		cmp #LINE2
;		bne:+
;
;			baeq	lock_mapper
;		:
;		cmp #LINE3
;		bne @not_test2
;			lda cursor_x
;			;.asciiz "esp B0 B1 P0 P1"
;			;         0123456789ABCDE
;			cmp #LEFT_MARGIN+4
;			bne:+
;				ldy	#SPI_ESPBL_PIN
;				ldx	#0
;				baeq	@set_pin
;			:
;			cmp #LEFT_MARGIN+7
;			bne:+
;				ldy	#SPI_ESPBL_PIN
;				ldx	#1
;				bane	@set_pin
;			:
;			cmp #LEFT_MARGIN+10
;			bne:+
;				ldy	#SPI_ESPPD_PIN
;				ldx	#0
;				baeq	@set_pin
;			:
;			cmp #LEFT_MARGIN+13
;			bne:+
;				ldy	#SPI_ESPPD_PIN
;				ldx	#1
;				bane	@set_pin
;			:
;
;			;got to this point, pressed start but not in proper location
;			rts
;
;			@set_pin:
;
;			;Y=pin, X=level
;
;			sty PCICCOM_R1	;pin number
;			jsr delay_ms
;			lda #0
;			sta PCICCOM_R2	;direction
;			jsr delay_ms
;			stx PCICCOM_R3	;level/value
;			jsr delay_ms
;
;			lda #SPI_CMD_GPIO
;			sta PCICCOM_R0	;command
;			jsr delay_ms
;
;			lda PCICCOM_R0
;			ldx #LEFT_MARGIN+17
;			ldy #LINE1
;			jsr display_hex_byte
;
;		@not_test2:
;
;
;	@no_start:
;
;rts;pciccom_run:
;

FamiTone audio library v1.24 by Shiru 06'11



About

FamiTone is an open source audio library for NES/Famicom. It is designed to provide easy way of adding music and sound effects into homebrew games and demos. It is not meant to be the best, the fastest, or the most powerful thing, it is pretty simple but does the job.

The library allows to create both music and sound effects using popular music cross editor FamiTracker and export them into the library's internal formats. It supports all the standart channels of 2A03: two pulse, triangle, noise, and DPCM. Expansion sound chips are not supported.

The library is open source and released into the Public Domain. You can use it for any purposes without restrictions: redistribute, modify, use for commercial and non-commercial projects. You could provide credits for the author, inform the author about your project, donate, etc, but nothing is obligatory, you could do it at free will. Obviously, the library provided AS IS without any warranties, the author is not responsible for anything.



How to create music

You have to create music in FamiTracker version 0.3.6 or higher, or with Pornotracker 1.3 (now Musetracker) and higher, following these limitations:

- Notes C-1..B-5 and Note Cut
- Only Volume, Arpeggio and Pitch envelopes (no Pitch for noise channel), loops are supported
- Pitch envelope accumulate the offset only at conversion time, thus it can't go further -64..63 and can't accumulate the offset with loop
- Only 32 instruments
- Only first step of Duty envelope for pulse and noise channels
- No volume column
- No effects except Fxx, tempo is not supported (fixed at 150)
- Bxx for looping without an intro (does not support forward references), at conversion time
- D00 for shorter patterns,at conversion time
- Minimal speed is 2 if you need PAL compatibility, speed 1 works in NTSC only

You can use DPCM, there are limitations too:

- You can use up to 12 samples with total size of ~16K
- Only samples of instrument 0 are supported, and only default octave assigments (C-3..B-3)
- Instrument number and octave are ignored on the DPCM channel

After you have made a song, you need to save it in text format supported by FamiTone converter.

In FamiTracker you need to use TextExporter.dll plugin. Put it into /plugins/ directory of FamiTracker. If there is no such directory, create it. Now load your song and go to File, Create NSF. In Type of file combo box select Text Exporter item, then click Export button, and save text file.

In Pornotracker you can just save it in needed format with LCtrl+LShift+S (by default).

Now you can use text2data tool to convert text into data. If you don't use DPCM, you'll get just one *.asm file. If you use DPCM, there will be two additional files, *.asm with sample parameters and *.bin with sample data. If you want to use few songs, convert them one by one, because multi song feature is not supported. You can reuse DPCM data between them, if the samples and their note assigments are the same for all the songs. If you need to save some memory, you can create a multisong FTM, export songs one by one (Export works with currently selected song), convert text files, and then get instruments data and music data into separate files. Use -insonly switch to get instruments data only, and -noins to get music data only.

text2data outputs data in NESASM format by default, you can use -ca65 or -asm6 switches after the filename to get output for these assemblers.



How to create sound effects

You have to create sound effects in FamiTracker or Pornotracker too. Every effect can use all the channels except DPCM, so the effects are multichannel. Put every effect into a separate song using multisong feature (check main menu, Module/Module Properties). You can use most of FamiTracker features for effects, except for sweep. Every effect should be finished with silence, and with effect C00 on last row of the effect. After you have made all the effects, export them as NSF and use nsf2data converter. You'll get *.asm file with all the effects.

There is major limitation: one effect can't be larger than 255 bytes after conversion. FamiTracker effects like slides could increase the size considerably, so avoid to overuse them. The converter will report size of every effect and warn you if an effect is too large.

You can play any sample of current DPCM bank, either used or not used in the music, as a sound effect too. It will have priority over samples in music, through stopping other samples while effect sample plays.



How to use the library

The main version of the FamiTone is for NESASM. There are versions for CA65 and ASM6 assemblers included in the archive as well, they are automatically converted from the main version using a custom tool, nesasmc, also included into the archive.

First, include famitone.asm (famitone.s for CA65, famitone_asm6.asm for ASM6) into your project. Next, include *.asm files of your music and sounds. You can place music and sound effects data anywhere, including bankswitched memory, they only needed to be switched into the CPU address space before FamiTone routines calls. DPCM samples (*.bin file) is special case, you can't put them in arbitrary place. They should be placed at $c000 or above with steps of 64 bytes, and they always should be in CPU address space (otherwise samples will not work).

Now you need to setup the FamiTone. Open famitone.asm, there are few defines in the beginning of the file. You can change RAM locations, DPCM location, and number of sound effects played at once.

There are all the user calls of the library:

 FamiToneInit
  A is 0 for PAL, not 0 for NTSC (PAL mode uses speed compensation). Call it
  once before all the other calls

 FamiToneSampleInit
  X,Y are 16-bit address of DPCM sample table, one that exported from text2data
  as *_dpcm.asm. Call it after FamiToneInit every time you need to change table

 FamiToneSfxInit
  X,Y are 16-bit address of sound effects data, one that exported from nsf2data
  Call it after FamiToneInit every time you need to change sound effects set

 FamiToneUpdate
  No parameters, call it every TV frame. It updates everything and writes to
  APU registers

 FamiToneSfxStart
  A is sound effect number, X is FT_SFX_CH0..FT_SFX_CH3 (no other values are
  allowed!). The number in X is sound effect stream. It works like priority,
  there is no automatic priority system in FamiTone (it would be slow). If
  you start a sound effect, it will stop any previous effect with the same
  priority

 FamiToneMusicStart
  X,Y are 16-bit address of music data, one that exported from text2data
  as *.asm

 FamiToneSampleStart
  A is sample number 0..11 (i.e. note C..B with assigned sample), it starts
  to play sample of active DPCM bank, interrupting any other sample

 FamiToneMusicStop
  No parameters, stops any music

 FamiToneMusicPause
  A is 0 or not 0, to play or pause music that currently in playing

Warning: don't forget that active DMC conflicts with $2002 and controllers polling, read docs to learn how to avoid it (generally don't use $2002 polling and poll controllers three times in a row, then use matching result). Additionally, you can add unused samples in the set, and then use it as sound effects.

You can expect peak CPU time with music and few sound effects playing is about 15%. You can reduce it through code optimization (it is not the most optimal), unrolling loops, reducing number of sound effects played at once.

You can exclude sound effects and DPCM support from code through defines FT_SFX_ENABLE and FT_DPCM_ENABLE (not define them to disable the functionality). Be aware that speed change commands placed on the same channel they placed in the editor, so if you put them to DPCM channel and disable DPCM support, they will be ignored by player.

If you going to put sound update into an interrupt (NMI or other), and call sound effects from the main code, make sure that FT_THREAD is defined.



Thanks

jsr (FamiTracker), Gradualore (plugin system for FamiTracker), Memblers (library's name), kevtris, Gil, B00daW, ManicGenius, thefox, UncleSporky, kulor, NesDev community.



List of products using FamiTone

Lan Master
Lawn Mower
Melanchony Of Existance:Chapter 0
Alter Ego
Zooming Secretary
Chase
MineShaft
Sir Ababol



Contacts

mailto:shiru@mail.ru
http://shiru.untergrund.net/
//FamiTracker Text Exporter plugin v1.26
//by Shiru (shiru@mail.ru) 05'11
//with help and base code by Gradualore

#include "TextExporter.h"

#include <fstream>
#include <vector>
#include <string>
#include <iomanip>
#include <windows.h>

using namespace std;



char effect(int num)
{
	switch(num)
	{
	case 0x10: return '1';
	case 0x11: return '2';
	case 0x06: return '3';
	case 0x0b: return '4';
	case 0x0c: return '7';
	case 0x16: return 'A';
	case 0x02: return 'B';
	case 0x04: return 'C';
	case 0x03: return 'D';
	case 0x05: return 'E';
	case 0x01: return 'F';
	case 0x0e: return 'G';
	case 0x08: return 'H';
	case 0x09: return 'I';
	case 0x0d: return 'P';
	case 0x14: return 'Q';
	case 0x15: return 'R';
	case 0x17: return 'S';
	case 0x12: return 'V';
	case 0x18: return 'X';
	case 0x13: return 'Y';
	case 0x0f: return 'Z';
	}

	return '.';
}



void sequence(std::ofstream& os,FamitrackerDocInterface const* doc,int type,const char *name)
{
	unsigned int i,j;
	SequenceHandle sequence;

	os<<"[Sequences"<<name<<"]"<<endl;

	if(!doc->GetSequenceCount(type))
	{
		os<<endl;
		return;
	}

	for(i=0;i<128;i++)
	{
		sequence=doc->GetSequence(i,type);

		if(sequence)
		{
			os<<"Sequence"<<name<<i<<"=";
			for(j=0;j<doc->GetItemCount(sequence);j++)
			{
				if(j==doc->GetLoopPoint(sequence)) os<<"|";
				os<<int(doc->GetItem(sequence,j));
				if(j<doc->GetItemCount(sequence)-1) os<<",";
			}
			os<<endl;
		}
	}

	os<<endl;
}



DLL_EXPORT const char* GetName( void )
{
	return "Text Exporter v1.26";
}

DLL_EXPORT const char* GetExt( void )
{
	return ".txt";
}

DLL_EXPORT bool Export( FamitrackerDocInterface const* doc, const char* fileName)
{
	const char *names[15]={"...","C-","C#","D-","D#","E-","F-","F#","G-","G#","A-","A#","B-","===","---"};
	unsigned int i,j,k,l;
	stChanNote note;
	Instrument2A03Handle ins;
	char name[1024];
	std::ofstream os;
	WCHAR openName[1024];

	MultiByteToWideChar(CP_ACP,0,fileName,strlen(fileName)+1,openName,sizeof(openName)/sizeof(WCHAR));

	os.open(openName,ios::out);

	os<<"[Module]"<<endl;
	os<<"Speed="<<doc->GetSongSpeed()<<endl;
	os<<"FramesCount="<<doc->GetFrameCount()<<endl;
	os<<"PatternLength="<<doc->GetPatternLength()<<endl;
	os<<"InstrumentsCount="<<doc->GetInstrumentCount()<<endl;
	os<<"SamplesCount="<<doc->GetSampleCount()<<endl;
	os<<"SequencesVolumeCount="<<doc->GetSequenceCount(SEQ_VOLUME)<<endl;
	os<<"SequencesArpeggioCount="<<doc->GetSequenceCount(SEQ_ARPEGGIO)<<endl;
	os<<"SequencesPitchCount="<<doc->GetSequenceCount(SEQ_PITCH)<<endl;
	os<<"SequencesHiPitchCount="<<doc->GetSequenceCount(SEQ_HIPITCH)<<endl;
	os<<"SequencesDutyCount="<<doc->GetSequenceCount(SEQ_DUTYCYCLE)<<endl;
	os<<endl;

	for(i=0;i<0x40;i++)
	{
		ins=doc->Get2A03Instrument(i);

		if(ins)
		{
			os<<"[Instrument"<<i<<"]"<<endl;
			if(doc->GetSeqEnable(ins,SEQ_VOLUME))    os<<"SequenceVolume="  <<doc->GetSeqIndex(ins,SEQ_VOLUME)<<endl;
			if(doc->GetSeqEnable(ins,SEQ_ARPEGGIO))  os<<"SequenceArpeggio="<<doc->GetSeqIndex(ins,SEQ_ARPEGGIO)<<endl;
			if(doc->GetSeqEnable(ins,SEQ_PITCH))     os<<"SequencePitch="   <<doc->GetSeqIndex(ins,SEQ_PITCH)<<endl;
			if(doc->GetSeqEnable(ins,SEQ_HIPITCH))   os<<"SequenceHiPitch=" <<doc->GetSeqIndex(ins,SEQ_HIPITCH)<<endl;
			if(doc->GetSeqEnable(ins,SEQ_DUTYCYCLE)) os<<"SequenceDuty="    <<doc->GetSeqIndex(ins,SEQ_DUTYCYCLE)<<endl;
			os<<endl;
		}
	}

	sequence(os,doc,SEQ_VOLUME,"Volume");
	sequence(os,doc,SEQ_ARPEGGIO,"Arpeggio");
	sequence(os,doc,SEQ_PITCH,"Pitch");
	sequence(os,doc,SEQ_HIPITCH,"HiPitch");
	sequence(os,doc,SEQ_DUTYCYCLE,"Duty");

	for(i=0;i<doc->GetFrameCount();i++)
	{
		os<<"[Frame"<<dec<<i<<"]"<<endl;

		for(j=0;j<doc->GetPatternLength();j++)
		{
			os<<hex<<setfill('0')<<setw(2)<<j;
			for(k=0;k<5;k++)
			{
				doc->GetNoteData(i,k,j,&note);
				os<<"|";
				os<<names[note.Note];
				if(note.Note>0&&note.Note<=12) os<<int(note.Octave);
				os<<" ";
				if(note.Instrument<0x40) os<<setw(2)<<int(note.Instrument); else os<<"..";
				os<<" ";
				if(note.Vol<0x10) os<<setw(1)<<int(note.Vol); else os<<".";

				for(l=0;l<1;l++)//l<4 for all the effect columns
				{
					os<<" ";
					os<<effect(note.EffNumber[l]);
					if(note.EffNumber[l]) os<<setw(2)<<int(note.EffParam[l]); else os<<"..";
				}
			}
			os<<endl;
		}
		os<<endl;
	}

	os<<dec;

	for(i=0;i<40;i++)
	{
		ins=doc->Get2A03Instrument(i);

		if(ins)
		{
			os<<"[DMC"<<i<<"]"<<endl;

			os<<"SamplesAssigned=";

			for(j=0;j<8*12;j++)
			{
				os<<(int)doc->GetSample(ins,j/12,j%12);
				if(j<8*12-1) os<<","; else os<<endl;
			}

			os<<"SamplesPitch=";

			for(j=0;j<8*12;j++)
			{
				os<<(int)doc->GetSamplePitch(ins,j/12,j%12);
				if(j<8*12-1) os<<","; else os<<endl;
			}

			os<<"SamplesLoop=";

			for(j=0;j<8*12;j++)
			{
				os<<(int)doc->GetSampleLoopOffset(ins,j/12,j%12);
				if(j<8*12-1) os<<","; else os<<endl;
			}

			os<<endl;
		}
	}

	for(i=0;i<0x40;i++)
	{
		if(doc->GetSampleSize(i)==0) continue;

		doc->GetSampleName(i,name);
		os<<dec;
		os<<"[Sample"<<(i+1)<<"]"<<endl;
		os<<"SampleName="<<name<<endl;
		os<<"SampleSize="<<doc->GetSampleSize(i)<<endl;
		os<<"SampleData=$"<<hex;

		for(j=0;j<(unsigned int)doc->GetSampleSize(i);j++) os<<setfill('0')<<setw(2)<<(int)(unsigned char)doc->GetSampleData(i,j);

		os<<endl<<endl;
	}

	os.close();

	return true;
}

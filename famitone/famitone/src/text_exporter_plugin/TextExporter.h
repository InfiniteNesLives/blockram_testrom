#include <string>
#include <sstream>
#include <CustomExporterInterfaces.h>

#define DLL_EXPORT __declspec(dllexport)

extern "C" DLL_EXPORT const char* GetName( void );

extern "C" DLL_EXPORT const char* GetExt( void );

extern "C" DLL_EXPORT bool Export( FamitrackerDocInterface const* iface, const char* fileName );
    .inesprg    2
    .ineschr    1
    .inesmir    0
    .inesmap    0

    .bank 0
    .org $8000

PPU_CTRL		equ $2000
PPU_MASK		equ $2001
PPU_STATUS		equ $2002
PPU_SCROLL		equ $2005
PPU_ADDR		equ $2006
PPU_DATA		equ $2007
PPU_FRAMECNT	equ $4017
DMC_FREQ		equ $4010
CTRL_PORT1		equ $4016

FRAME_CNT		equ $ff
NTSC_MODE		equ $fe


reset

;init hardware

    sei
    ldx #$40
    stx PPU_FRAMECNT
    ldx #$ff
    txs
    inx
    stx PPU_MASK
    stx DMC_FREQ
	lda #%10000000
    sta PPU_CTRL

	jsr waitVBlank

    txa
clearRAM
    sta $000,x
    sta $100,x
    sta $200,x
    sta $300,x
    sta $400,x
    sta $500,x
    sta $600,x
    sta $700,x
    inx
    bne clearRAM

	jsr waitVBlank
	lda #$00
	sta PPU_SCROLL
	sta PPU_SCROLL

clearVRAM
	lda #$00
	sta PPU_ADDR
	sta PPU_ADDR
	ldx #$00
.1
	ldy #$40
.2
	sta PPU_DATA
	iny
	bne .2
	inx
	bne .1

	lda #0
	sta FRAME_CNT

detectNTSC
	jsr waitNMI		;blargg's code
	ldx #52
	ldy #24
.1
	dex
	bne .1
	dey
	bne .1

	lda PPU_STATUS
	and #$80
	sta NTSC_MODE	;$00 PAL, $80 NTSC

showScreen
	lda #$3f		;set palettes
	sta $2006
	lda #$00
	sta $2006
	ldx #$00
	stx $2007
	lda #$0f
	sta $2007
	lda #$10
	sta $2007
	lda #$30
	sta $2007
	stx $2007
	lda #$0d
	sta $2007
	lda #$10
	sta $2007
	lda #$38
	sta $2007
	stx $2007
	lda #$0d
	sta $2007
	lda #$21
	sta $2007
	lda #$3b
	sta $2007
	stx $2007
	lda #$09
	sta $2007
	lda #$19
	sta $2007
	lda #$29
	sta $2007


	ldx #LOW(screenData)	;unpack nametable and attributes
	ldy #HIGH(screenData)
	lda #$20
	sta PPU_ADDR
	lda #$00
	sta PPU_ADDR
	jsr unrle

	lda #0
	sta PPU_ADDR
	sta PPU_ADDR
	
	jsr waitNMI
	lda #%00011110	;enable display
	sta PPU_MASK

initLoop
	lda NTSC_MODE
	jsr FamiToneInit		;init FamiTone

	ldx #LOW(music_dpcm)	;set DPCM sample table location
	ldy #HIGH(music_dpcm)
	jsr FamiToneSampleInit

	ldx #LOW(sounds)		;set sound effects data location
	ldy #HIGH(sounds)
	jsr FamiToneSfxInit

	jsr padInit				;init gamepad polling code

mainLoop
	jsr waitNMI				;wait for new frame

	ldx #0					;delay to shift FamiTone's time into visible raster
.delay
	nop
	nop
	nop
	nop
	nop
	dex
	bne .delay

	lda #%10011110			;show start of sound code time
	sta PPU_MASK
	sta $401e				;for VirtuaNES custom build

	jsr FamiToneUpdate		;update sound

	sta $401f				;show end of sound code time
	lda #%00011110
	sta PPU_MASK

	jsr padPoll				;poll gamepad

	ldx PAD_STATET
	txa
	and #PAD_LEFT
	beq .1
	lda #0					;play effect 0 on channel 0
	ldx #FT_SFX_CH0
	jsr FamiToneSfxStart
	jmp mainLoop
.1
	txa
	and #PAD_RIGHT
	beq .2
	lda #1					;play effect 1 on channel 1
	ldx #FT_SFX_CH1
	jsr FamiToneSfxStart
	jmp mainLoop
.2
	txa
	and #PAD_UP
	beq .3
	lda #2					;play effect 2 on channel 2
	ldx #FT_SFX_CH2
	jsr FamiToneSfxStart
	jmp mainLoop
.3
	txa
	and #PAD_DOWN
	beq .4
	lda #3					;play effect 3 on channel 3
	ldx #FT_SFX_CH3
	jsr FamiToneSfxStart
	jmp mainLoop
.4
	txa
	and #PAD_B
	beq .5
	ldx #LOW(music1_module)	;play first song
	ldy #HIGH(music1_module)
	jsr FamiToneMusicStart
	jmp mainLoop
.5
	txa
	and #PAD_A
	beq .6
	ldx #LOW(music2_module)	;play second song
	ldy #HIGH(music2_module)
	jsr FamiToneMusicStart
	jmp mainLoop
.6
	txa
	and #PAD_START
	beq .7
	lda #11					;play sample 11
	jsr FamiToneSampleStart
	jmp mainLoop
.7
	txa
	and #PAD_SELECT
	beq .8
	jsr FamiToneMusicStop		;stop music
.8
	jmp mainLoop



waitVBlank
    bit PPU_STATUS
.1
    bit PPU_STATUS
    bpl .1
	rts

waitNMI
	lda FRAME_CNT
.1
	cmp FRAME_CNT
	beq .1
	rts

nmi
	inc FRAME_CNT
    rti

	.include "controller.asm"
	.include "rle.asm"

screenData
	.incbin "screen.rle"

	.include "famitone.asm"
	.include "music1.asm"
	.include "music_dpcm.asm"
	.include "sounds.asm"

    .bank 2
	.org FT_DPCM_OFF
	.incbin "music_dpcm.bin"

	.bank 3
	.org $e000
	.include "music2.asm"
    .org  $fffa
    .dw   nmi
    .dw   reset

	.bank 4
	.incbin "screen.chr"
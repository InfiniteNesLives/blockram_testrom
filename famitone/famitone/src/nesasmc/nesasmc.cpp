#define _CRT_SECURE_NO_WARNINGS	//to allow fopen etc be used in VC2008 Express without warnings

#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <string.h>


#define ASM6	0
#define CA65	1


unsigned char *src,*out;
int src_size,out_size;



void putstr(unsigned char *out,int &pd,const char *str)
{
	int i;

	for(i=0;i<(int)strlen(str);i++) out[pd++]=str[i];
}



int process(const char *name,int mode)
{
	char filename[1024];
	FILE *file;
	int i,pp,pd,ps;
	bool equ;

	pp=0;
	pd=0;

	while(pp<src_size)
	{
		if(src[pp]==';')//comment, skip to the next row
		{
			while(pp<src_size)
			{
				if(src[pp]==0x0a) break;
				out[pd++]=src[pp++];
			}
			continue;
		}

		if(src[pp]==0x0a)//new row, check for a label
		{
			out[pd++]=src[pp++];
			if(pp>=src_size) break;

			if(mode==CA65)//ca65 requires .define instead of a label for a define
			{
				if(!memcmp(&src[pp],"FT_DPCM_ENABLE",14))
				{
					putstr(out,pd,".define FT_DPCM_ENABLE 1");
					pp+=14;
					continue;
				}

				if(!memcmp(&src[pp],"FT_SFX_ENABLE",13))
				{
					putstr(out,pd,".define FT_SFX_ENABLE  1");
					pp+=13;
					continue;
				}

				if(!memcmp(&src[pp],"FT_THREAD",9))
				{
					putstr(out,pd,".define FT_THREAD      1");
					pp+=9;
					continue;
				}
			}

			if(src[pp]>=0x20&&src[pp]!=' '&&src[pp]!=';')//not a space, next line, tab, or a comment
			{
				if(src[pp]=='.')//beginning of a local label
				{
					out[pd++]='@';//both ca65 and asm6 use @ for local labels instead of .
					pp++;
				}

				if(mode==CA65)//ca65 requires : after a label (not for equ)
				{
					ps=pp;
					equ=false;

					while(ps<src_size)//check if there is equ on the line
					{
						if(!memcmp(&src[ps],"equ",3)||!memcmp(&src[ps],"EQU",3)||src[ps]=='=')
						{
							equ=true;
							break;
						}
						if(src[ps]==0x0d||src[ps]==';') break;
						ps++;
					}

					if(!equ)//add : after a label
					{
						while(pp<src_size)
						{
							if(src[pp]<0x20||src[pp]==' ') break;
							out[pd++]=src[pp++];
						}
						out[pd++]=':';
					}
				}
			}
		}

		if(src[pp]=='.'&&src[pp-1]==' ')//label reference
		{
			out[pd++]='@';//both ca65 and asm6 use @ for local labels instead of .
			pp++;
		}

		if(!memcmp(&src[pp],"LOW",3))
		{
			if(mode==CA65) putstr(out,pd,".lobyte");
			if(mode==ASM6) out[pd++]='<';
			pp+=3;
		}

		if(!memcmp(&src[pp],"HIGH",4))
		{
			if(mode==CA65) putstr(out,pd,".hibyte");
			if(mode==ASM6) out[pd++]='>';
			pp+=4;
		}

		if(src[pp]=='[')//both ca65 and asm6 needs () instead of []
		{
			out[pd++]='(';
			pp++;
		}
		if(src[pp]==']')
		{
			out[pd++]=')';
			pp++;
		}

		if(mode==CA65)//ca65 needs changes in .ifdef/.ifndef
		{
			if(!memcmp(&src[pp],".ifndef",7))//.ifndef to .if(! )
			{
				putstr(out,pd,".if(!");
				pp+=7;
				while(pp<src_size)
				{
					if(src[pp]>0x20) break;
					pp++;
				}
				while(pp<src_size)
				{
					if(src[pp]==' '||src[pp]==0x09||src[pp]<0x20) break;
					out[pd++]=src[pp++];
				}
				out[pd++]=')';
			}
			if(!memcmp(&src[pp],".ifdef",6))//.ifdef to .if( )
			{
				putstr(out,pd,".if(");
				pp+=6;
				while(pp<src_size)
				{
					if(src[pp]>0x20) break;
					pp++;
				}
				while(pp<src_size)
				{
					if(src[pp]==' '||src[pp]==0x09||src[pp]<0x20) break;
					out[pd++]=src[pp++];
				}
				out[pd++]=')';
			}
		}

		if(mode==ASM6)//asm6 need db,dw,byte,word without .
		{
			if(!memcmp(&src[pp],".byte",5)) pp++;
			if(!memcmp(&src[pp],".word",5)) pp++;
			if(!memcmp(&src[pp],".db",3)) pp++;
			if(!memcmp(&src[pp],".dw",3)) pp++;
		}

		out[pd++]=src[pp++];
	}

	out_size=pd;

	strcpy(filename,name);
	i=strlen(filename);

	if(mode==CA65)
	{
		filename[i-3]='s';
		filename[i-2]=0;
	}
	if(mode==ASM6)
	{
		filename[i-4]='_';
		filename[i+0]='6';
		filename[i+1]='.';
		filename[i+2]='a';
		filename[i+3]='s';
		filename[i+4]='m';
		filename[i+5]=0;
	}

	file=fopen(filename,"wb");
	
	if(!file)
	{
		printf("Error: Can't create file\n");
		free(src);
		free(out);
		return 1;
	}

	fwrite(out,out_size,1,file);
	fclose(file);

	return 0;
}



int main(int argc,char *argv[])
{
	FILE *file;

	if(argc<2)
	{
		printf("NESASM source code converter for FamiTone\n");
		printf("by Shiru (shiru@mail.ru) 06'11\n");
		printf("Usage: nesasmc filename.asm\n");
		return 0;
	}

	file=fopen(argv[1],"rb");

	if(!file)
	{
		printf("Error: Can't open file %s\n",argv[1]);
		return 1;
	}

	fseek(file,0,SEEK_END);
	src_size=ftell(file);
	fseek(file,0,SEEK_SET);
	src=(unsigned char*)malloc(src_size+16);
	out=(unsigned char*)malloc(src_size*2);
	fread(src,src_size,1,file);
	fclose(file);

	process(argv[1],CA65);
	process(argv[1],ASM6);

	free(src);
	free(out);

	return 0;
}


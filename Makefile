

#Build directory
BUILD = build

#project name
#doesn't need to be associated with any file names
PROJ = test_rom

#SOURCES=$(wildcard source/*.c source_stm_only/*.c)
#OBJECTS=$(patsubst %.c,%.o,$(SOURCES))
ASM_SRC=$(wildcard source/*.s)
ASM_OBJ=$(patsubst %.s,%.o,$(SOURCES))

all: clean dir $(BUILD)/$(PROJ).nes

$(BUILD)/$(PROJ).nes:
	ca65 source/proj.s -g -o $(BUILD)/proj.o
	ld65 -o $(BUILD)/$(PROJ).nes -C include/proj.cfg $(BUILD)/proj.o -m $(BUILD)/proj.map.txt -Ln $(BUILD)/proj.labels.txt --dbgfile $(BUILD)/$(PROJ).dbg
	lua convert_gw1n.lua

dir:
	mkdir -p $(BUILD)

clean:
	rm -f build/$(PROJ).nes
#	rm -rf build




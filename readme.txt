
ascii tileset by "NES Doug": https://github.com/nesdoug/01_Hello
minor modification to correct order of [/] characters & addition of first 2 rows.

TODO:
list version number
get CIC version number
get ESP firmware version
get FPGA version number
get SD card data/ver
get bootloader ver
select PRG-ROM and boot
different keypress for bootloader vs dev PRG-ROM
parallel CICCOM test
	-power up/down ESP-01
	-change ESP boot/flash pins to enter ESP bootmodes
	-jtag enable so ESP can JTAG->FPGA
print instructions/url at testrom power on

onscreen keyboard
	-insert, delete, backspace
	-preview selected character in text location helps with overscan
	-"end" key that inserts CRLF<null>
	-maybe just make a second overscan freindly keyboard mode that only has 0-128
	 put it in empty tile area ||||||||| below the non-overscan freindly area.
	-kind of want tiles 0x80-0xFF to be numbered..  if did this wouldn't need
	 to see that area of the keyboard and could use B+pad for inserting those values
	-restrict cursor to keyboard area while in keyboard entry mode?

single wifi connection check & setup test
	-check connection & list IP
	-scan networks listing in order of signal strength
	-select network from list with keyboard so don't have to type in ssid
	-support manually typing ssid
	-option to CWLAP on the selected network for more details
	-password entry without remembering quotes/CRLF
	-meaningful error check
	-if success ask if want to make default
network tests
	-list IP address
	-ping 192.168.1.1
	-ping online
	-get time, select timezone (non-volatile) 
	-quote of the day
load executable code from online/network
	-ram only
	-perform checksum verification
	-allow option to jump
 *This would allow for easy updates*
	-including CIC version updates if console doesn't have lockout chip, or I add parallel ciccom
	 functionality that can update flash (or atleast load code into SRAM that can be smart enough
	 to not erase/write flash if there's not a big enough time window for next CIC xfr)
SD card data updating 
	-maybe this is best option as doesn't require network.  
	-Doesn't require 6502 to be executing, CIC just needs power to copy SDcard data to itself, FPGA, or ESP

I'm probably not going to be able to get online updates of the CIC/STM8 firmware (and thus the FPGA configuration) functionally complete without the INLretro before release.
Hoping to get the ESP firmware updatable without the INLretro, but not sure I'll get there.  Although that tiny network bootstrap should create me a path in provided I've got means of toggling the ESP's bootloader pins.
the AT firmware has an built in OTA, but doesn't seem to work.  Might be issue with the provided update files from espressif.  I need to test if I can get it to work if I provide my own server IP and point it to files on my website (that's ulitmately what I want anyway).
infiniteneslivesToday at 9:30 AM
Although the ESP has connection to the FPGA's JTAG signals.  So long as the ESP can get the STM8 to activate JTAG by toggling the JTAGSEL pin, I don't need to update the STM8 firmware to get an online update of the FPGA.  The ESP could do it.
(ESP firmware would need to be updateable online first though).
many chickens, many eggs..  :hatching_chick:

keyboard insert/delete/backspace modes

